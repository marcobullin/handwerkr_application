var express = require('express');
var fs = require('fs');
var app = express();
var basicAuth = require('express-basic-auth');

app.use(express.static('./build/public'));

app.use(basicAuth({
    users: { 'top': 'secret!' },
    challenge: true,
    realm: 'my_realm'
}));

app.get('/*', function (req, res) {
    fs.readFile('./build/index.html', function (err, html) {
        if (err) {
            throw err; 
        }       
    
        res.writeHeader(200, {"Content-Type": "text/html"});  
        res.write(html);  
        res.end(); 
    });
});

app.listen(process.env.PORT || 3000, function () {
    console.log("Ready", "Mode = " + process.env.NODE_ENV);
});