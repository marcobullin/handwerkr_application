#!/bin/sh

docker build -t handwerkr_client . -f Dockerfile.dev
docker container rm -f handwerkr_client
docker container run -d -p 80:3000 --net=handwerkr -v $(pwd):/app --name=handwerkr_client handwerkr_client