import dispatcher from '../../../dispatcher';
import xhttp from '../../../xhttp';

export function getMiddleWage() {
  xhttp('GET', '/middleWage').then(middleWage => {
    dispatcher.dispatch({type: 'FETCHED_WAGE', middleWage});
  });
}

export function update(middleWage) {
  xhttp('POST', '/middleWage/' + middleWage._id, {middleWage: middleWage.middleWage}).then(() => {
    dispatcher.dispatch({type: 'UPDATED_WAGE', middleWage});
  });
}