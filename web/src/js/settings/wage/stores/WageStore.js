
import { EventEmitter } from 'events';
import dispatcher from '../../../dispatcher';

class WageStore extends EventEmitter {
  constructor() {
    super();
    this.reset();
  }

  reset() {
    this.middleWage = {};
    this.updated = false;
  }

  handleActions(action) {
    switch(action.type) {
      case 'FETCHED_WAGE': {
        this.middleWage = action.middleWage;
        this.emit('change');
        break;
      }
      case 'UPDATED_WAGE': {
        this.updated = true;
        this.middleWage = action.middleWage;
        this.emit('change');
        break;
      }
    }
  }
}

const store = new WageStore;
dispatcher.register(store.handleActions.bind(store));

export default store;