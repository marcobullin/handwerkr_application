import React from 'react';
import Loading from '../../common/Loading';
import * as Actions from './actions/WageActions';
import WageStore from './stores/WageStore';

export default class OverviewPage extends React.Component {
  constructor() {
    super();
    
    this.state = {
      fetching: true,
      updated: WageStore.updated,
      middleWage: WageStore.middleWage
    };

    this.handleChange = this.handleChange.bind(this);
    
    Actions.getMiddleWage();
  }

  componentDidMount() {
    WageStore.on('change', this.handleChange);
  }

  componentWillUnmount() {
    WageStore.removeListener('change', this.handleChange);
    WageStore.reset();
  }

  handleChange() {
    this.setState({
      fetching: false,
      updated: WageStore.updated,
      middleWage: WageStore.middleWage
    });
  }

  changeMiddleWage() {
    this.state.middleWage.middleWage = this.refs.middleWage.value;

    this.setState({
      middleWage: this.state.middleWage
    });
  }

  saveMiddleWage() {
    Actions.update(this.state.middleWage);
  }

  render() {
    const { fetching, updated, middleWage } = this.state;

    if (fetching) {
      return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px'}}>
          <Loading />
        </div>
      );
    }

    let successAlertComponent = ''
    if (updated) {
      successAlertComponent = <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Der Stundenlohn wurde erfolgreich gespeichert.</strong>
      </div>
    }

    return (
      <div>
        <div class="container-fluid">
          { successAlertComponent }

          <h1>Lohneinstellungen</h1>
          <div class="well">
            <div class="row">
              <div class="col-md-12 col-xs-12 form-group">
                <label for="middleWage">Stundenlohn in €</label>
                <input type="number" ref="middleWage" id="middleWage" class="form-control input" placeholder="Stundenlohn in €" onChange={this.changeMiddleWage.bind(this)} value={ middleWage.middleWage } />
              </div>
            </div>

            <button type="button" onClick={this.saveMiddleWage.bind(this)} class="btn btn-action">Speichern</button>
          </div>
        </div>
      </div>
    );
  }
}