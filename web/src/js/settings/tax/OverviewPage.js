import React from 'react';
import Loading from '../../common/Loading';
import * as Actions from './actions/TaxActions';
import TaxStore from './stores/TaxStore';

export default class OverviewPage extends React.Component {
  constructor() {
    super();
    
    this.state = {
      fetching: true,
      updated: TaxStore.updated,
      tax: TaxStore.tax
    };

    this.handleChange = this.handleChange.bind(this);
    
    Actions.getTax();
  }

  componentDidMount() {
    TaxStore.on('change', this.handleChange);
  }

  componentWillUnmount() {
    TaxStore.removeListener('change', this.handleChange);
    TaxStore.reset();
  }

  handleChange() {
    this.setState({
      fetching: false,
      updated: TaxStore.updated,
      tax: TaxStore.tax
    });
  }

  changeTax() {
    const tax = this.refs.tax.value;

    this.setState({
      tax
    });
  }

  saveTax() {
    Actions.update(this.refs.tax.value);
  }

  render() {
    const { fetching, updated, tax } = this.state;

    if (fetching) {
      return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px'}}>
          <Loading />
        </div>
      );
    }

    let successAlertComponent = ''
    if (updated) {
      successAlertComponent = <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Die Umsatzsteuer wurde erfolgreich gespeichert.</strong>
      </div>
    }

    return (
      <div>
        <div class="container-fluid">
          { successAlertComponent }

          <h1>Umsatzsteuereinstellungen</h1>
          <div class="well">
            <div class="row">
              <div class="col-md-12 col-xs-12 form-group">
                <label for="tax">Umsatzsteuer in %</label>
                <input type="number" ref="tax" id="tax" class="form-control input" placeholder="Umsatzsteuer in %" onChange={this.changeTax.bind(this)} value={ tax } />
              </div>
            </div>

            <button type="button" onClick={this.saveTax.bind(this)} class="btn btn-action">Speichern</button>
          </div>
        </div>
      </div>
    );
  }
}