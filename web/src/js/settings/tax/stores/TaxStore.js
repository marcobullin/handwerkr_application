
import { EventEmitter } from 'events';
import dispatcher from '../../../dispatcher';

class TaxStore extends EventEmitter {
  constructor() {
    super();
    this.reset();
  }

  reset() {
    this.tax = 19;
    this.updated = false;
  }

  handleActions(action) {
    switch(action.type) {
      case 'FETCHED_TAX': {
        this.tax = action.tax;
        this.emit('change');
        break;
      }
      case 'UPDATED_TAX': {
        this.updated = true;
        this.tax = action.tax;
        this.emit('change');
        break;
      }
    }
  }
}

const store = new TaxStore;
dispatcher.register(store.handleActions.bind(store));

export default store;