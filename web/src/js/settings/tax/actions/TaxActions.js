import dispatcher from '../../../dispatcher';
import xhttp from '../../../xhttp';

export function getTax() {
  xhttp('GET', '/tax').then(tax => {
    dispatcher.dispatch({type: 'FETCHED_TAX', tax});
  });
}

export function update(tax) {
  xhttp('POST', '/tax', {tax: tax}).then(() => {
    dispatcher.dispatch({type: 'UPDATED_TAX', tax});
  });
}