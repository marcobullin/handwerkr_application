import axios from 'axios';
import config from './config';
import crypto from 'crypto';

export default function xhr(method, path, data, onSuccess, onError) {
  const sharedSecret = '_my_shared_secret';
  const encodedPath = path.trim();
  const url = config.url + encodedPath;
  const signature = crypto.createHmac('sha256', sharedSecret).update(encodedPath).digest("hex");

  return axios({
    method: method.toLowerCase(),
    responseType: 'json',
    headers: {
      'X-Signature': signature
    },
    url: url,
    data: data
  }).then(response => {
    return response.data || [];
  }).catch((error) => {
    console.log(error);
  });
};