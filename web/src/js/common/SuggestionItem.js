import React from 'react';

export default class SuggestionItem extends React.Component {
  clickItem(e) {
    e.preventDefault();
    const { id, selectSuggestion } = this.props;

    selectSuggestion(id);
  }
  render() {
    const { id, title, selected } = this.props;
    const className = selected ? 'list-group-item list-group-item-info' : 'list-group-item';
    return (
      <a href="#" onClick={this.clickItem.bind(this)} class={className}>{title}</a>
    );
  }
}