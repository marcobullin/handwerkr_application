import React from 'react';
import { Link } from 'react-router-dom';

export default class Home extends React.Component {
  render() {
    return (
      <div>
        <div class="container-fluid">
          <h1>Willkommen beim HandwerkR</h1>
          <p>Hier kannst du deine <Link to="/customers">Kunden</Link>, <Link to="/materials">Materialien</Link>, <Link to="/calculations">Kalkulationen</Link> und <Link to="/offers">Angebote</Link> verwalten.</p>
        </div>
      </div>
    );
  }
}