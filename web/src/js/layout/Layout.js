import React from 'react';
import Nav from './Navigation';

export default class Layout extends React.Component {
  render() {
    const { location } = this.props;
    return (
      <div>
        <Nav location={location}/>
        {this.props.children}
      </div>
    );
  }
}