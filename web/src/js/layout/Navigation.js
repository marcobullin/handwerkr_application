import React from 'react';
import { Link } from 'react-router-dom';

export default class Navigation extends React.Component {
  render() {
    const { location } = this.props;
    const offersClass = location.pathname.match(/^\/offers/) ? 'active' : '';
    const customersClass = location.pathname.match(/^\/customers/) ? 'active' : '';
    const materialsClass = location.pathname.match(/^\/materials/) ? 'active' : '';
    const calculationsClass = location.pathname.match(/^\/calculations/) ? 'active' : '';
    const invoicesClass = location.pathname.match(/^\/invoices/) ? 'active' : '';

    return (
      <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">HandwerkR</a>
          </div>

          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class={offersClass}><Link to="/offers">Angebote</Link></li>
              <li class={invoicesClass}><Link to="/invoices">Rechnungen</Link></li>
              <li class={customersClass}><Link to="/customers">Kunden</Link></li>
              <li class={materialsClass}><Link to="/materials">Materialien</Link></li>
              <li class={calculationsClass}><Link to="/calculations">Kalkulationen</Link></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Einstellungen <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><Link to="/settings/wage">Stundenlohn</Link></li>
                  <li><Link to="/settings/tax">Umsatzsteuer</Link></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}