module.exports = {
  url: process.env.NODE_ENV === 'production' ? 'https://handwerkrservice.herokuapp.com' : 'http://localhost:3001'
}