export function getUst(netWage, tax) {
  return (netWage / 100 * parseFloat(tax));
}

export function getNetWage(sections) {
  let netWage = 0;
  
  sections.map(section => {
    section.subSections.map(subSection => {
      subSection.positions.map(position => {
        if (position.positionType === 'calculation' && (position.positionMode === 'normal' || position.positionMode === '')) {
          netWage += (validFloat(position.positionNewWageMinutes || position.positionWageMinutes || 0) / 60) * validFloat(position.positionNewMiddleWage || position.positionMiddleWage || 0) * validFloat(position.positionQuantity);
        }
      });
    });
  });

  return netWage;
}

export function getWagePortionForMiddleWage(wageMinutes, middleWage) {
  return this.round(((parseFloat(wageMinutes) || 0) / 60) * (parseFloat(middleWage) || 0), 2);
}

export function round(value, decimals) {
  value = parseFloat(value) || 0;
  decimals = parseInt(decimals) || 0;
  
  return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals).toFixed(2);
}

export function validFloat(value) {
  return parseFloat(value || 0) || 0;
}

export function getDiscountAmount(price, discount) {
  return this.round(parseFloat(price || 0) / 100 * parseFloat(discount || 0), 2);
}