import React from 'react';
import InvoiceStore from './stores/InvoiceStore';
import * as InvoiceActions from './actions/InvoiceActions';
import { Link } from 'react-router-dom';
import Loading from '../common/Loading';
import moment from 'moment';
import * as Helper from '../helper';
import * as InvoiceComputing from '../offers/computing';

export default class PrintPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fetching: true,
      invoice: InvoiceStore.invoice
    };

    this.handleChange = this.handleChange.bind(this);
    
    InvoiceActions.getInvoiceById(props.match.params.invoiceId);
  }
  

  componentDidMount() {
    InvoiceStore.on('change', this.handleChange);
    window.scrollTo(0, 0);
  }

  componentWillUnmount() {
    InvoiceStore.removeListener('change', this.handleChange);
    InvoiceStore.reset();
  }

  handleChange() {
    this.setState({
      fetching: false,
      invoice: InvoiceStore.invoice
    });
  }

  markAsPayed() {
    InvoiceActions.markAsPayed(this.state.invoice._id);
  }

  render() {
    const { fetching, invoice } = this.state;

    if (fetching) {
      return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px'}}>
          <Loading />
        </div>
      );
    }

    const { customer, invoiceDate, status, partialPaymentAmount, type, offer } = invoice;
    const rows = [];
    const PositionElements = invoice.sections ? invoice.sections.map((section, j) => {
      rows.push(
        <tr key={ "section_" + j} class="print-section">
          <td><strong>Titel: { j+1 }</strong></td>
          <td><strong>{ section.title }</strong></td>
          <td></td>
          <td></td>
        </tr>
      );

      if (section.subSections) {
        section.subSections.map((subSection, x) => {
          if (subSection.title) {
            rows.push(
              <tr key={ "subsection_" + j + '_' + x} class="print-sub-section">
                <td></td>
                <td><i>- { subSection.title } -</i></td>
                <td></td>
                <td></td>
              </tr>
            );
          }

          if (subSection.positions) {
            subSection.positions.map((position, i) => {
              let lastFieldEntry = Helper.round(InvoiceComputing.getPositionTotal(position.positionNewPrice || position.positionPrice, position.positionQuantity), 2) + ' €';
              if (position.positionMode === 'eventually') {
                lastFieldEntry = 'Eventual';
              } else if (position.positionMode === 'alternative') {
                lastFieldEntry = 'Alternativ';
              }

              let special = '';
              if (position.positionType === 'calculation') {
                special = <p><br/>(Lohn: { (parseFloat(position.positionNewWagePortion || position.positionWagePortion) || 0).toFixed(2) + ' €'}, Material: { (parseFloat(position.positionNewMaterialPortion || position.positionMaterialPortion) || 0).toFixed(2) + ' €' })</p>
              }

              let descriptionComponent = '';
              if (position.positionDescription) {
                descriptionComponent = <p>{ position.positionDescription }<br/></p>
              }

              rows.push(
                <tr class="print-position" key={ "position_" + j + '_' + x + '_' + i}>
                  <td>{ position.positionNumber }</td>
                  <td>
                    <p>{ position.positionName }</p>
                    { descriptionComponent }
                    <p><strong>Menge: ca. { position.positionQuantity } { position.positionUnit }</strong></p>
                    {special}
                  </td>
                  <td>{ Helper.round(position.positionNewPrice ? position.positionNewPrice : position.positionPrice, 2) }&nbsp;€</td>
                  <td>{ lastFieldEntry }</td>
                </tr>
              );
            });
          }
        });
      }


      const sumWithoutDiscount = InvoiceComputing.getSectionPriceWithoutDiscount(section);
      const sum = InvoiceComputing.getSectionPrice(section);

      rows.push(      
        <tr key={ "sum_without_discount_" + j} class="print-sub-section-sum">
          <td></td>
          <td><strong>Summe { section.title }</strong></td>
          <td></td>
          <td><strong>{ Helper.round(sumWithoutDiscount || sum, 2) }&nbsp;€</strong></td>
        </tr>
      );

      if (section.discount) {
        rows.push(      
          <tr key={ "sum_discount_" + j} class="print-sub-section-discount">
            <td></td>
            <td><strong>Angebot { section.discount }&nbsp;%</strong></td>
            <td></td>
            <td class="print-sub-section-discount-value"><strong>-{ Helper.getDiscountAmount(sumWithoutDiscount, section.discount) }&nbsp;€</strong></td>
          </tr>
        );
        rows.push(      
          <tr key={ "sum_" + j} class="print-sub-section-sum">
            <td></td>
            <td><strong>Gesamt { section.title }</strong></td>
            <td></td>
            <td><strong>{ Helper.round(sum, 2) }&nbsp;€</strong></td>
          </tr>
        );
      }
    }) : '';

    const SummeryElements = invoice.sections.map((section, i) => {
      const sum = InvoiceComputing.getSectionPrice(section);
      
      return (
        <tr key={i}>
          <td>Titel: { (i + 1)} { section.title }</td>
          <td>
            { Helper.round(sum, 2) }&nbsp;€
          </td>
        </tr>
      );
    });

    let customerData = '';
    if (customer) {
      let title = '';
      switch (customer.title) {
        case 'mrs': title = 'Frau '; break;
        case 'mr': title = 'Herr '; break;
      }

      customerData = <p class="print-address pull-left">
        { title + customer.name }<br/>
        { customer.street }<br/>
        { customer.zip } { customer.city }
      </p>
    }

    let lawText = '';
    if (invoice.showLawText) {
      const netWage = Helper.getNetWage(invoice.sections);
      const ust = Helper.getUst(netWage, invoice.tax);
      const grossWage = ust + netWage;

      lawText = <p>
        Gemäß §35a Abs.2 S.1 EStG können Sie bei Renovierungs- oder Modernisierungsarbeiten 20 % des Lohnwertes einer Rechnung als Steuerbonus geltend machen. Der Nettolohnwert der Rechnung beläuft sich auf { netWage.toFixed(2) } €.<br/>
        Die anteilige USt ({ invoice.tax } %) des Lohnwertes beträgt { ust.toFixed(2) } €.<br/>
        Damit ergibt sich ein Bruttolohnwert von { grossWage.toFixed(2) } €.
      </p>
    }

    const sectionsPrice = InvoiceComputing.getSectionsPrice(invoice.sections);

    let netPrice = sectionsPrice;

    let prices = [];
    if (type === 'end' && offer.invoices && offer.invoices.length > 0) { 
      offer.invoices.forEach(invoiceItem => {
        let price = invoiceItem.type === 'partial' ? InvoiceComputing.getSectionsPrice(invoiceItem.sections) : invoiceItem.partialPaymentAmount;
        
        prices.push({
          type: invoiceItem.type,
          invoiceId: invoiceItem.invoiceId,
          invoiceDate: invoiceItem.invoiceDate,
          price: price
        });

        netPrice -= Helper.validFloat(price);
      });
    }
    
    const netAmount = InvoiceComputing.getSectionsNetAmount(netPrice, invoice.withoutTax ? 0 : invoice.tax);
    const invoicePrice = netPrice + netAmount;
    const allowanceAmount = InvoiceComputing.getAllowanceAmount(invoicePrice, invoice.allowance);
    const invoicePriceWithAllowance = invoicePrice - allowanceAmount;

    let PaymentInfoComponents = [];
    if (type === 'end' && prices.length > 0) {
      prices.forEach(price => {
        PaymentInfoComponents.push(
          <tr key={ price.invoiceId }>
            <td>{ price.type === 'partial' ? `Teilrechnung vom ${price.invoiceDate} (${price.invoiceId})`: `Anzahlungsrechnung vom ${price.invoiceDate} (${price.invoiceId})` }</td>
            <td>-{ Helper.round(price.price, 2) }&nbsp;€</td>
          </tr>
        );
      });

      PaymentInfoComponents.push(
        <tr key="end-sum" class="print-netto-sum">
          <td>Nettosumme mit allen Abzügen</td>
          <td>{ Helper.round(netPrice, 2) }&nbsp;€</td>
        </tr>
      );
    }

    return (
      <div class="print">
        <img src="/images/Fliesenleger-Logo-240x70.png"/>

        <div class="print-content">
          <h1 class="print-headline">Druckvorschau</h1> 
          <Link to={'/offers/update/' + invoice.offer.offerId} class="btn btn-secondary print-cancel">Zum Angebot</Link>&nbsp;
          <Link to={'/invoices'} class="btn btn-secondary print-cancel">Alle Rechnungen</Link>&nbsp;
          { status !== 'payed' ? <span><Link to={{pathname: `/invoices/${invoice._id}`, state: {adjust: true}}} class="btn btn-secondary print-cancel">Rechnung anpassen</Link>&nbsp;</span> : ''}
          <a href="javascript:window.print();" class="btn btn-action print-cancel">Drucken</a>&nbsp;
          { status !== 'payed' ? <button type="button" onClick={ this.markAsPayed.bind(this) } class="btn btn-action print-cancel">Geld erhalten</button> : <strong>ist BEZAHLT</strong> }

          <div style={{overflow: 'hidden'}}>
            { customerData }
            <p class="pull-right">
              Fliesenlegermeister Andreas Francke<br/>
              Emseloher Schulgasse 9<br/>
              06542 Allstedt<br/>
              Tel.: 034659/61502<br/>
              Fax.: 034659/61503
            </p>
          </div>
          
          <table class="print-positions">
            <thead>
              <tr class="print-heading">
                <th colSpan="2"><h3>{ invoice.type === 'partial' ? 'Teilrechnung' : 'Endrechnung' }: { invoice.invoiceId }</h3></th>
                <th colSpan="2"><h4 class="pull-right">vom: { moment(invoiceDate, 'DD.MM.YYYY').format('DD.MM.YYYY') }</h4></th>
              </tr>
              <tr class="print-heading">
                <th colSpan="2">Bauvorhaben: { invoice.name }</th>
                <th>Kd-Nr.: { (customer ? customer.customerId : '') }</th>
                <th class="print-pageNumber"></th>
              </tr>
              <tr class="print-heading">
                <th>Position</th>
                <th>Bezeichnung</th>
                <th>Einzelpreis</th>
                <th>Gesamtpreis</th>
              </tr>
            </thead>
            <tbody>
              { rows }
            </tbody>
          </table>


          <table class="print-summary">
            <thead>
              <tr>
                <th>Titelzusammenstellung</th>
                <th>Preise</th>
              </tr>
            </thead>

            <tbody>
              { SummeryElements }
              <tr class="print-netto-sum"><td>Nettosumme</td><td>{ Helper.round(sectionsPrice, 2) }&nbsp;€</td></tr>
              
              { PaymentInfoComponents }

              <tr><td>+ { invoice.withoutTax ? 0 : invoice.tax } % USt</td><td>{ netAmount.toFixed(2) }&nbsp;€</td></tr>
              { invoice.withoutTax ? <tr><td>Die Steuerschuld trägt gem. §13b UStG. der Leistungsempfänger Umsatzsteuer {invoice.tax} %</td><td></td></tr> : null }
              <tr><td>Steuer-Nr.: 11822040758 beim Finanzamt Eisleben</td><td></td></tr>
              <tr class="print-sum"><td>Rechnungsbetrag</td><td>{ invoicePrice.toFixed(2) }&nbsp;€</td></tr>
              { invoice.dueDateAllowance ? <tr><td colSpan="2">Bei Zahlungseingang bis zum { invoice.dueDateAllowance } abzüglich { invoice.allowance } % Skonto = {Helper.round(allowanceAmount, 2)}&nbsp;€ vom Rechnungsbetrag, ergibt einen zu zahlenden Betrag von {Helper.round(invoicePriceWithAllowance, 2)}&nbsp;€.</td><td></td></tr> : null }
              <tr><td colSpan="2">Zahlen Sie bitte bis zum { invoice.dueDate }{ invoice.dueDateAllowance ? ', ohne Skontoabzug.' : '.' }</td><td></td></tr>
            </tbody>
          </table>

          { lawText }
        </div>

        <div class="print-payment">
          <p class="print-payinfo">Bitte überweisen Sie den Betrag auf eines der folgenden Konten</p>
          <p>
            IBAN: DE29 8008 0000 0848 4260 00&nbsp;&nbsp;&nbsp;BIC: DRESDEFF800&nbsp;&nbsp;&nbsp;Commerzbank<br/>
            IBAN: DE77 8006 3558 0005 5681 53&nbsp;&nbsp;&nbsp;BIC: GENODEF1SGH&nbsp;&nbsp;&nbsp;Volksbank
          </p>
        </div>
      </div>
    );
  }
}