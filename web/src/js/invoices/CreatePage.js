import React from 'react';
import InvoiceForm from './components/InvoiceForm';
import PartialPaymentForm from './components/PartialPaymentForm';
import { Redirect } from 'react-router-dom';
import InvoiceStore from './stores/InvoiceStore';
import * as InvoiceActions from './actions/InvoiceActions';
import Loading from '../common/Loading';

export default class CreatePage extends React.Component {
  constructor(props) {
    super(props);

    const { offerId, sectionIndex } = this.props.match.params;
    
    this.offerId = offerId;
    this.sectionIndex = sectionIndex;
    this.isPartialPayment = props.match.url.indexOf('partialPayment') !== -1;

    this.state = {
      fetching: true,
      redirectTo: InvoiceStore.redirectTo
    };

    this.handleChange = this.handleChange.bind(this);

    InvoiceActions.getInvoiceDraft(offerId, sectionIndex, this.isPartialPayment);
  }

  handleChange() {
    if (!this._isMounted) { return; }

    this.setState({
      fetching: false,
      redirectTo: InvoiceStore.redirectTo
    });
  }

  componentDidMount() {
    this._isMounted = true;
    InvoiceStore.on('change', this.handleChange);
    window.scrollTo(0, 0);
  }

  componentWillUnmount() {
    this._isMounted = false;
    InvoiceStore.removeListener('change', this.handleChange);
    InvoiceStore.reset();
  }

  render() {
    const { fetching, redirectTo } = this.state;
    
    if (fetching) {
      return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px'}}>
          <Loading />
        </div>
      );
    }

    if (redirectTo) {
      return <Redirect to={redirectTo} />;
    }

    return (
      <div>
        <div class="container-fluid">
          <h1>Rechnung erstellen</h1>
          { this.isPartialPayment ? <PartialPaymentForm/> : <InvoiceForm/> }
        </div>
      </div>
    )
  }
}