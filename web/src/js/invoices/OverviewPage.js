import React from 'react';
import InvoiceTableEntry from './components/InvoiceTableEntry';
import InvoiceStore from './stores/InvoiceStore';
import * as InvoiceActions from './actions/InvoiceActions';
import Loading from '../common/Loading';

export default class OverviewPage extends React.Component {
  constructor() {
    super();
    this.state = {
      fetching: true,
      filter: 'all',
      typeFilter: '',
      invoices: InvoiceStore.invoices,
      pages: InvoiceStore.pages,
      page: InvoiceStore.page
    };

    this.handleChange = this.handleChange.bind(this);
    
    InvoiceActions.getAll();
  }

  componentDidMount() {
    InvoiceStore.on('change', this.handleChange);
    window.scrollTo(0, 0);
  }

  componentWillUnmount() {
    InvoiceStore.removeListener('change', this.handleChange);
    InvoiceStore.reset();
  }

  handleChange() {
    this.setState({
      fetching: false,
      invoices: InvoiceStore.invoices,
      pages: InvoiceStore.pages,
      page: InvoiceStore.page
    });
  }

  searchInvoice() {
    InvoiceActions.searchInvoice(this.refs.search.value, this.state.filter, 1, this.state.typeFilter);
  }

  changePage(page, e) {
    e.preventDefault();

    InvoiceActions.searchInvoice(this.refs.search.value, this.state.filter, page, this.state.typeFilter);
  }

  changeFilter(e) {
    this.setState({
      filter: e.target.value
    });

    InvoiceActions.searchInvoice(this.refs.search.value, e.target.value, 1, this.state.typeFilter);
  }

  changeTypeFilter(e) {
    this.setState({
      typeFilter: e.target.value
    });

    InvoiceActions.searchInvoice(this.refs.search.value, this.state.filter, 1, e.target.value);
  }

  render() {
    const { fetching, invoices, filter, typeFilter, pages, page } = this.state;
    
    if (fetching) {
      return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px'}}>
          <Loading />
        </div>
      );
    }

    const InvoiceComponents = invoices.map((invoice, i) => { 
      return <InvoiceTableEntry key={invoice._id} index={i} invoice={invoice} /> 
    });

    let PaginationComponents = [];
    for (let i = 1; i <= parseInt(pages); i++) {
      PaginationComponents.push(<li key={i} class={ i === parseInt(page) ? 'active' : ''}><a href="#" onClick={this.changePage.bind(this, i)}>{ i }</a></li>);
    }

    return (
      <div>
        <div class="container-fluid">
          <h1>Rechnungen</h1>
          <div class="panel panel-default">
            <div class="panel-heading">Alle Rechnungen</div>
            <div class="panel-body">
              <input type="text" ref="search" onChange={this.searchInvoice.bind(this)} class="form-control" placeholder="Rechnung suchen"/>
              <br/>
              <label class="radio-inline">
                <input type="radio" name="filter" onChange={this.changeFilter.bind(this)} ref="filterAll" value="all" checked={filter === 'all' ? 'checked' : '' }/>
                alle Rechnungen
              </label>
              <label class="radio-inline">
                <input type="radio" name="filter" onChange={this.changeFilter.bind(this)} ref="filterDrafts" value="draft" checked={filter === 'draft' ? 'checked' : '' }/>
                Rechnungsentwürfe
              </label>
              <label class="radio-inline">
                <input type="radio" name="filter" onChange={this.changeFilter.bind(this)} ref="filterOpen" value="open" checked={filter === 'open' ? 'checked' : '' }/>
                offene Rechnungen
              </label>
              <label class="radio-inline">
                <input type="radio" name="filter" onChange={this.changeFilter.bind(this)} ref="filterPayed" value="payed" checked={filter === 'payed' ? 'checked' : '' }/>
                bezahlte Rechnungen
              </label>
              <br/><br/>
              <label class="radio-inline">
                <input type="radio" name="typeFilter" onChange={this.changeTypeFilter.bind(this)} ref="filterPartialInvoice" value="" checked={typeFilter === '' ? 'checked' : '' }/>
                all Rechnungsarten
              </label>
              <label class="radio-inline">
                <input type="radio" name="typeFilter" onChange={this.changeTypeFilter.bind(this)} ref="filterPartialInvoice" value="percentage" checked={typeFilter === 'percentage' ? 'checked' : '' }/>
                Anzahlungsrechnungen
              </label>
              <label class="radio-inline">
                <input type="radio" name="typeFilter" onChange={this.changeTypeFilter.bind(this)} ref="filterPartialInvoice" value="partial" checked={typeFilter === 'partial' ? 'checked' : '' }/>
                Teilrechnungen
              </label>
              <label class="radio-inline">
                <input type="radio" name="typeFilter" onChange={this.changeTypeFilter.bind(this)} ref="filterFinalInvoice" value="end" checked={typeFilter === 'end' ? 'checked' : '' }/>
                Endrechnungen
              </label>
            </div>
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th class="col-md-2">Rechnungsnummer</th>
                    <th class="col-md-2">Kunde</th>
                    <th class="col-md-2">Typ</th>
                    <th class="col-md-2">Status</th>
                    <th class="col-md-2">Erstellt am</th>
                    <th class="col-md-1">Fälligkeit am</th>
                    <th class="col-md-1">Aktion</th>
                  </tr>
                </thead>
                <tbody>
                  { InvoiceComponents }
                </tbody>
              </table>
            </div>
          </div>
          <nav aria-label="Page navigation" style={{textAlign: 'center'}}>
            <ul class="pagination">
              <li>
                <a href="#" onClick={this.changePage.bind(this, parseInt(page)-1 || 1 )} aria-label="Previous">
                  <span aria-hidden="true">&laquo;</span>
                </a>
              </li>
              { PaginationComponents }
              <li>
                <a href="#" onClick={this.changePage.bind(this, (parseInt(page) + 1 <= pages) ? (parseInt(page) + 1) : pages )} aria-label="Next">
                  <span aria-hidden="true">&raquo;</span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    );
  }
}