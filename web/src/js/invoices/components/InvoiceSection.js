import React from 'react';
import InvoiceStore from '../stores/InvoiceStore';
import InvoiceSubSection from './InvoiceSubSection';
import * as InvoiceActions from '../actions/InvoiceActions';
import * as Helper from '../../helper';
import * as Computing from '../../offers/computing';
import { Link } from 'react-router-dom';

export default class InvoiceSection extends React.Component {
  constructor(props) {
    super(props);

    const { section } = props;

    this.section = section;
  }

  changeDiscount() {
    this.section.discount = this.refs.sectionDiscount.value;
    this.props.updateSection(this.props.index, this.section);
  }

  changeTitle() {
    this.section.title = this.refs.title.value;
    this.props.updateSection(this.props.index, this.section);
  }

  addSubSection() {
    InvoiceActions.addSubSection(this.props.index);
  }

  updateSubSection(subSectionIndex, subSection) {
    InvoiceActions.updateSubSection(this.props.index, subSectionIndex, subSection);
  }

  deleteSubSection(subSectionIndex) {
    InvoiceActions.deleteSubSection(this.props.index, subSectionIndex);
  }

  deleteSection() {
    var answer = confirm('Möchtest du wirklich den Titel "' + this.section.title ? this.section.title : 'kein Titel vorhanden' + '" und die dazugehörigen ' + (this.section.subSections ? this.section.subSections.length : 0) + ' Zwischentitel mit allen Positionen löschen?');
    
    if (answer === true) {
      this.props.deleteSection(this.props.index);
    }
  }

  renderSubSectionComponents(subSections) {
    return subSections ? subSections.map((subSection, i) => {
      if (!subSection.id) {
        subSection.id = 'subSection_' + new Date().getTime() + (Math.ceil(Math.random() * 999999999));
      }
      return <InvoiceSubSection key={subSection.id} sectionIndex={this.props.index} index={i} subSection={subSection} deleteSubSection={this.deleteSubSection.bind(this)} updateSubSection={this.updateSubSection.bind(this)} units={this.props.units}/>
    }) : '' ;
  }

  render() {
    const { title, subSections, discount, belongToPartialInvoice } = this.section;
    const sum = Computing.getSectionPrice(this.section);
    const lossOrProfit = Computing.getLossOrProfitForSection(this.section);
    
    let profitComponent = <li class="list-group-item list-group-item-success">
        <span class="badge">{ Helper.round(lossOrProfit, 2) } €</span>
        Gewinn
      </li>

    if (lossOrProfit < 0) {
      profitComponent = <li class="list-group-item list-group-item-danger">
        <span class="badge">{ Helper.round(lossOrProfit, 2) } €</span>
        Verlust
      </li>
    }

    if (lossOrProfit === 0) {
      profitComponent = <li class="list-group-item list-group-item-warning">
        <span class="badge">{ Helper.round(lossOrProfit, 2) } €</span>
        Gewinn/Verlust
      </li>
    }

    return (
      <div>
        <div class="panel panel-default" style={{ background: 'rgb(235, 235, 235)' }}>
          <div class="section">
            <div class={ belongToPartialInvoice ? 'section__overlay' : '' }></div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-10 cols-xs-12 form-group">
                  <label>Titel</label>
                  <input type="text" ref="title" class="form-control" placeholder="Titel" onChange={this.changeTitle.bind(this)} value={ title || '' }/>
                </div>
                <div class="col-md-2 col-xs-12 form-group">
                  <label>Rabatt in %</label>
                  <input type="number" ref="sectionDiscount" class="form-control" placeholder="Rabatt in %" onChange={this.changeDiscount.bind(this)} value={ discount || '' }/>
                </div>
              </div>
              
              { this.renderSubSectionComponents(subSections) }

              <div class="panel">
                <div class="panel-body" style={{ background: 'rgb(243, 243, 243)', boxShadow: '0px 2px 5px #999' }}>
                  <button type="button" onClick={this.addSubSection.bind(this)} class="btn btn-link"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Zwischentitel hinzufügen</button>
                </div>
              </div>

              <ul class="list-group">
                <li class="list-group-item list-group-item-success">
                  <span class="badge">{ Helper.round(sum, 2) } €</span>
                  { title ? 'Summe für ' + title : 'Summe' }
                </li>
                
                { profitComponent }
              </ul>
              <button type="button" onClick={this.deleteSection.bind(this)} class="btn btn-outline-primary"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>&nbsp;Titel löschen</button>          
            </div>
          </div>
        </div>
      </div>
    );
  }
}