
import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';

export default class InvoiceTableEntry extends React.Component {
  render() {
    const { _id, invoiceDate, invoiceId, status, customer, dueDay, dueDate, type } = this.props.invoice;
    
    return (
      <tr>
        <td>{ invoiceId ? invoiceId : 'noch nicht vergeben'}</td>
        <td>{ customer ? customer.name : '' }</td> 
        <td>{ type === 'partial' ? 'Teilrechnung' : (type === 'end' ? 'Endrechnung' : 'Anzahlungsrechnungen') }</td>
        <td>{ status  === 'draft' ? 'Entwurf' : (status === 'open' ? 'offen' : 'bezahlt')}</td>
        <td>{ moment(invoiceDate, 'DD.MM.YYYY').format('DD.MM.YYYY') }</td> 
        <td>{ dueDate ? dueDate: moment().add(dueDay, 'day').format('DD.MM.YYYY') }</td> 
        <td>
          <Link to={ `/invoices/${ _id }`} class="btn btn-secondary btn-sm"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></Link>
        </td>
      </tr>
    );
  }
}