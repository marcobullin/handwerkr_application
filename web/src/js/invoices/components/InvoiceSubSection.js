import React from 'react';
import InvoiceStore from '../stores/InvoiceStore';
import InvoicePosition from './InvoicePosition';
import * as InvoiceActions from '../actions/InvoiceActions';

export default class InvoiceSubSection extends React.Component {
  constructor(props) {
    super(props);

    const { sectionIndex, subSection } = props;
    
    // section
    this.sectionIndex = sectionIndex;

    this.subSection = subSection;
  }

  changeTitle() {
    this.subSection.title = this.refs.title.value;
    this.props.updateSubSection(this.props.index, this.subSection);
  }

  addPosition() {
    InvoiceActions.addPosition(this.sectionIndex, this.props.index);
  }

  updatePosition(positionIndex, position) {
    InvoiceActions.updatePosition(this.sectionIndex, this.props.index, positionIndex, position);
  }

  deletePosition(positionIndex) {
    InvoiceActions.deletePosition(this.sectionIndex, this.props.index, positionIndex);
  }

  deleteSubSection() {
    var answer = confirm('Möchtest du wirklich den Zwischentitel "' + (this.subSection.title ? this.subSection.title : 'kein Titel vorhanden') + '" und die dazugehörigen ' + (this.subSection.positions ? this.subSection.positions.length : 0) + ' Positionen löschen?');
    
    if (answer === true) {
      this.props.deleteSubSection(this.props.index);
    }
  }

  renderPositionComponents(positions) {
    return positions ? positions.map((position, i) => {
      if (!position.key) {
        position.key = 'position_' + new Date().getTime() + (Math.ceil(Math.random() * 999999999));
      }
      return <InvoicePosition ref={"position_" + i} key={position.key} sectionIndex={this.sectionIndex} subSectionIndex={this.props.index} index={i} position={position} deletePosition={this.deletePosition.bind(this)} updatePosition={this.updatePosition.bind(this)} units={this.props.units}/>
    }) : '';
  }

  render() {
    const { title, positions } = this.subSection;
    
    return (
      <div class="well well-sm" style={{ background: 'rgb(243, 243, 243)', boxShadow: '0px 2px 5px #999' }}>
        <div class="row">
          <div class="col-md-12 col-xs-12 form-group">
            <label>Zwischentitel</label>
            <input type="text" ref="title" class="form-control" placeholder="Zwischentitel" onChange={this.changeTitle.bind(this)} value={ title || '' }/>
          </div>
        </div>

        { this.renderPositionComponents(positions) }

        <div class="panel">
          <div class="panel-body" style={{ boxShadow: '0px 2px 5px #999' }}>
            <button type="button" onClick={this.addPosition.bind(this)} class="btn btn-link"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Position hinzufügen</button>
          </div>
        </div>

        <button type="button" onClick={this.deleteSubSection.bind(this)} class="btn btn-sm btn-outline-primary"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>&nbsp;Zwischentitel löschen</button>
      </div>
    );
  }
}