import React from 'react';
import MaterialStore from '../../materials/stores/MaterialStore';
import * as MaterialActions from '../../materials/actions/MaterialActions';
import CalculationStore from '../../calculations/stores/CalculationStore';
import * as CalculationActions from '../../calculations/actions/CalculationActions';
import SuggestionItem from '../../common/SuggestionItem';
import CalculationModal from '../../calculations/components/CalculationModal';
import MeasurementModal from '../../offers/components/MeasurementModal';
import * as Helper from '../../helper';
import * as CalcComputing from '../../calculations/computing';

export default class InvoicePosition extends React.Component {
  constructor(props) {
    super(props);

    const { sectionIndex, subSectionIndex, position } = props;

    // section
    this.sectionIndex = sectionIndex;
    // sub section
    this.subSectionIndex = subSectionIndex;

    this.position = position;

    if (this.position.positionMode === '') {
      this.position.positionMode = 'normal';
    }

    if (!this.position.positionUnit) {
      this.position.positionUnit = (this.props.units ? this.props.units[0].name : '');
    }

    this.state = {
      showCalculationModalForPosition: -1,
      showMeasurementModalForPosition: -1,
      showCalculationSuggestions: false,
      showMaterialSuggestions: false,
      calculationSuggestions: CalculationStore.calculations,
      materialSuggestions: MaterialStore.materials,
      suggestionIndex: 0
    }

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange() {
    this.setState({
      calculationSuggestions: CalculationStore.calculations,
      materialSuggestions: MaterialStore.materials
    });
  }

  componentDidMount() {
    this._isMounted = true;
    
    MaterialStore.on('change', this.handleChange);
    CalculationStore.on('change', this.handleChange)
  }

  componentWillUnmount() {
    this._isMounted = false;

    MaterialStore.removeListener('change', this.handleChange);
    CalculationStore.removeListener('change', this.handleChange);
  }

  changePositionName(e) {
    const positionType = this.refs.positionType.value;
    const positionName = e.target.value;

    var showMaterialSuggestions = false;
    var showCalculationSuggestions = false;
    
    this.position.positionName = positionName;

    if (positionName.length === 0) {
      this.setState({
        showMaterialSuggestions,
        showCalculationSuggestions
      });

      this.position = {
        positionNumber: this.position.positionNumber,
        positionMode: this.position.positionMode,
        positionType: this.position.positionType
      };
      
      return this.props.updatePosition(this.props.index, this.position);
    }

    if (positionType === 'material') {
      MaterialActions.searchMaterial(positionName);
      showMaterialSuggestions = true;
    } else if (positionType === 'calculation') {
      CalculationActions.searchCalculation(positionName);
      showCalculationSuggestions = true;
    }
    
    this.setState({
      showMaterialSuggestions,
      showCalculationSuggestions
    });
    
    this.props.updatePosition(this.props.index, this.position);
  }

  changePositionType() {
    const positionType = this.refs.positionType.value;

    this.position = {
      positionNumber: this.position.positionNumber,
      positionMode: this.position.positionMode,
      positionType
    };

    this.props.updatePosition(this.props.index, this.position);
  }

  onBlur() {
    window.setTimeout(() => {
      if (this._isMounted && (this.state.showMaterialSuggestions || this.state.showCalculationSuggestions)) {
        this.setState({
          showMaterialSuggestions: false,
          showCalculationSuggestions: false
        });
      }
    }, 500);
  }

  changeActivSuggestion(e) {
    const positionType = this.refs.positionType.value;
    const { materialSuggestions, calculationSuggestions } = this.state;
    
    var length = 0;
    if (positionType === 'material') {
      length = materialSuggestions.length;
    } else if (positionType === 'calculation') {
      length = calculationSuggestions.length;
    } else {
      return;
    }

    if (e.keyCode === 38) {
      var index = this.state.suggestionIndex - 1;
      if (index < 0) {
        index = length - 1;
      }

      this.setState({
        suggestionIndex: index
      });

      return;
    } else if (e.keyCode === 40) {
      var index = this.state.suggestionIndex + 1;
      if (index >= length) {
        index = 0;
      }

      this.setState({
        suggestionIndex: index
      });

      return;
    } else if (e.keyCode === 27) {
      this.setState({
        showMaterialSuggestions: false,
        showCalculationSuggestions: false,
        suggestionIndex: 0
      });

      return;
    } else if (e.keyCode === 13) {
      this.selectSuggestion(this.state.suggestionIndex);
      return;
    }
  }

  selectSuggestion(index) {
    const positionType = this.refs.positionType.value;
    const { materialSuggestions, calculationSuggestions } = this.state;
    
    var suggestions = [];
    if (positionType === 'material') {
      this.position = {
        _id: materialSuggestions[index]._id || '',
        positionNumber: this.position.positionNumber,
        positionMode: this.position.positionMode,
        positionName: materialSuggestions[index].title || '',
        positionPrice: materialSuggestions[index].sellingPrice || '',
        positionPurchasePrice: materialSuggestions[index].purchasePrice || 0,
        positionQuantity: materialSuggestions[index].amount || '',
        positionUnit: materialSuggestions[index].unit.name || '',
        positionDescription: materialSuggestions[index].text || '',
        positionWageMinutes: 0,
        positionType: 'material'
      };
    } else if (positionType === 'calculation') {
      const calculation = calculationSuggestions[index];
      this.position = {
        _id: calculation._id || '',
        positionName: calculation.title || '',
        positionNumber: this.position.positionNumber,
        positionMode: this.position.positionMode,
        positionPrice: CalcComputing.getCalculationPrice(calculation, calculation.middleWage),
        positionQuantity: 1,
        positionUnit: calculation.unit.name || '',
        positionDescription: calculation.text || '',
        positionWageMinutes: calculation.wageMinutes || 0,
        positionMaterialPortion: CalcComputing.getMaterialPortion(calculation.materials),
        positionWagePortion: CalcComputing.getWagePortion(calculation.wageMinutes, CalcComputing.getAverageWagePerMinute(calculation.middleWage)),
        positionMiddleWage: calculation.middleWage,
        positionMaterials: calculation.materials || [],
        positionType: 'calculation'
      };
    }

    this.setState({
      showMaterialSuggestions: false,
      showCalculationSuggestions: false,
      suggestionIndex: 0
    });

    this.props.updatePosition(this.props.index, this.position);
  }

  changePositionMode(e) {
    this.position.positionMode = e.target.value;
    this.props.updatePosition(this.props.index, this.position);
  }

  changePositionPrice(e) {
    this.position.positionPrice = parseFloat(e.target.value || 0);
    this.props.updatePosition(this.props.index, this.position);
  }

  changePositionQuantity(e) {
    this.position.positionQuantity = parseFloat(e.target.value || 0);
    this.props.updatePosition(this.props.index, this.position);
  }

  changePositionUnit(e) {
    this.position.positionUnit = e.target.value;
    this.props.updatePosition(this.props.index, this.position);
  }

  changePositionDescription(e) {
    this.position.positionDescription = e.target.value;
    this.props.updatePosition(this.props.index, this.position);
  }

  changePositionNumber(e) {
    this.position.positionNumber = e.target.value;
    this.props.updatePosition(this.props.index, this.position);
  }

  changeCalculation() {
    this.setState({
      showCalculationModalForPosition: this.props.index
    });
  }

  changeMeasurement() {
    this.setState({
      showMeasurementModalForPosition: this.props.index
    });
  }

  cancelCalculationModal() {
    this.setState({
      showCalculationModalForPosition: -1
    });
  }

  cancelMeasurementModal() {
    this.setState({
      showMeasurementModalForPosition: -1
    });
  }

  updatePosition(position) {
    this.props.updatePosition(this.props.index, position);
    
    this.setState({
      showCalculationModalForPosition: -1,
      showMeasurementModalForPosition: -1
    });
  }

  deletePosition() {
    var answer = confirm('Möchtest du wirklich die Position "' + (this.position.positionName ? this.position.positionName : this.position.positionNumber) + '" löschen?');
    
    if (answer === true) {
      this.props.deletePosition(this.props.index);
    }
  }

  render() {
    const { showMeasurementModalForPosition, showCalculationSuggestions, calculationSuggestions, showMaterialSuggestions, materialSuggestions, showCalculationModalForPosition } = this.state;
    const { _id, positionNumber, positionMode, positionType, positionName, positionPrice, positionQuantity, positionUnit, positionDescription, positionHasChanged, positionNewPrice,
        positionNewWageMinutes,
        positionNewWagePortion,
        positionNewMiddleWage,
        positionNewMaterialPortion } = this.position;

    var suggestionComponents = '';
    if (showMaterialSuggestions) {
      suggestionComponents = materialSuggestions.slice(0, 10).map((material, i) => { 
        return <SuggestionItem key={material._id} selectSuggestion={this.selectSuggestion.bind(this)} id={i} title={material.title} selected={i === this.state.suggestionIndex}/>
      });
    } else if (showCalculationSuggestions) {
      suggestionComponents = calculationSuggestions.slice(0, 10).map((calculation, i) => {
        return <SuggestionItem key={calculation._id} selectSuggestion={this.selectSuggestion.bind(this)} id={i} title={calculation.title} selected={i === this.state.suggestionIndex}/>
      });      
    }

    var CaluclationModalComponent = '';
    if (showCalculationModalForPosition !== -1) {
      CaluclationModalComponent = <CalculationModal index={showCalculationModalForPosition} position={this.position} cancelCalculationModal={this.cancelCalculationModal.bind(this)} updatePosition={this.updatePosition.bind(this)}/>
    }

    var MeasurementModalComponent = '';
    if (showMeasurementModalForPosition !== -1) {
      MeasurementModalComponent = <MeasurementModal index={showMeasurementModalForPosition} position={this.position} cancelMeasurementModal={this.cancelMeasurementModal.bind(this)} updatePosition={this.updatePosition.bind(this)}/>
    }

    var editButton = <button type="button" onClick={this.changeMeasurement.bind(this)} class="btn btn-sm btn-secondary pull-right"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Aufmaß</button>;
    if (positionType === 'calculation') {
      editButton = <button type="button" onClick={this.changeCalculation.bind(this)} class="btn btn-sm btn-secondary pull-right"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Kalkulation</button>
    }

    const UnitComponents = this.props.units.map(unit => {
      return <option key={unit._id} value={ unit.name }>{ unit.name }</option>
    });

    return (
      <div class="panel">
        <div class="panel-body" style={{ boxShadow: '0px 2px 5px #999' }}>
          { CaluclationModalComponent }
          { MeasurementModalComponent }
          <div class="row">
            <div class="col-md-1 col-xs-12 form-group">
              <label>Pos.</label>
              <input type="number" ref="positionNumber" class="form-control input-sm" placeholder="Postionsnummer" onChange={this.changePositionNumber.bind(this)} value={ positionNumber || '' }/>
            </div>
            <div class="col-md-2 col-xs-12 form-group">
              <label>Auswahl</label>
              <select class="form-control input-sm" ref="positionMode" onChange={this.changePositionMode.bind(this)} value={ positionMode || ''}>
                <option value="normal">normal</option>
                <option value="alternative">alternativ</option>
                <option value="eventually">eventual</option>
              </select>
            </div>
            <div class="col-md-2 col-xs-12 form-group">
              <label>Art</label>
              <select class="form-control input-sm" ref="positionType" onChange={this.changePositionType.bind(this)} value={ positionType || '' }>
                <option value="material">Material</option>
                <option value="calculation">Kalkulation</option>
                <option value="other">Anderes</option>
              </select>
            </div>
            <div class="col-md-3 col-xs-12 form-group" style={{ position: 'relative' }}>
              <label>Positionsname</label>
              <input type="text" ref="positionName" autoComplete="off" onChange={this.changePositionName.bind(this)}  onKeyUp={this.changeActivSuggestion.bind(this)} value={ positionName || '' } onBlur={this.onBlur.bind(this)} class="form-control input-sm" placeholder="Positionsname"/>
              <div class="list-group" style={{ position: 'absolute', zIndex: 1 }}>
                { suggestionComponents }
              </div>
            </div>
            <div class="col-md-2 col-xs-12 form-group">
              <label>Verkaufspreis in €</label>
              <input type="number" ref="positionPrice" class="form-control input-sm" placeholder="Verkaufspreis in €" onChange={this.changePositionPrice.bind(this)} value={ positionHasChanged ? positionNewPrice || '' : positionPrice || '' } />
            </div>
            <div class="col-md-1 col-xs-12 form-group">
              <label>Menge</label>
              <input type="number" ref="positionQuantity" class="form-control input-sm" placeholder="Menge" onChange={this.changePositionQuantity.bind(this)} value={ positionQuantity || '' }/>
            </div>
            <div class="col-md-1 col-xs-12 form-group">
              <label>Einheit</label>
              <select class="form-control input-sm" ref="positionUnit" onChange={this.changePositionUnit.bind(this)} value={ positionUnit || '' }>
                { UnitComponents }  
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 form-group">
              <label>Beschreibungstext</label>
              <textarea ref="positionDescription" class="form-control input-sm" rows="3" name="text" onChange={this.changePositionDescription.bind(this)} value={ positionDescription || '' }></textarea>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <button type="button" onClick={this.deletePosition.bind(this)} class="btn btn-sm btn-outline-primary"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>&nbsp;Position löschen</button>
              { editButton }
            </div>
          </div>
        </div>
      </div>
    );
  }
}