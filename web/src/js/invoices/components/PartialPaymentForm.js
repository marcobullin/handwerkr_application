import React from 'react';
import * as InvoiceActions from '../actions/InvoiceActions';
import { Link } from 'react-router-dom';
import InvoiceStore from '../stores/InvoiceStore';
import InvoiceSection from './InvoiceSection';
import moment from 'moment';
import * as Helper from '../../helper';
import * as Computing from '../../offers/computing';

export default class PartialPaymentForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      invoice: InvoiceStore.invoice
    }

    this.handleChange = this.handleChange.bind(this);

    moment.locale('de');
  }

  handleChange() {
    if (!this._isMounted) { return; }

    this.setState({
      invoice: InvoiceStore.invoice
    });
  }

  componentDidMount() {
    this._isMounted = true;
    InvoiceStore.on('change', this.handleChange);
  }

  componentWillUnmount() {
    this._isMounted = false;
    InvoiceStore.removeListener('change', this.handleChange);
    InvoiceStore.reset();
  }

  handleSubmit(redirecTo, createInvoiceNumber) {
    const invoiceDate = moment(this.state.invoice.invoiceDate, 'DD.MM.YYYY');
    const invoiceDateFormated = invoiceDate.isValid() ? invoiceDate.format('DD.MM.YYYY') : moment().format('DD.MM.YYYY');
    
    const data = {
      createInvoiceNumber,
      offer: this.state.invoice.offer._id,
      invoiceDate: invoiceDateFormated,
      offerDate: this.state.invoice.offerDate,
      tax: this.state.invoice.tax,
      name: this.state.invoice.name,
      editor: this.state.invoice.editor,
      customerId: this.state.invoice.customer ? this.state.invoice.customer.customerId : '',
      sectionIndex: this.state.invoice.sectionIndex,
      sections: JSON.stringify(this.state.invoice.sections),
      dueDay: this.state.invoice.dueDay || 1,
      allowance: this.state.invoice.allowance,
      dueDayAllowance: this.state.invoice.dueDayAllowance,
      withoutTax: this.state.invoice.withoutTax || false,
      type: 'percentage',
      status: this.state.invoice.status || 'draft',
      dueDateAllowance: this.state.invoice.dueDateAllowance || '',
      dueDate: this.state.invoice.dueDate || '',
      partialPayment: this.state.invoice.partialPayment,
      partialPaymentAmount: this.state.invoice.partialPaymentAmount
    };

    if (this.state.invoice._id) {
      InvoiceActions.updateInvoice(this.state.invoice, data, redirecTo);
    } else {
      InvoiceActions.createInvoice(data, redirecTo);
    }
  }

  changeTax(e) {
    InvoiceActions.setWithoutTax(e.target.checked);
  }

  addSection() {
    InvoiceActions.addSection();
  }

  updateSection(index, section) {
    InvoiceActions.updateSection(index, section);
  }

  deleteSection(sectionIndex) {
    InvoiceActions.deleteSection(sectionIndex);
  }

  deleteInvoce() {
    var answer = confirm('Möchtest du wirklich den Rechnungsentwurf löschen?');
    
    if (answer === true) {
      InvoiceActions.deleteInvoice(this.state.invoice._id);
    }
  }

  renderPartialPaymentInformation(partialPayment, partialPaymentAmount, total) {
    return <table>
      <thead>
        <tr>
          <th>Position</th>
          <th>Anzahlung</th>
          <th>Betrag</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td>{ `Anzahlung von ${partialPayment} % vom Gesamtbetrag (${ Helper.round(total, 2) } €) nach erfolgter Materiallieferung und Arbeitsbeginn.` }</td>
          <td>{ `${Helper.round(partialPaymentAmount, 2)} €` }</td>
        </tr>
      </tbody>
    </table>
  }

  renderDueDayComponents(key) {
    var components = [];
    for (var i = 0; i <= 30; i++) {
      if (i === 0) {
        components.push(<option key={ key + i} value="">Tage auswählen</option>);
        continue;
      }
      components.push(<option key={ key + i} value={ i }>{(i === 1 ? `${i} Tag` : `${i} Tage`)}</option>);
    }

    return components;
  }

  changeDueDay(e) {
    InvoiceActions.setDueDay(this.refs.dueDay.value);
  }

  changeDueDayAllowance(e) {
    InvoiceActions.setDueDayAllowance(this.refs.dueDayAllowance.value);
  }

  changeAllowance() {
    InvoiceActions.setAllowance(this.refs.allowance.value);
  }

  render() {
    const { invoice } = this.state;

    const { 
      sections,
      customer, 
      name, 
      editor, 
      invoiceDate,
      dueDay,
      allowance,
      dueDayAllowance,
      withoutTax,
      tax,
      partialPayment
    } = invoice;

    const totalWageMinutes = Computing.getTotalWageMinutesAsText(sections);
    const totalOfAllSections = Computing.getSectionsPrice(sections);
    const total = Computing.getPartialPaymentAmount(totalOfAllSections, partialPayment);
    const totalNetAmount = Computing.getSectionsNetAmount(total, withoutTax ? 0 : tax);
    
    let CustomerComponent = '';
    
    if (customer) {
      CustomerComponent = <div>
        <p>{ customer.name }</p>
        <p>{ customer.street }</p>
        <p>{ customer.zip + ' ' + customer.city}</p>
      </div>
    }

    return (
      <div>
        <h2>Rechnungsdetails</h2>
        <div class="form-group">
          <label>Bearbeiter</label>
          <p>Andreas Francke</p>
        </div>
        <div class="form-group">
          <label>Bauvorhaben</label>
          <p>{ name }</p>
        </div>
        <div class="form-group">
          <label>Kunde</label>
          { CustomerComponent }
        </div>

        <div class="form-group">
          <label>Erstellungsdatum</label>
          <p>{ invoiceDate || moment().format('DD.MM.YYYY') }</p>
        </div>

        <div class="separator"/>
        <h2>Anzahlungsübersicht</h2>
        { this.renderPartialPaymentInformation(partialPayment, total, totalOfAllSections) }
        
        <div class="separator"/>
        <h2>Zusammenfassung</h2>
        <ul class="list-group">
          <li class="list-group-item">
            <span class="badge">{ Helper.round(total, 2) } €</span>
            Gesamtbetrag
          </li>
          <li class="list-group-item">
            <span class="badge">{ Helper.round(total, 2) } €</span>
            Nettosumme
          </li>
          <li class="list-group-item">
            <span class="badge">{ Helper.round(total + totalNetAmount, 2) } €</span>
            +{ withoutTax ? 0 : tax }%
          </li>
        </ul>

        <div class="separator"/>
        <h2>Zahlung</h2>
        <div class="row">
          <div class="form-group col-md-3 col-xs-12">
            <label for="dueDay">Zahlung in Tagen (ohne Skonto)</label>
            <select class="form-control input-sm" ref="dueDay" onChange={this.changeDueDay.bind(this)} value={ dueDay || '' }>
              { this.renderDueDayComponents('no_allowance') }  
            </select>
          </div>
          <div class="form-group col-md-2 col-xs-12">
            <label for="unitId">Zahlungstag</label>
            <p>{ moment().add(dueDay || 1, 'day').format('DD.MM.YYYY') }</p>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-3 col-xs-12">
            <label for="dueDay">Zahlung in Tagen (mit Skonto)</label>
            <select class="form-control input-sm" ref="dueDayAllowance" onChange={this.changeDueDayAllowance.bind(this)} value={ dueDayAllowance || '' }>
              { this.renderDueDayComponents('allowance') }  
            </select>
          </div>
          <div class="form-group col-md-2 col-xs-12">
            <label for="allowance">Skonto in %</label>
            <input type="text" ref="allowance" class="form-control" placeholder="Skonto in %" name="allowance" onChange={this.changeAllowance.bind(this)} value={ allowance || '' }/>
          </div>
          { (parseInt(allowance || 0) || 0) && dueDayAllowance ? <div class="form-group col-md-2 col-xs-12">
            <label for="unitId">Zahlungstag</label>
            <p>{ moment().add(dueDayAllowance || 1, 'day').format('DD.MM.YYYY') }</p>
          </div> : '' }
        </div>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="" onChange={this.changeTax.bind(this)} checked={withoutTax ? 'checked' : ''}/>
            ohne Umsatzsteuer
          </label>
        </div>
        <br/>
        <br/>
        <div class="btn-toolbar">
          <Link to={ `/offers/update/${invoice.offer.offerId}` } class="btn btn-unimportant pull-left">Abbrechen</Link>
          <button type="button" onClick={this.handleSubmit.bind(this, 'printPreviewPartialPayment', true)} class="btn btn-action pull-right">Erstellen und zur Druckvorschau</button>
        </div>
        <br/>
        <br/>
      </div>
    );
  }
}