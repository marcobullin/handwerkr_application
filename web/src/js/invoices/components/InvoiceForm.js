import React from 'react';
import * as InvoiceActions from '../actions/InvoiceActions';
import { Link } from 'react-router-dom';
import InvoiceStore from '../stores/InvoiceStore';
import InvoiceSection from './InvoiceSection';
import moment from 'moment';
import * as Helper from '../../helper';
import * as Computing from '../../offers/computing';

export default class InvoiceForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      invoice: InvoiceStore.invoice,
      adjust:  InvoiceStore.adjust
    }

    this.handleChange = this.handleChange.bind(this);

    moment.locale('de');
  }

  handleChange() {
    if (!this._isMounted) { return; }

    this.setState({
      invoice: InvoiceStore.invoice,
      adjust: InvoiceStore.adjust
    });
  }

  componentDidMount() {
    this._isMounted = true;
    InvoiceStore.on('change', this.handleChange);
  }

  componentWillUnmount() {
    this._isMounted = false;
    InvoiceStore.removeListener('change', this.handleChange);
    InvoiceStore.reset();
  }

  handleSubmit(redirecTo, createInvoiceNumber) {
    const invoiceDate = moment(this.state.invoice.invoiceDate, 'DD.MM.YYYY');
    const invoiceDateFormated = invoiceDate.isValid() ? invoiceDate.format('DD.MM.YYYY') : moment().format('DD.MM.YYYY');
    
    const data = {
      createInvoiceNumber,
      offer: this.state.invoice.offer._id,
      invoiceDate: invoiceDateFormated,
      offerDate: this.state.invoice.offerDate,
      tax: this.state.invoice.tax,
      name: this.state.invoice.name,
      editor: this.state.invoice.editor,
      customerId: this.state.invoice.customer ? this.state.invoice.customer.customerId : '',
      sections: JSON.stringify(this.state.invoice.sections),
      dueDay: this.state.invoice.dueDay || 1,
      allowance: this.state.invoice.allowance,
      dueDayAllowance: this.state.invoice.dueDayAllowance,
      withoutTax: this.state.invoice.withoutTax || false,
      type: this.state.invoice.type,
      status: this.state.invoice.status,
      dueDateAllowance: this.state.invoice.dueDateAllowance || '',
      dueDate: this.state.invoice.dueDate || ''
    };

    if (this.state.invoice.type === 'partial') {
      data.sectionIndex = this.state.invoice.sectionIndex;
    }

    if (this.state.invoice.type === 'end') {
      data.showLawText = this.state.invoice.showLawText || false;
    }

    if (this.state.invoice._id) {
      InvoiceActions.updateInvoice(this.state.invoice, data, redirecTo);
    } else {
      InvoiceActions.createInvoice(data, redirecTo);
    }
  }

  changeTax(e) {
    InvoiceActions.setWithoutTax(e.target.checked);
  }

  addSection() {
    InvoiceActions.addSection();
  }

  updateSection(index, section) {
    InvoiceActions.updateSection(index, section);
  }

  deleteSection(sectionIndex) {
    InvoiceActions.deleteSection(sectionIndex);
  }

  deleteInvoce() {
    var answer = confirm('Möchtest du wirklich den Rechnungsentwurf löschen?');
    
    if (answer === true) {
      InvoiceActions.deleteInvoice(this.state.invoice._id);
    }
  }

  renderSectionComponents(sections) {
    return sections ? sections.map((section, i) => {
      if (!section.id) {
        section.id = 'section_' + new Date().getTime() + (Math.ceil(Math.random() * 999999999));
      }

      return <InvoiceSection key={section.id} index={i} section={section} deleteSection={this.deleteSection.bind(this)} updateSection={this.updateSection.bind(this)} units={InvoiceStore.units}/>
    }) : '';
  }

  renderDueDayComponents(key) {
    var components = [];
    for (var i = 0; i <= 30; i++) {
      if (i === 0) {
        components.push(<option key={ key + i} value="">Tage auswählen</option>);
        continue;
      }
      components.push(<option key={ key + i} value={ i }>{(i === 1 ? `${i} Tag` : `${i} Tage`)}</option>);
    }

    return components;
  }

  changeDueDay(e) {
    InvoiceActions.setDueDay(this.refs.dueDay.value);
  }

  changeDueDayAllowance(e) {
    InvoiceActions.setDueDayAllowance(this.refs.dueDayAllowance.value);
  }

  changeAllowance() {
    InvoiceActions.setAllowance(this.refs.allowance.value);
  }

  changeLawText(e) {
    InvoiceActions.setLawText(e.target.checked);
  }

  render() {
    const { invoice, adjust } = this.state;

    const { 
      showLawText, 
      sections,
      customer, 
      name, 
      editor, 
      invoiceDate,
      dueDay,
      allowance,
      dueDayAllowance,
      withoutTax,
      tax,
      type,
      partialPayment,
      partialPaymentAmount,
      offer
    } = invoice;

    const totalWageMinutes = Computing.getTotalWageMinutesAsText(sections);
    const total = Computing.getSectionsPrice(sections);
    let newTotal = total;

    let prices = [];
    if (type === 'end' && offer.invoices && offer.invoices.length > 0) { 
      offer.invoices.forEach(invoice => {
        let price = invoice.type === 'partial' ? Computing.getSectionsPrice(invoice.sections) : invoice.partialPaymentAmount;
        
        prices.push({
          type: invoice.type,
          invoiceId: invoice.invoiceId,
          invoiceDate: invoice.invoiceDate,
          price: price
        });

        newTotal -= Helper.validFloat(price);
      });
    }
    
    const totalNetAmount = Computing.getSectionsNetAmount(newTotal, withoutTax ? 0 : tax);
    const lossOrProfit = Computing.getLossOrProfitForSections(sections);

    let profitComponent = <li class="list-group-item list-group-item-success">
        <span class="badge">{ Helper.round(lossOrProfit, 2) } €</span>
        Gewinn
      </li>

    if (lossOrProfit < 0) {
      profitComponent = <li class="list-group-item list-group-item-danger">
        <span class="badge">{ Helper.round(lossOrProfit, 2) } €</span>
        Verlust
      </li>
    }

    if (lossOrProfit === 0) {
      profitComponent = <li class="list-group-item list-group-item-warning">
        <span class="badge">{ Helper.round(lossOrProfit, 2) } €</span>
        Gewinn/Verlust
      </li>
    }
    
    let CustomerComponent = '';
    if (customer) {
      CustomerComponent = <div>
        <p>{ customer.name }</p>
        <p>{ customer.street }</p>
        <p>{ customer.zip + ' ' + customer.city}</p>
      </div>
    }

    let SettingsComponent = '';
    if (type === 'end') {
      SettingsComponent = <div>
        <div class="separator"/>
        <h2>Einstellungen</h2>
        <div class="checkbox">
          <label>
            <input type="checkbox" value=""  onChange={this.changeLawText.bind(this)} checked={showLawText ? 'checked' : ''}/>
            Gesetzestext anzeigen
          </label>
        </div>
      </div>
    }

    let PaymentInfoComponents = [];
    if (type === 'end' && prices.length > 0) {
      prices.forEach(price => {
        PaymentInfoComponents.push(
          <li key={ price.invoiceId } class="list-group-item list-group-item-danger">
            <span class="badge">-{ Helper.round(price.price, 2) }&nbsp;€</span>
            { price.type === 'partial' ? `Teilrechnung vom ${price.invoiceDate} (${price.invoiceId})`: `Anzahlungsrechnung vom ${price.invoiceDate} (${price.invoiceId})` }
          </li>
        );
      });

      PaymentInfoComponents.push(
        <li key="end-sum" class="list-group-item">
          <span class="badge">{ Helper.round(newTotal, 2) }&nbsp;€</span>
          Gesamtbetrag mit allen Abzügen
        </li>
      )
    }

    return (
      <div>
        <h2>Rechnungsdetails</h2>
        <div class="form-group">
          <label>Bearbeiter</label>
          <p>Andreas Francke</p>
        </div>
        <div class="form-group">
          <label>Bauvorhaben</label>
          <p>{ name }</p>
        </div>
        <div class="form-group">
          <label>Kunde</label>
          { CustomerComponent }
        </div>

        <div class="form-group">
          <label>Erstellungsdatum</label>
          <p>{ invoiceDate || moment().format('DD.MM.YYYY') }</p>
        </div>

        <div class="separator"/>
        <h2>Aufgaben</h2>
        { this.renderSectionComponents(sections) }
        <button type="button" class="btn btn-secondary" onClick={this.addSection.bind(this)}>Titel hinzufügen</button>
        
        <div class="separator"/>
        <h2>Zusammenfassung</h2>
        <ul class="list-group">
          <li class="list-group-item">
            <span class="badge">{ totalWageMinutes }</span>
            Zeit
          </li>
          <li class="list-group-item">
            <span class="badge">{ Helper.round(total, 2) } €</span>
            Gesamtbetrag
          </li>

          { PaymentInfoComponents }

          <li class="list-group-item">
            <span class="badge">{ Helper.round(newTotal, 2) } €</span>
            Nettosumme
          </li>
          <li class="list-group-item">
            <span class="badge">{ Helper.round(newTotal + totalNetAmount, 2) } €</span>
            +{ withoutTax ? 0 : tax }%
          </li>

          { profitComponent }
        </ul>

        { SettingsComponent }

        <div class="separator"/>
        <h2>Zahlung</h2>
        <div class="row">
          <div class="form-group col-md-3 col-xs-12">
            <label for="dueDay">Zahlung in Tagen (ohne Skonto)</label>
            <select class="form-control input-sm" ref="dueDay" onChange={this.changeDueDay.bind(this)} value={ dueDay || '' }>
              { this.renderDueDayComponents('no_allowance') }  
            </select>
          </div>
          <div class="form-group col-md-2 col-xs-12">
            <label for="unitId">Zahlungstag</label>
            <p>{ moment().add(dueDay || 1, 'day').format('DD.MM.YYYY') }</p>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-md-3 col-xs-12">
            <label for="dueDay">Zahlung in Tagen (mit Skonto)</label>
            <select class="form-control input-sm" ref="dueDayAllowance" onChange={this.changeDueDayAllowance.bind(this)} value={ dueDayAllowance || '' }>
              { this.renderDueDayComponents('allowance') }  
            </select>
          </div>
          <div class="form-group col-md-2 col-xs-12">
            <label for="allowance">Skonto in %</label>
            <input type="text" ref="allowance" class="form-control" placeholder="Skonto in %" name="allowance" onChange={this.changeAllowance.bind(this)} value={ allowance || '' }/>
          </div>
          { (parseInt(allowance || 0) || 0) && dueDayAllowance ? <div class="form-group col-md-2 col-xs-12">
            <label for="unitId">Zahlungstag</label>
            <p>{ moment().add(dueDayAllowance || 1, 'day').format('DD.MM.YYYY') }</p>
          </div> : '' }
        </div>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="" onChange={this.changeTax.bind(this)} checked={withoutTax ? 'checked' : ''}/>
            ohne Umsatzsteuer
          </label>
        </div>
        
        <br/>
        <br/>
        <div class="btn-toolbar">
          <Link to={ adjust ? `/invoices/printPreview/${invoice._id}` : `/offers/update/${invoice.offer.offerId}` } class="btn btn-unimportant pull-left">Abbrechen</Link>
          { invoice._id && !adjust ? <button type="button" onClick={this.deleteInvoce.bind(this)} class="btn btn-outline-primary pull-left">Entwurf löschen</button> : '' }
          
          { !adjust ? <button type="button" onClick={this.handleSubmit.bind(this, 'printPreview', true)} class="btn btn-action pull-right">Erstellen und zur Druckvorschau</button> : '' }
          <button type="button" onClick={this.handleSubmit.bind(this, (adjust ? 'printPreview' : 'offerUpdate'), false)} class="btn btn-secondary pull-right">{ adjust ? 'Speichern' : 'Entwurf speichern' }</button>
        </div>
        <br/>
        <br/>
      </div>
    );
  }
}