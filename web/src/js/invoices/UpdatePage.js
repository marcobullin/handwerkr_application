import React from 'react';
import InvoiceForm from './components/InvoiceForm';
import { Redirect } from 'react-router-dom';
import InvoiceStore from './stores/InvoiceStore';
import * as InvoiceActions from './actions/InvoiceActions';
import Loading from '../common/Loading';

export default class UpdatePage extends React.Component {
  constructor(props) {
    super(props);

    const { invoiceId } = this.props.match.params;
    
    this.state = {
      fetching: true,
      redirectTo: InvoiceStore.redirectTo
    };

    this.handleChange = this.handleChange.bind(this);

    const adjust = props.location.state && props.location.state.adjust;
    InvoiceActions.getInvoiceById(invoiceId, adjust);
  }

  handleChange() {
    if (!this._isMounted) { return; }

    this.setState({
      fetching: false,
      redirectTo: InvoiceStore.redirectTo
    });
  }

  componentDidMount() {
    this._isMounted = true;
    InvoiceStore.on('change', this.handleChange);
    window.scrollTo(0, 0);
  }

  componentWillUnmount() {
    this._isMounted = false;
    InvoiceStore.removeListener('change', this.handleChange);
    InvoiceStore.reset();
  }

  render() {
    const { fetching, redirectTo } = this.state;
    
    if (fetching) {
      return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px'}}>
          <Loading />
        </div>
      );
    }

    if (redirectTo) {
      return <Redirect to={redirectTo} />;
    }

    return (
      <div>
        <div class="container-fluid">
          <h1>Rechnung bearbeiten</h1>
          <InvoiceForm/>
        </div>
      </div>
    )
  }
}