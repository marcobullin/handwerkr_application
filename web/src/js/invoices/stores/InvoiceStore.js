import { EventEmitter } from 'events';
import dispatcher from '../../dispatcher';
import moment from 'moment';

class InvoiceStore extends EventEmitter {
  constructor() {
    super();
    this.reset();
  }

  reset() {
    this.redirectTo = false;
    this.invoices = [];
    this.invoice = {};
    this.units = [];
    this.pages = 1;
    this.page = 1;
    this.adjust = false;
  }

  getAll() {
    return this.invoices;
  }

  handleActions(action) {
    switch(action.type) {
      case 'DELETED_INVOICE': {
        this.redirectTo = `/offers/update/${this.invoice.offer.offerId}`;
        this.emit('change');
        break;
      }
      case 'FOUND_INVOICE': {
        this.invoice = action.invoice;
        this.invoice.offer.invoices.reverse();

        this.adjust = action.adjust;

        if (action.units) {
          this.units = action.units;
        }

        if (!this.adjust && this.invoice.invoiceYear && this.invoice.invoiceCount) {
          this.redirectTo = `/invoices/printPreview/${this.invoice._id}`;

          if (this.invoice.type === 'percentage') {
            this.redirectTo = `/invoices/printPreview/partialPayment/${this.invoice._id}`;
          }
        }

        this.emit('change');
        break;
      }
      case 'FOUND_INVOICES': {
        this.invoices = action.invoices;
        this.pages = action.pages;
        this.page = action.page;
        this.emit('change');
        break;
      }
      case 'RECEIVE_INVOICES': {
        this.invoices = action.invoices;
        this.pages = action.pages;
        this.page = action.page;
        this.emit('change');
        break;
      }
      case 'CREATED_INVOICE': {
        this.redirectTo = action.redirectTo;
        this.emit('change');
        break;
      }
      case 'UPDATED_INVOICE': {
        this.redirectTo = action.redirectTo;
        this.emit('change');
        break;
      }
      case 'UPDATE_INVOICE_DATE': {
        this.invoice.invoiceDate = action.invoiceDate;
        this.emit('change');
        break;
      }
      case 'INVOICE_ADD_SECTION': {
        if (!this.invoice.sections) {
          this.invoice.sections = [];
        }

        this.invoice.sections.push({});
        this.emit('change');
        break;
      }
      case 'INVOICE_UPDATE_SECTION': {
        this.invoice.sections[action.index] = action.section;
        this.emit('change');
        break;
      }
      case 'INVOICE_DELETE_SECTION': {
        this.invoice.sections.splice(action.index, 1);
        this.emit('change');
        break;
      }
      case 'INVOICE_ADD_SUB_SECTION': {
        if (!this.invoice.sections[action.index].subSections) {
          this.invoice.sections[action.index].subSections = [];
        }

        this.invoice.sections[action.index].subSections.push({});
        this.emit('change');
        break;
      }
      case 'INVOICE_UPDATE_SUB_SECTION': {
        this.invoice.sections[action.sectionIndex].subSections[action.subSectionIndex] = action.subSection;
        this.emit('change');
        break;
      }
      case 'INVOICE_DELETE_SUB_SECTION': {
        this.invoice.sections[action.sectionIndex].subSections.splice(action.subSectionIndex, 1);
        this.emit('change');
        break;
      }
      case 'INVOICE_ADD_POSITION': {
        if (!this.invoice.sections[action.sectionIndex].subSections[action.subSectionIndex].positions) {
          this.invoice.sections[action.sectionIndex].subSections[action.subSectionIndex].positions = [];
        }

        const positions = this.invoice.sections[action.sectionIndex].subSections[action.subSectionIndex].positions;
        const positionNumber = positions.length > 0 ? (Math.max.apply(null, positions.map(pos => pos.positionNumber)) + 1) : 1;

        this.invoice.sections[action.sectionIndex].subSections[action.subSectionIndex].positions.push({
          positionNumber,
          positionMode: 'normal'
        });
        this.emit('change');
        break;
      }
      case 'INVOICE_UPDATE_POSITION': {
        this.invoice.sections[action.sectionIndex]
                  .subSections[action.subSectionIndex]
                  .positions[action.positionIndex] = action.position;
        this.emit('change');
        break;
      }
      case 'INVOICE_DELETE_POSITION': {
        this.invoice.sections[action.sectionIndex]
                  .subSections[action.subSectionIndex]
                  .positions
                  .splice(action.positionIndex, 1);
        this.emit('change');
        break;
      }
      case 'INVOICE_UPDATE_WITHOUT_TAX': {
        this.invoice.withoutTax = action.checked;
        this.emit('change');
        break;
      }
      case 'INVOICE_UPDATE_DUE_DAY': {
        this.invoice.dueDay = action.dueDay;
        this.emit('change');
        break;
      }
      case 'INVOICE_UPDATE_DUE_DAY_ALLOWANCE': {
        this.invoice.dueDayAllowance = action.dueDayAllowance;
        this.emit('change');
        break;
      }
      case 'INVOICE_UPDATE_ALLOWANCE': {
        this.invoice.allowance = action.allowance;
        this.emit('change');
        break;
      }
      case 'INVOICE_MARKED_AS_PAYED': {
        this.invoice.status = 'payed';
        this.emit('change');
        break;
      }
      case 'INVOICE_UPDATE_LAW_TEXT': {
        this.invoice.showLawText = action.checked;
        this.emit('change');
        break;
      }
    }
  }
}

const store = new InvoiceStore;
dispatcher.register(store.handleActions.bind(store));

export default store;