import dispatcher from '../../dispatcher';
import xhttp from '../../xhttp';

export function getAll() {
  xhttp('GET', '/invoices').then(data => {
    dispatcher.dispatch({type: 'RECEIVE_INVOICES', invoices: data.invoices, pages: data.pages, page: data.page});
  });
}

export function getInvoiceDraft(offerId, sectionIndex, isPartialPayment) {
  xhttp('GET', '/invoices/draft/' + offerId + (sectionIndex ? '/' + sectionIndex : '') + (isPartialPayment ? '?isPartialPayment=true' : '')).then(data => {
    dispatcher.dispatch({type: 'FOUND_INVOICE', invoice: data.invoice, units: data.units});
  });
}

export function getInvoiceById(invoiceId, adjust) {
  xhttp('GET', '/invoices/' + invoiceId).then(data => {
    dispatcher.dispatch({type: 'FOUND_INVOICE', invoice: data.invoice, customer: data.customer, units: data.units, adjust: adjust});
  });
}

export function setLawText(checked) {
  dispatcher.dispatch({type: 'INVOICE_UPDATE_LAW_TEXT', checked});
}

export function createInvoice(invoice, redirect) {
  xhttp('POST', '/invoices/create', invoice).then(data => {
    let redirectTo = '';

    switch(redirect) {
      case 'printPreview':
        redirectTo = `/invoices/printPreview/${data.createdInvoice._id}`;
        break;
      case 'printPreviewPartialPayment':
        redirectTo = `/invoices/printPreview/partialPayment/${data.createdInvoice._id}`;
        break;
      case 'invoices':
        redirectTo = '/invoices';
        break;
      case 'offerUpdate':
        redirectTo = `/offers/update/${data.createdInvoice.offer.offerId}`;
        break;  
    }

    dispatcher.dispatch({type: 'CREATED_INVOICE', redirectTo});
  });
}

export function updateInvoice(invoice, data, redirect) {
  xhttp('POST', `/invoices/${invoice._id}`, data).then(() => {
    let redirectTo = '';
    
    switch(redirect) {
      case 'printPreview' : 
        redirectTo = `/invoices/printPreview/${invoice._id}`;
        break;
      case 'invoices':
        redirectTo = '/invoices';
        break;
      case 'offerUpdate':
        redirectTo = `/offers/update/${invoice.offer.offerId}`;
        break;  
    }
    dispatcher.dispatch({type: 'UPDATED_INVOICE', redirectTo});
  });
}

export function searchInvoice(value, filter, page, typeFilter) {
  xhttp('GET', '/invoices/search/' + encodeURIComponent(value) + '?filter=' + filter + '&type=' + typeFilter + '&page=' + (page || 1)).then(data => {
    dispatcher.dispatch({type: 'FOUND_INVOICES', invoices: data.invoices, pages: data.pages, page: data.page});
  });
}

export function deleteInvoice(id) {
  xhttp('POST', '/invoices/delete/' + id).then(() => {
    dispatcher.dispatch({type: 'DELETED_INVOICE'});
  });
}

export function setInvoiceDate(invoiceDate) {
  dispatcher.dispatch({type: 'UPDATE_INVOICE_DATE', invoiceDate});
}

export function addSection() {
  dispatcher.dispatch({type: 'INVOICE_ADD_SECTION'});
}

export function updateSection(index, section) {
  dispatcher.dispatch({type: 'INVOICE_UPDATE_SECTION', index, section});
}

export function deleteSection(index) {
  dispatcher.dispatch({type: 'INVOICE_DELETE_SECTION', index});
}

export function addSubSection(index) {
  dispatcher.dispatch({type: 'INVOICE_ADD_SUB_SECTION', index});
}

export function updateSubSection(sectionIndex, subSectionIndex, subSection) {
  dispatcher.dispatch({type: 'INVOICE_UPDATE_SUB_SECTION', sectionIndex, subSectionIndex, subSection});
}

export function deleteSubSection(sectionIndex, subSectionIndex) {
  dispatcher.dispatch({type: 'INVOICE_DELETE_SUB_SECTION', sectionIndex, subSectionIndex});
}

export function addPosition(sectionIndex, subSectionIndex) {
  dispatcher.dispatch({type: 'INVOICE_ADD_POSITION', sectionIndex, subSectionIndex});
}

export function updatePosition(sectionIndex, subSectionIndex, positionIndex, position) {
  dispatcher.dispatch({type: 'INVOICE_UPDATE_POSITION', sectionIndex, subSectionIndex, positionIndex, position});
}

export function deletePosition(sectionIndex, subSectionIndex, positionIndex) {
  dispatcher.dispatch({type: 'INVOICE_DELETE_POSITION', sectionIndex, subSectionIndex, positionIndex});
}

export function setWithoutTax(checked) {
  dispatcher.dispatch({type: 'INVOICE_UPDATE_WITHOUT_TAX', checked});
}

export function setDueDay(dueDay) {
  dispatcher.dispatch({type: 'INVOICE_UPDATE_DUE_DAY', dueDay});
}

export function setDueDayAllowance(dueDayAllowance) {
  dispatcher.dispatch({type: 'INVOICE_UPDATE_DUE_DAY_ALLOWANCE', dueDayAllowance});
}

export function setAllowance(allowance) {
  dispatcher.dispatch({type: 'INVOICE_UPDATE_ALLOWANCE', allowance});
}

export function markAsPayed(id) {
  xhttp('POST', '/invoices/markAsPayed/' + id).then(() => {
    dispatcher.dispatch({type: 'INVOICE_MARKED_AS_PAYED'});
  });
}