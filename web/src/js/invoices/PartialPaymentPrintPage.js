import React from 'react';
import InvoiceStore from './stores/InvoiceStore';
import * as InvoiceActions from './actions/InvoiceActions';
import { Link } from 'react-router-dom';
import Loading from '../common/Loading';
import moment from 'moment';
import * as Helper from '../helper';
import * as InvoiceComputing from '../offers/computing';

export default class PrintPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fetching: true,
      invoice: InvoiceStore.invoice
    };

    this.handleChange = this.handleChange.bind(this);
    
    InvoiceActions.getInvoiceById(props.match.params.invoiceId);
  }

  componentDidMount() {
    InvoiceStore.on('change', this.handleChange);
    window.scrollTo(0, 0);
  }

  componentWillUnmount() {
    InvoiceStore.removeListener('change', this.handleChange);
    InvoiceStore.reset();
  }

  handleChange() {
    this.setState({
      fetching: false,
      invoice: InvoiceStore.invoice
    });
  }

  markAsPayed() {
    InvoiceActions.markAsPayed(this.state.invoice._id);
  }

  render() {
    const { fetching, invoice } = this.state;

    if (fetching) {
      return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px'}}>
          <Loading />
        </div>
      );
    }

    const { customer, invoiceDate, status, partialPayment } = invoice;

    let customerData = '';
    if (customer) {
      let title = '';
      switch (customer.title) {
        case 'mrs': title = 'Frau '; break;
        case 'mr': title = 'Herr '; break;
      }

      customerData = <p class="print-address pull-left">
        { title + customer.name }<br/>
        { customer.street }<br/>
        { customer.zip } { customer.city }
      </p>
    }

    const netPrice = InvoiceComputing.getSectionsPrice(invoice.sections);
    const total = InvoiceComputing.getPartialPaymentAmount(netPrice, partialPayment);
    const netAmount = InvoiceComputing.getSectionsNetAmount(total, invoice.withoutTax ? 0 : invoice.tax);
    const invoicePrice = total + netAmount;
    const allowanceAmount = InvoiceComputing.getAllowanceAmount(invoicePrice, invoice.allowance);
    const invoicePriceWithAllowance = invoicePrice - allowanceAmount;

    return (
      <div class="print">
        <img src="/images/Fliesenleger-Logo-240x70.png"/>

        <div class="print-content">
          <h1 class="print-headline">Druckvorschau</h1> 
          <Link to={'/offers/update/' + invoice.offer.offerId} class="btn btn-secondary print-cancel">Zum Angebot</Link>&nbsp;
          <Link to={'/invoices'} class="btn btn-secondary print-cancel">Alle Rechnungen</Link>&nbsp;
          <a href="javascript:window.print();" class="btn btn-action print-cancel">Drucken</a>&nbsp;
          { status !== 'payed' ? <button type="button" onClick={ this.markAsPayed.bind(this) } class="btn btn-action print-cancel">Geld erhalten</button> : <strong>ist BEZAHLT</strong> }

          <div style={{overflow: 'hidden'}}>
            { customerData }
            <p class="pull-right">
              Fliesenlegermeister Andreas Francke<br/>
              Emseloher Schulgasse 9<br/>
              06542 Allstedt<br/>
              Tel.: 034659/61502<br/>
              Fax.: 034659/61503
            </p>
          </div>
          
          <table class="print-positions">
            <thead>
              <tr class="print-heading">
                <th colSpan="2"><h3>Anzahlungsrechnung: { invoice.invoiceId }</h3></th>
                <th><h4 class="pull-right">vom: { moment(invoiceDate, 'DD.MM.YYYY').format('DD.MM.YYYY') }</h4></th>
              </tr>
              <tr class="print-heading">
                <th colSpan="2">Bauvorhaben: { invoice.name }</th>
                <th>Kd-Nr.: { (customer ? customer.customerId : '') }</th>
              </tr>
              <tr class="print-heading">
                <th>Position</th>
                <th>Anzahlung</th>
                <th>Betrag</th>
              </tr>
            </thead>
            <tbody>
            <tr>
              <td>1</td>
              <td>{ `Anzahlung von ${partialPayment} % vom Gesamtbetrag (${ Helper.round(netPrice, 2) } €) nach erfolgter Materiallieferung und Arbeitsbeginn.` }</td>
              <td>{ `${Helper.round(total, 2)} €` }</td>
            </tr>
            </tbody>
          </table>


          <table class="print-summary">
            <thead>
              <tr>
                <th>Titelzusammenstellung</th>
                <th>Preise</th>
              </tr>
            </thead>

            <tbody>
              <tr>
                <td>Titel: 1 Anzahlung</td>
                <td>
                  { Helper.round(total, 2) }&nbsp;€
                </td>
              </tr>
              <tr class="print-netto-sum"><td>Nettosumme</td><td>{ Helper.round(total, 2) }&nbsp;€</td></tr>
              <tr><td>+ { invoice.withoutTax ? 0 : invoice.tax } % USt</td><td>{ Helper.round(netAmount, 2) }&nbsp;€</td></tr>
              { invoice.withoutTax ? <tr><td>Die Steuerschuld trägt gem. §13b UStG. der Leistungsempfänger Umsatzsteuer {invoice.tax} %</td><td></td></tr> : null }
              <tr><td>Steuer-Nr.: 11822040758 beim Finanzamt Eisleben</td><td></td></tr>
              <tr class="print-sum"><td>Rechnungsbetrag</td><td>{ Helper.round(invoicePrice, 2) }&nbsp;€</td></tr>
              { invoice.dueDateAllowance ? <tr><td colSpan="2">Bei Zahlungseingang bis zum { invoice.dueDateAllowance } abzüglich { invoice.allowance } % Skonto = {Helper.round(allowanceAmount, 2)}&nbsp;€ vom Rechnungsbetrag, ergibt einen zu zahlenden Betrag von {Helper.round(invoicePriceWithAllowance, 2)}&nbsp;€.</td><td></td></tr> : null }
              <tr><td colSpan="2">Zahlen Sie bitte bis zum { invoice.dueDate }{ invoice.dueDateAllowance ? ', ohne Skontoabzug.' : '.' }</td><td></td></tr>
            </tbody>
          </table>
        </div>

        <div class="print-payment">
          <p class="print-payinfo">Bitte überweisen Sie den Betrag auf eines der folgenden Konten</p>
          <p>
            IBAN: DE29 8008 0000 0848 4260 00&nbsp;&nbsp;&nbsp;BIC: DRESDEFF800&nbsp;&nbsp;&nbsp;Commerzbank<br/>
            IBAN: DE77 8006 3558 0005 5681 53&nbsp;&nbsp;&nbsp;BIC: GENODEF1SGH&nbsp;&nbsp;&nbsp;Volksbank
          </p>
        </div>
      </div>
    );
  }
}