import { EventEmitter } from 'events';
import dispatcher from '../../dispatcher';

class CustomerStore extends EventEmitter {
  constructor() {
    super();
    this.reset();
  }

  reset() {
    this.customers = [];
    this.redirect = false;
    this.pages = 1;
    this.page = 1;
    this.customer = {
      _id: '',
      customerId: '', 
      firstname: '', 
      lastname: '', 
      name: '',
      street: '', 
      zip: '', 
      city: '',
      mail: '',
      phone: ''
    };
  }

  getCustomer(customerId) {
    return this.customers.filter(entry => entry.customerId === customerId)[0];
  }

  getAll() {
    return this.customers;
  }

  handleActions(action) {
    switch(action.type) {
      case 'CREATED_CUSTOMER': {
        this.redirect = true;
        setTimeout(() => {this.emit('change');}, 0);
        break;
      }
      case 'UPDATED_CUSTOMER': {
        this.redirect = true;
        setTimeout(() => {this.emit('change');}, 0);
        break;
      }
      case 'DELETED_CUSTOMER': {
        this.customers.splice(action.customerIndex, 1);
        this.emit('change');
        break;
      }
      case 'FOUND_CUSTOMER': {
        this.customer = action.customer;
        this.emit('change');
        break;
      }
      case 'FOUND_CUSTOMERS': {
        this.customers = action.customers;
        this.pages = action.pages;
        this.page = action.page;
        this.emit('change');
        break;
      }
      case 'RECEIVE_CUSTOMERS': {
        this.customers = action.customers;
        this.pages = action.pages;
        this.page = action.page;
        this.emit('change');
        break;
      }
      case 'CHANGE_CUSTOMER': {
        this.customer = Object.assign(this.customer, action.customer);
        this.emit('change');
      }
    }
  }
}

const customerStore = new CustomerStore;
dispatcher.register(customerStore.handleActions.bind(customerStore));

export default customerStore;