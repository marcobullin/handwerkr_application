import React from 'react';
import CustomerForm from './components/CustomerForm';
import CustomerStore from './stores/CustomerStore';
import { Redirect } from 'react-router-dom';

export default class CreatePage extends React.Component {
  constructor() {
    super();
    this.state = {
      redirect: CustomerStore.redirect
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange() {
    this.setState({redirect: CustomerStore.redirect});
  }

  componentDidMount() {
    CustomerStore.on('change', this.handleChange);
  }

  componentWillUnmount() {
    CustomerStore.removeListener('change', this.handleChange);
    CustomerStore.reset();
  }

  render() {
    const { redirect } = this.state;
    
    if (redirect) {
      return <Redirect to="/customers" />;
    }

    return (
      <div>
        <div class="container-fluid">
          <h1>Neuen Kunden anlegen</h1>
          <CustomerForm />
        </div>
      </div>
    );
  }
}