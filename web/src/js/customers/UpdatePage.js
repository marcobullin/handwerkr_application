import React from 'react';
import CustomerForm from './components/CustomerForm';
import CustomerStore from './stores/CustomerStore';
import { Redirect } from 'react-router-dom';
import * as CustomerActions from './actions/CustomerActions';
import Loading from '../common/Loading';

export default class UpdatePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fetching: true,
      redirect: CustomerStore.redirect,
      customer: CustomerStore.customer
    };

    this.handleOnChange = this.handleOnChange.bind(this);
    CustomerActions.getCustomer(this.props.match.params.customerId);
  }

  handleOnChange() {
    this.setState({
      fetching: false,
      redirect: CustomerStore.redirect,
      customer: CustomerStore.customer
    });
  }

  componentDidMount() {
    CustomerStore.on('change', this.handleOnChange);
  }

  componentWillUnmount() {
    CustomerStore.removeListener('change', this.handleOnChange);
    CustomerStore.reset();
  }

  render() {
    const { fetching, redirect, customer } = this.state;

    if (fetching) {
      return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px'}}>
          <Loading />
        </div>
      );
    }

    if (redirect) {
      return <Redirect to="/customers" />;
    }
    
    return (
      <div>
        <div class="container-fluid">
          <h1>Kunden bearbeiten</h1>
          <CustomerForm />
        </div>
      </div>
    );
  }
}