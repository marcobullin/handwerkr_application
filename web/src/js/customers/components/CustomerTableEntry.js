import React from 'react';
import * as CustomerActions from '../actions/CustomerActions';
import { Link } from 'react-router-dom';

export default class CustomerTableEntry extends React.Component {
  deleteCustomer() {
    var answer = confirm('Möchtest du wirklich den Kunden "' + this.props.customer.name + '" löschen?');
    
    if (answer === true) {
      CustomerActions.deleteCustomer(this.props.customer._id, this.props.index);
    }
  }

  render() {
    const { customerId, name, street, zip, city } = this.props.customer;
    
    return (
      <tr>
        <td>{ customerId }</td>
        <td>{ name }</td>
        <td>{ street }</td>
        <td>{ zip }</td>
        <td>{ city }</td>
        <td>
          <Link to={'/customers/update/' + customerId} class="btn btn-secondary btn-sm"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></Link>&nbsp;
          <button type="button" onClick={this.deleteCustomer.bind(this)} data-id={customerId} class="btn btn-outline-primary btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
        </td>
      </tr>
    );
  }
}