import React from 'react';
import CustomerStore from '../stores/CustomerStore';
import * as CustomerActions from '../actions/CustomerActions';
import SuggestionItem from '../../common/SuggestionItem';

export default class CustomerSearch extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showSuggestions: false,
      suggestions: CustomerStore.customers,
      suggestionIndex: 0
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange() {
    this.setState({
      suggestions: CustomerStore.customers
    });
  }

  componentDidMount() {
    this._isMounted = true;
    CustomerStore.on('change', this.handleChange);
  }

  componentWillUnmount() {
    this._isMounted = false;
    CustomerStore.removeListener('change', this.handleChange);
  }

  searchCustomer(e) {
    const value = this.refs.customerName.value;

    var showSuggestions = false;
    if (value.length >= 2) {
      CustomerActions.searchCustomer(value);
      showSuggestions = true;
    }

    this.setState({
      showSuggestions: showSuggestions
    });
  }

  changeActivSuggestion(e) {
    const length = this.state.suggestions.length;

    if (e.keyCode === 38) {
      var index = this.state.suggestionIndex - 1;
      if (index < 0) {
        index = length - 1;
      }

      this.setState({
        suggestionIndex: index
      });

      return;
    } else if (e.keyCode === 40) {
      var index = this.state.suggestionIndex + 1;
      if (index >= length) {
        index = 0;
      }

      this.setState({
        suggestionIndex: index
      });

      return;
    } else if (e.keyCode === 27) {
      this.setState({
        showSuggestions: false,
        suggestionIndex: 0
      });

      return;
    } else if (e.keyCode === 13) {
      this.selectSuggestion(this.state.suggestionIndex);
      return;
    }
  }

  selectSuggestion(index) {
    this.setState({
      showSuggestions: false,
      suggestionIndex: 0
    });

    const { suggestions } = this.state;

    this.props.selectCustomer(suggestions[index]);
  }

  onBlur() {
    window.setTimeout(() => {
      if (this._isMounted && this.showSuggestions) {
        this.setState({
          showSuggestions: false,
          suggestionIndex: 0
        });
      }
    }, 500);
  }

  createCustomer(e) {
    e.preventDefault();
    this.props.createCustomer();
  }

  render() {
    const { showSuggestions, suggestions } = this.state;

    var suggestionComponents = '';
    if (showSuggestions) {
      suggestionComponents = suggestions.slice(0, 10).map((customer,i) => { 
        return <SuggestionItem key={customer._id} selectSuggestion={this.selectSuggestion.bind(this)} id={i} title={ customer.name + (customer.city ? ' (' + customer.city + ')' : '') } selected={i === this.state.suggestionIndex}/>
      });
    }

    return (
      <div>
        <div class="form-group">
          <input type="text" ref="customerName" autoComplete="off" onChange={this.searchCustomer.bind(this)} onKeyUp={this.changeActivSuggestion.bind(this)} onBlur={this.onBlur.bind(this)} class="form-control" placeholder="Kunde suchen"/>
          <div class="list-group">
            { suggestionComponents }
          </div>
          <button type="button" class="btn btn-secondary" onClick={this.createCustomer.bind(this)}>Neuer Kunde</button>
        </div>
      </div>
    );
  }
}