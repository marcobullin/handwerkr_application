import React from 'react';
import * as CustomerActions from '../actions/CustomerActions';

export default class CustomerModal extends React.Component {
  cancel() {
    this.props.cancelCustomerModal();
  }

  save() {
    const firstname = this.refs.firstname.value;
    const lastname = this.refs.lastname.value;
    const street = this.refs.street.value;
    const zip = this.refs.zip.value;
    const city = this.refs.city.value;
    const mail = this.refs.mail.value;
    const phone = this.refs.phone.value;
    const title = this.refs.title.value;

    CustomerActions.createCustomer({title, firstname, lastname, street, zip, city, mail, phone});
  }

  render() {
    return (
      <div>
        <div class="modal fade in" aria-labelledby="myModalLabel" style={{display: 'block', background: 'rgba(0, 0, 0, 0.5)'}}>
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" onClick={this.cancel.bind(this)} data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Kunde anlegen</h4>
              </div>
              <div class="modal-body">
              <div class="row">
                  <div class="form-group col-md-2 col-xs-12">
                    <label for="title">Anrede</label>
                    <select class="form-control input-sm" ref="title">
                      <option value="">Anrede</option>
                      <option value="mrs">Frau</option>
                      <option value="mr">Herr</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-xs-12 form-group">
                    <label for="firstname">Vorname</label>
                    <input type="text" ref="firstname" class="form-control input-sm" placeholder="Vorname" />
                  </div>
                  <div class="col-md-6 col-xs-12 form-group">
                    <label for="lastname">Nachname</label>
                    <input type="text" ref="lastname" class="form-control input-sm" placeholder="Nachname"/>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12 form-group">
                    <label for="street">Straße und Hausnummer</label>
                    <input type="text" ref="street" class="form-control input-sm" placeholder="Straße"/>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4 col-xs-12 form-group">
                    <label for="zip">PLZ</label>
                    <input type="text" ref="zip" class="form-control input-sm" placeholder="PLZ"/>
                  </div>
                  <div class="col-md-8 col-xs-12 form-group">
                    <label for="city">Ort</label>
                    <input type="text" ref="city" class="form-control input-sm" placeholder="Ort"/>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12 form-group">
                    <label for="mail">E-Mail</label>
                    <input type="text" ref="mail" class="form-control input-sm" placeholder="E-Mail"/>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 col-xs-12 form-group">
                    <label for="phone">Telefonnummer</label>
                    <input type="text" ref="phone" class="form-control input-sm" placeholder="Telefonnummer"/>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" onClick={this.cancel.bind(this)} class="btn btn-unimportant pull-left" data-dismiss="modal">Abbrechen</button>
                <button type="button" onClick={this.save.bind(this)} class="btn btn-action">Erstellen</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}