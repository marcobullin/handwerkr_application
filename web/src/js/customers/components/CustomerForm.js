import React from 'react';
import * as CustomerActions from '../actions/CustomerActions';
import { Link } from 'react-router-dom';
import CustomerStore from '../stores/CustomerStore';

export default class CustomerForm extends React.Component {
  constructor() {
    super();

    this.state = {
      customer: CustomerStore.customer
    }

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange() {
    if (!this._isMounted) {
      return;
    }
    
    this.setState({
      customer: CustomerStore.customer
    });
  }

  componentDidMount() {
    this._isMounted = true;
    CustomerStore.on('change', this.handleChange);
  }

  componentWillUnmount() {
    this._isMounted = false;
    CustomerStore.removeListener('change', this.handleChange);
    CustomerStore.reset();
  }

  handleSubmit() {
    const _id = this.refs._id.value;
    const customerId = this.state.customer.customerId;
    const title = this.refs.title.value;
    const firstname = this.refs.firstname.value;
    const lastname = this.refs.lastname.value;
    const street = this.refs.street.value;
    const zip = this.refs.zip.value;
    const city = this.refs.city.value;
    const mail = this.refs.mail.value;
    const phone = this.refs.phone.value;

    if (_id) {
      return CustomerActions.updateCustomer(_id, {customerId, title, firstname, lastname, street, zip, city, mail, phone});
    }

    CustomerActions.createCustomer({title, firstname, lastname, street, zip, city, mail, phone});
  }

  changeCustomerData() {
    const title = this.refs.title.value;
    const firstname = this.refs.firstname.value;
    const lastname = this.refs.lastname.value;
    const street = this.refs.street.value;
    const zip = this.refs.zip.value;
    const city = this.refs.city.value;
    const mail = this.refs.mail.value;
    const phone = this.refs.phone.value;

    CustomerActions.changeCustomerData({title, firstname, lastname, street, zip, city, mail, phone});
  }

  render() {
    const { customer } = this.state;
    const { _id, title, firstname, lastname, street, zip, city, mail, phone } = customer;

    return (
      <div>
        <input type="hidden" ref="_id" defaultValue={_id}/>
        <div class="well">
          <div class="row">
            <div class="form-group col-md-2 col-xs-12">
              <label for="title">Anrede</label>
              <select class="form-control input-sm" ref="title" onChange={this.changeCustomerData.bind(this)} value={ title || '' }>
                <option value="">Anrede</option>
                <option value="mrs">Frau</option>
                <option value="mr">Herr</option>
              </select>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-xs-12 form-group">
              <label for="firstname">Vorname</label>
              <input type="text" ref="firstname" class="form-control input-sm" placeholder="Vorname" onChange={this.changeCustomerData.bind(this)} value={firstname} />
            </div>
            <div class="col-md-6 col-xs-12 form-group">
              <label for="lastname">Nachname</label>
              <input type="text" ref="lastname" class="form-control input-sm" placeholder="Nachname" onChange={this.changeCustomerData.bind(this)} value={lastname}/>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 col-xs-12 form-group">
              <label for="street">Straße und Hausnummer</label>
              <input type="text" ref="street" class="form-control input-sm" placeholder="Straße" onChange={this.changeCustomerData.bind(this)} value={street}/>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4 col-xs-12 form-group">
              <label for="zip">PLZ</label>
              <input type="text" ref="zip" class="form-control input-sm" placeholder="PLZ" onChange={this.changeCustomerData.bind(this)} value={zip}/>
            </div>
            <div class="col-md-8 col-xs-12 form-group">
              <label for="city">Ort</label>
              <input type="text" ref="city" class="form-control input-sm" placeholder="Ort" onChange={this.changeCustomerData.bind(this)} value={city}/>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 col-xs-12 form-group">
              <label for="mail">E-Mail</label>
              <input type="text" ref="mail" class="form-control input-sm" placeholder="E-Mail" onChange={this.changeCustomerData.bind(this)} value={mail}/>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 col-xs-12 form-group">
              <label for="phone">Telefonnummer</label>
              <input type="text" ref="phone" class="form-control input-sm" placeholder="Telefonnummer" onChange={this.changeCustomerData.bind(this)} value={phone}/>
            </div>
          </div>
        </div>
        <Link to="/customers" class="btn btn-unimportant pull-left">Abbrechen</Link>
        <button type="button" onClick={this.handleSubmit.bind(this)} class="btn btn-action pull-right">Speichern</button>
      </div>
    );
  }
}