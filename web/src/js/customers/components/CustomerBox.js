import React from 'react';

export default class CustomerBox extends React.Component {
  removeCustomer(e) {
    e.preventDefault();
    this.props.removeCustomer();
  }

  render() {
    const { customer } = this.props;

    return (
      <div>
        <div class="well well-sm">
          <p>{ customer.name }</p>
          <p>{ customer.street }</p>
          <p>{ customer.zip + ' ' + customer.city}</p>
          <button onClick={this.removeCustomer.bind(this)} class="btn btn-outline-primary">Entfernen</button>
        </div>
      </div>
    );
  }
}