import dispatcher from '../../dispatcher';
import xhttp from '../../xhttp';

export function createCustomer(customer) {
  xhttp('POST', '/customers/create', customer).then(createdCustomer => {
    dispatcher.dispatch({type: 'CREATED_CUSTOMER', createdCustomer});
  });
}

export function updateCustomer(customerId, customer) {
  xhttp('POST', '/customers/' + customerId, customer).then(() => {
    dispatcher.dispatch({type: 'UPDATED_CUSTOMER'});
  });
}

export function deleteCustomer(customerId, customerIndex) {
  xhttp('POST', '/customers/delete/' + customerId).then(() => {
    dispatcher.dispatch({type: 'DELETED_CUSTOMER', customerIndex});
  });
}

export function searchCustomer(value, page) {
  xhttp('GET', '/customers/search/' + encodeURIComponent(value) + '?page=' + (page || 1)).then(data => {
    dispatcher.dispatch({type: 'FOUND_CUSTOMERS', customers: data.customers, pages: data.pages, page: data.page});
  });
}

export function getCustomer(customerId) {
  xhttp('GET', '/customers/' + customerId).then(customer => {
    dispatcher.dispatch({type: 'FOUND_CUSTOMER', customer: customer});
  });
}

export function getAll() {
  xhttp('GET', '/customers').then(data => {
    dispatcher.dispatch({type: 'RECEIVE_CUSTOMERS', customers: data.customers, pages: data.pages, page: data.page});
  });
}

export function changeCustomerData(customer) {
  dispatcher.dispatch({type: 'CHANGE_CUSTOMER', customer});
}