import React from 'react';
import CustomerTableEntry from './components/CustomerTableEntry';
import CustomerStore from './stores/CustomerStore';
import * as CustomerActions from './actions/CustomerActions';
import CustomerForm from './components/CustomerForm';
import { Link } from 'react-router-dom';
import Loading from '../common/Loading';

export default class OverviewPage extends React.Component {
  constructor() {
    super();

    this.state = {
      fetching: true,
      customers: CustomerStore.getAll(),
      pages: CustomerStore.pages,
      page: CustomerStore.page
    };

    this.handleChange = this.handleChange.bind(this);
    CustomerActions.getAll();
  }

  componentWillMount() {
    CustomerStore.on('change', this.handleChange);
  }

  componentWillUnmount() {
    CustomerStore.removeListener('change', this.handleChange);
    CustomerStore.reset();
  }

  handleChange() {
    this.setState({
      fetching: false,
      customers: CustomerStore.getAll(),
      pages: CustomerStore.pages,
      page: CustomerStore.page
    });
  }

  searchCustomer(e) {
    CustomerActions.searchCustomer(e.target.value, 1);
  }

  changePage(page, e) {
    e.preventDefault();

    CustomerActions.searchCustomer(this.refs.search.value, page);
  }

  render() {
    const { fetching, customers, pages, page } = this.state;

    if (fetching) {
      return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px'}}>
          <Loading />
        </div>
      );
    }

    const CustomerComponents = customers.map((customer, i) => { return <CustomerTableEntry key={customer._id} index={i} customer={customer} /> });

    let PaginationComponents = [];
    for (let i = 1; i <= parseInt(pages); i++) {
      PaginationComponents.push(<li key={i} class={ i === parseInt(page) ? 'active' : ''}><a href="#" onClick={this.changePage.bind(this, i)}>{ i }</a></li>);
    }

    return (
      <div>
        <div class="container-fluid">
          <h1>Kunden</h1>
          <div class="panel panel-default">
            <div class="panel-heading">Alle Kunden</div>
            <div class="panel-body">
              <input type="text" ref="search" onChange={this.searchCustomer.bind(this)} class="form-control" placeholder="Kunden suchen"/>
              <br/>
              <Link to="/customers/create" class="btn btn-action">Kunden anlegen</Link>
            </div>
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Straße</th>
                    <th>PLZ</th>
                    <th>Ort</th>
                    <th>Aktion</th>
                  </tr>
                </thead>
                <tbody>
                  { CustomerComponents }
                </tbody>
              </table>
            </div>
          </div>
          <nav aria-label="Page navigation" style={{textAlign: 'center'}}>
            <ul class="pagination">
              <li>
                <a href="#" onClick={this.changePage.bind(this, parseInt(page)-1 || 1 )} aria-label="Previous">
                  <span aria-hidden="true">&laquo;</span>
                </a>
              </li>
              { PaginationComponents }
              <li>
                <a href="#" onClick={this.changePage.bind(this, (parseInt(page) + 1 <= pages) ? (parseInt(page) + 1) : pages )} aria-label="Next">
                  <span aria-hidden="true">&raquo;</span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    );
  }
}