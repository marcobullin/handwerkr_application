import React from 'react';
import OfferStore from './stores/OfferStore';
import * as OfferActions from './actions/OfferActions';
import { Link } from 'react-router-dom';
import Loading from '../common/Loading';
import moment from 'moment';
import * as Helper from '../helper';
import * as OfferComputing from './computing';

export default class PrintPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fetching: true,
      offer: OfferStore.offer
    };

    this.handleChange = this.handleChange.bind(this);
    OfferActions.getOffer(props.match.params.offerId);
  }

  componentDidMount() {
    this._isMounted = true;
    OfferStore.on('change', this.handleChange);
    window.scrollTo(0, 0);
  }

  componentWillUnmount() {
    this._isMounted = false;
    OfferStore.removeListener('change', this.handleChange);
    OfferStore.reset();
  }

  handleChange() {
    if (!this._isMounted) { return; }
    
    this.setState({
      fetching: false,
      offer: OfferStore.offer
    });
  }

  render() {
    const { fetching, offer } = this.state;
    
    if (fetching) {
      return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px'}}>
          <Loading />
        </div>
      );
    }

    const { customer, offerDate, partialPayment } = offer;
    const rows = [];
    const PositionElements = offer.sections ? offer.sections.map((section, j) => {
      rows.push(
        <tr key={ "section_" + j} class="print-section">
          <td><strong>Titel: { j+1 }</strong></td>
          <td><strong>{ section.title }</strong></td>
          <td></td>
          <td></td>
        </tr>
      );

      section.subSections ? section.subSections.map((subSection, x) => {
        if (subSection.title) {
          rows.push(
            <tr key={ "subsection_" + j + '_' + x} class="print-sub-section">
              <td></td>
              <td><i>- { subSection.title } -</i></td>
              <td></td>
              <td></td>
            </tr>
          );
        }

        subSection.positions ? subSection.positions.map((position, i) => {
          let lastFieldEntry = Helper.round(OfferComputing.getPositionTotal(position.positionNewPrice || position.positionPrice, position.positionQuantity), 2) + ' €';
          if (position.positionMode === 'eventually') {
            lastFieldEntry = 'Eventual';
          } else if (position.positionMode === 'alternative') {
            lastFieldEntry = 'Alternativ';
          }

          let special = '';
          if (position.positionType === 'calculation') {
            special = <p><br/>(Lohn: { (parseFloat(position.positionNewWagePortion || position.positionWagePortion) || 0).toFixed(2) + ' €'}, Material: { (parseFloat(position.positionNewMaterialPortion || position.positionMaterialPortion) || 0).toFixed(2) + ' €' })</p>
          }
          
          let descriptionComponent = '';
          if (position.positionDescription) {
            descriptionComponent = <p>{ position.positionDescription }<br/></p>
          }

          rows.push(
            <tr class="print-position" key={ "position_" + j + '_' + x + '_' + i}>
              <td>{ position.positionNumber }</td>
              <td>
                <p>{ position.positionName }</p>
                { descriptionComponent }
                <p><strong>Menge: ca. { position.positionQuantity } { position.positionUnit }</strong></p>
                { special }
              </td>
              <td>{ Helper.round(position.positionNewPrice ? position.positionNewPrice : position.positionPrice, 2) }&nbsp;€</td>
              <td>{ lastFieldEntry }</td>
            </tr>
          );
        }) : '';
      }) : '';


      const sumWithoutDiscount = OfferComputing.getSectionPriceWithoutDiscount(section);
      const sum = OfferComputing.getSectionPrice(section);

      rows.push(      
        <tr key={ "sum_without_discount_" + j} class="print-sub-section-sum">
          <td></td>
          <td><strong>Summe { section.title }</strong></td>
          <td></td>
          <td><strong>{ Helper.round(sumWithoutDiscount || sum, 2) }&nbsp;€</strong></td>
        </tr>
      );

      if (section.discount) {
        rows.push(      
          <tr key={ "sum_discount_" + j} class="print-sub-section-discount">
            <td></td>
            <td><strong>Angebot { section.discount }&nbsp;%</strong></td>
            <td></td>
            <td class="print-sub-section-discount-value"><strong>-{ Helper.getDiscountAmount(sumWithoutDiscount, section.discount) }&nbsp;€</strong></td>
          </tr>
        );
        rows.push(      
          <tr key={ "sum_" + j} class="print-sub-section-sum">
            <td></td>
            <td><strong>Gesamt { section.title }</strong></td>
            <td></td>
            <td><strong>{ Helper.round(sum, 2) }&nbsp;€</strong></td>
          </tr>
        );
      }
    }) : '';

    const SummeryElements = offer.sections ? offer.sections.map((section, i) => {
      const sum = OfferComputing.getSectionPrice(section);
      
      return (
        <tr key={i}>
          <td>Titel: { (i + 1)} { section.title }</td>
          <td>
            { Helper.round(sum, 2) }&nbsp;€
          </td>
        </tr>
      );
    }) : '';

    let headline = 'Sehr geehrte Damen und Herren,';
    if (customer) {
      switch (customer.title) {
        case 'mrs': headline = `Sehr geehrte Frau ${customer.lastname},`; break;
        case 'mr': headline = `Sehr geehrter Herr ${customer.lastname},`; break;
      }
    }

    let introductionText = '';
    if (offer.showIntroduction) {
      introductionText = <div class="print-introduction">
        <h3>{ headline }</h3>
        <p>
          wir bedanken uns für Ihre Anfrage und unterbreiten Ihnen beiliegendes Angebot, an das wir uns einen Monat gebunden halten. Die angebotenen Leistungen und Materialien entsprechen dem allgemeinen anerkannten Stand der Technik. Wir hoffen, dass unser Angebot Sie durch sein ausgewogenes Preis-Leistungsverhältnis überzeugen wird. Bei Angebotsbestätigung bitten wir Sie, uns ein Exemplar per E-Mail zurückzusenden, oder Sie informieren uns telefonisch oder per Fax. Der Beginn der von uns angegebenen Leistungszeit (Baubeginn) ist ein vorläufig festgelegter Termin, der sich durch verschiedene Einflüsse (z.B. Liefertermine, Baustellenverzug vorgehender Baustellen, Schlechtwetter usw.) verschieben könnte. Bei Auftreten dieser Situation setzen wir uns vorher mit Ihnen in Verbindung. Eventuelle Fragen beantworten wir gerne und auch für eine Beratung stehen wir Ihnen natürlich zur Verfügung.
        </p>
        
        <p>
          <strong>Bemerkung:</strong><br/>
          Schneidereste der bauseits gelieferten Fliesen werden nicht von uns entsorgt.
        </p>
      </div>
    }

    let mfgText = '';
    if (offer.showMfg) {
      mfgText = <div class="print-mfg">
        <p>Mit freundlichen Grüßen</p>
        <p>Ihr Handwerksbetrieb</p>
      </div>
    }

    let lawText = '';
    if (offer.showLawText) {
      const netWage = Helper.getNetWage(offer.sections);
      const ust = Helper.getUst(netWage, offer.tax);
      const grossWage = ust + netWage;

      lawText = <p>
        Gemäß §35a Abs.2 S.1 EStG können Sie bei Renovierungs- oder Modernisierungsarbeiten 20 % des Lohnwertes einer Rechnung als Steuerbonus geltend machen. Der Nettolohnwert der Rechnung beläuft sich auf { netWage.toFixed(2) } €.<br/>
        Die anteilige USt ({ offer.tax } %) des Lohnwertes beträgt { ust.toFixed(2) } €.<br/>
        Damit ergibt sich ein Bruttolohnwert von { grossWage.toFixed(2) } €.
      </p>
    }

    let partialPaymentText = '';
    if (partialPayment) {
      partialPaymentText = <p><strong>Hinweis</strong>: Bei erfolgter Materiallieferung und Arbeitsbeginn wird eine Anzahlung von <strong>{ partialPayment } %</strong> des Gesamtbetrags fällig. Hierfür erhalten Sie eine separate Rechnung.</p>;
    }

    let customerData = '';
    if (customer) {
      let title = '';
      switch (customer.title) {
        case 'mrs': title = 'Frau '; break;
        case 'mr': title = 'Herr '; break;
      }

      customerData = <p class="print-address pull-left">
        { title + customer.name }<br/>
        { customer.street }<br/>
        { customer.zip } { customer.city }
      </p>
    }

    const netPrice = OfferComputing.getSectionsPrice(offer.sections);
    const netAmount = OfferComputing.getSectionsNetAmount(netPrice, offer.withoutTax ? 0 : offer.tax);
    const offerPrice = netPrice + netAmount;

    return (
      <div class="print">
        <img src="/images/Fliesenleger-Logo-240x70.png"/>

        <div class="print-content">
          <h1 class="print-headline">Druckvorschau</h1> 
          <Link to={'/offers/update/' + offer.offerId} class="print-cancel btn btn-secondary">Zum Angebot</Link>&nbsp;
          <Link to={'/offers'} class="btn btn-secondary print-cancel">Alle Angebote</Link>&nbsp;
          <a href="javascript:window.print();" class="btn btn-action print-cancel">Drucken</a>&nbsp;

          <div style={{overflow: 'hidden'}}>
            { customerData }
            <p class="pull-right">
              Fliesenlegermeister Andreas Francke<br/>
              Emseloher Schulgasse 9<br/>
              06542 Allstedt<br/>
              Tel.: 034659/61502<br/>
              Fax.: 034659/61503
            </p>
          </div>
          
          { introductionText }
          { mfgText }

          <table>
            <thead>
              <tr class="print-heading">
                <th colSpan="2"><h3>Angebot: { offer.offerId }</h3></th>
                <th colSpan="2"><h4 class="pull-right">vom: { moment(offerDate, 'DD.MM.YYYY').format('DD.MM.YYYY') }</h4></th>
              </tr>
              <tr class="print-heading">
                <th colSpan="2">Bauvorhaben: { offer.name }</th>
                <th>Kd-Nr.: { (customer ? customer.customerId : '') }</th>
                <th class="print-pageNumber"></th>
              </tr>
              <tr class="print-heading">
                <th>Position</th>
                <th>Bezeichnung</th>
                <th>Einzelpreis</th>
                <th>Gesamtpreis</th>
              </tr>
            </thead>
            <tbody>
              { rows }
            </tbody>
          </table>


          <table class="print-summary">
            <thead>
              <tr>
                <th>Titelzusammenstellung</th>
                <th>Preise</th>
              </tr>
            </thead>

            <tbody>
              { SummeryElements }
              <tr class="print-netto-sum"><td>Nettosumme</td><td>{ netPrice.toFixed(2) }&nbsp;€</td></tr>
              <tr><td>+ { offer.withoutTax ? 0 : offer.tax } % USt</td><td>{ netAmount.toFixed(2) }&nbsp;€</td></tr>
              { offer.withoutTax ? <tr><td>Die Steuerschuld trägt gem. §13b UStG. der Leistungsempfänger Umsatzsteuer { offer.tax } %</td><td></td></tr> : null }
              <tr class="print-sum"><td>Angebotssumme</td><td>{ offerPrice.toFixed(2) }&nbsp;€</td></tr>
            </tbody>
          </table>
        
          { lawText }

          { partialPaymentText }
        </div>

        <div class="print-payment">
        <p class="print-payinfo">Bezahlmöglichkeiten</p>
        <p>
          IBAN: DE29 8008 0000 0848 4260 00&nbsp;&nbsp;&nbsp;BIC: DRESDEFF800&nbsp;&nbsp;&nbsp;Commerzbank<br/>
          IBAN: DE77 8006 3558 0005 5681 53&nbsp;&nbsp;&nbsp;BIC: GENODEF1SGH&nbsp;&nbsp;&nbsp;Volksbank
        </p>
      </div>
      </div>
    );
  }
}