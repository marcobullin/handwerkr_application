import * as Helper from '../helper';

export function getSectionsNetAmount(sectionsPrice, tax) {
  return (Helper.validFloat(sectionsPrice) / 100) * Helper.validFloat(tax);
}

export function getAllowanceAmount(price, allowance) {
  return (Helper.validFloat(price) / 100) * Helper.validFloat(allowance);
}

export function getSectionsPrice(sections) {
  if (!sections || sections.length === 0) {
    return 0;
  }

  let sum = 0;
  
  sections.map(section => {
    sum += this.getSectionPrice(section);
  });

  return sum;
}

export function getTotalWageMinutes(sections) {
  if (!sections || sections.length === 0) {
    return 0;
  }

  return sections.map(section => {
    if (!section.subSections || section.subSections.length === 0) {
      return 0;
    }

    return section.subSections.map(subSection => {
      if (!subSection.positions || subSection.positions.length === 0) {
        return 0;
      }

      return subSection.positions.map(position => {
        if (position.positionMode !== 'normal' && position.positionMode !== '') {
          return 0;
        }

        const wageMinutes = Helper.validFloat(position.positionNewWageMinutes) || Helper.validFloat(position.positionWageMinutes) || 0;
        return Helper.validFloat(wageMinutes) * Helper.validFloat(position.positionQuantity);
      }).reduce((left, right) => Helper.validFloat(left) + Helper.validFloat(right), 0);
    }).reduce((left, right) => Helper.validFloat(left) + Helper.validFloat(right), 0);
  }).reduce((left, right) => Helper.validFloat(left) + Helper.validFloat(right), 0);
}

export function getTotalWageMinutesAsText(sections) {
  const totalWageMinutes = getTotalWageMinutes(sections);
  
  if (totalWageMinutes > 100) {
    return (totalWageMinutes / 60).toFixed(2) + ' h';
  }

  return totalWageMinutes.toFixed(2) + ' min';
}

export function getSectionPriceWithoutDiscount(section) {
  const copySection = Object.assign({}, section);
  copySection.discount = 0;

  return this.getSectionPrice(copySection);
}

export function getSectionPrice(section) {
  let { subSections, discount } = section;

  if (!discount) {
    discount = 0;
  }

  let sum = subSections ? subSections.map(subSection => {
    if (!subSection.positions) {
      return 0;
    }
    
    return subSection.positions.map(position => {
      if (position.positionMode !== 'normal' && position.positionMode !== '') {
        return 0;
      }  
      
      return (Helper.validFloat(position.positionNewPrice) || Helper.validFloat(position.positionPrice)) * Helper.validFloat(position.positionQuantity);
    }).reduce((left, right) => Helper.validFloat(left) + Helper.validFloat(right), 0);
  }).reduce((left, right) => Helper.validFloat(left) + Helper.validFloat(right), 0) : 0;
  
  sum -= (sum/100 * discount);
  
  if (sum < 0) {
    sum = 0;
  }
  
  return sum;
}

export function getLossOrProfitForMaterial(purchasePrice, sellingPrice, quantity) {
  const customersPrice = Helper.validFloat(sellingPrice) * Helper.validFloat(quantity);
  const withoutProfitPrice = Helper.validFloat(purchasePrice) * Helper.validFloat(quantity);

  return customersPrice - withoutProfitPrice;
}

// lohnminuten, angepasste lohnminuten, studenlohn, angepasster stundenlohn, original materialien, verkaufspreis Materialien, angepasster Verkaufspreis Materialien, quantity
export function getLossOrProfitForCalculation(wageMinutes, newWageMinutes, wagePortion, newWagePortion, materials, materialPortion, newMaterialPortion, quantity) {
  let addition = Helper.validFloat(wagePortion) - 1/((0.01*56.52)/Helper.validFloat(wagePortion) + 1/Helper.validFloat(wagePortion));
  let wageLossOrProfit = (Helper.validFloat(wageMinutes) / 60) * Helper.validFloat(addition) * Helper.validFloat(quantity);
  
  if (newWageMinutes === '' || newWageMinutes >= 0) {
    if (newWagePortion === '' || newWagePortion >= 0) {
      addition = Helper.validFloat(newWagePortion) - 1/((0.01 * 56.52)/Helper.validFloat(newWagePortion) + 1/Helper.validFloat(newWagePortion));
      wageLossOrProfit = (Helper.validFloat(newWageMinutes) / 60) * Helper.validFloat(addition) * Helper.validFloat(quantity);  
    } else {
      wageLossOrProfit = (Helper.validFloat(newWageMinutes) / 60) * Helper.validFloat(addition) * Helper.validFloat(quantity);  
    }
    
  }
  
  let totalPurchasePriceForMaterials = 0;

  if (materials) {
    materials.map(material => {
      totalPurchasePriceForMaterials += Helper.validFloat(material.purchasePrice) * Helper.validFloat(material.amount);
    });
  }

  totalPurchasePriceForMaterials *= Helper.validFloat(quantity);

  let totalSellingPriceForMaterials = Helper.validFloat(materialPortion) * Helper.validFloat(quantity);
  if (newMaterialPortion === '' || newMaterialPortion >= 0) {
    totalSellingPriceForMaterials = Helper.validFloat(newMaterialPortion) * Helper.validFloat(quantity);
  }

  const materialLossOrProfit = totalSellingPriceForMaterials - totalPurchasePriceForMaterials;
  
  return materialLossOrProfit + wageLossOrProfit;
}

export function getLossOrProfitForSection(section) {
  const { subSections, discount } = section;
  let lossOrProfit = 0;
  
  if (!subSections || subSections.length === 0) {
    return lossOrProfit;
  }

  subSections.map(subSection => {
    if (!subSection.positions || subSection.positions.length === 0) {
      return 0;
    }
    
    subSection.positions.map(position => {
      if (position.positionType === 'material' && (position.positionMode === 'normal' || position.positionMode === '')) {
        lossOrProfit += this.getLossOrProfitForMaterial(position.positionPurchasePrice, position.positionNewPrice || position.positionPrice, position.positionQuantity);
        return;
      }
      
      if (position.positionType === 'calculation' && (position.positionMode === 'normal' || position.positionMode === '')) {
        lossOrProfit += this.getLossOrProfitForCalculation(
          position.positionWageMinutes, 
          position.positionNewWageMinutes,
          position.positionWagePortion, 
          position.positionNewMiddleWage, 
          position.positionMaterials, 
          position.positionMaterialPortion, 
          position.positionNewMaterialPortion,
          position.positionQuantity);
      }
    });
  });

  return lossOrProfit - (Math.abs(lossOrProfit) / 100) * Helper.validFloat(discount);
}

export function getLossOrProfitForSections(sections) {
  let lossOrProfit = 0;

  if (!sections) {
    return lossOrProfit;
  }

  sections.map(section => {
    lossOrProfit += this.getLossOrProfitForSection(section);
  });

  return lossOrProfit;
}

export function getPositionTotal(positionPrice, positionQuantity) {
  return Helper.validFloat(positionPrice) * Helper.validFloat(positionQuantity);
}

export function canCreateEndInvoice(sections) {
  return (sections ? sections.filter(section => !section.invoice) : []).length > 0;
}

export function getPartialPaymentAmount(total, percent) {
  return Helper.validFloat(total) / 100 * Helper.validFloat(percent);
}