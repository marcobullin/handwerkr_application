import React from 'react';
import OfferForm from './components/OfferForm';
import { Redirect } from 'react-router-dom';
import OfferStore from './stores/OfferStore';
import * as OfferActions from './actions/OfferActions';
import Loading from '../common/Loading';

export default class CreatePage extends React.Component {
  constructor() {
    super();
    this.state = {
      fetching: true,
      redirectTo: OfferStore.redirectTo
    };

    this.handleChange = this.handleChange.bind(this);
    OfferActions.getUnits();
  }

  handleChange() {
    if (!this._isMounted) { return; }
    
    this.setState({
      fetching: false,
      redirectTo: OfferStore.redirectTo
    });
  }

  componentDidMount() {
    this._isMounted = true;
    OfferStore.on('change', this.handleChange);
    window.scrollTo(0, 0);
  }

  componentWillUnmount() {
    this._isMounted = false;
    OfferStore.removeListener('change', this.handleChange);
    OfferStore.reset();
  }

  render() {
    const { fetching, redirectTo } = this.state;
    
    if (fetching) {
      return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px'}}>
          <Loading />
        </div>
      );
    }

    if (redirectTo) {
      return <Redirect to={redirectTo} />;
    }

    return (
      <div>
        <div class="container-fluid">
          <h1>Neues Angebot erstellen</h1>
          <OfferForm />
        </div>
      </div>
    )
  }
}