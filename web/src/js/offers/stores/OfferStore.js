import { EventEmitter } from 'events';
import dispatcher from '../../dispatcher';
import moment from 'moment';

class OfferStore extends EventEmitter {
  constructor() {
    super();
    this.reset();
  }

  reset() {
    this.redirectTo = false;
    this.offers = [];
    this.units = [];
    this.offer = {};
  }

  getAll() {
    return this.offers;
  }

  handleActions(action) {
    switch(action.type) {
      case 'CREATED_OFFER': {
        this.redirectTo = action.redirectTo;
        setTimeout(() => {this.emit('change');}, 0);
        break;
      }
      case 'UPDATED_OFFER': {
        this.redirectTo = action.redirectTo;
        setTimeout(() => {this.emit('change');}, 0);
        break;
      }
      case 'DELETED_OFFER': {
        this.offers.splice(action.offerIndex, 1);
        this.emit('change');
        break;
      }
      case 'FOUND_OFFER': {
        if (action.offer) {
          this.offer = action.offer;
        }

        if (action.units) {
          this.units = action.units;
        }
        
        setTimeout(() => {this.emit('change')});
        break;
      }
      case 'FOUND_OFFERS': {
        this.offers = action.offers;
        this.pages = action.pages;
        this.page = action.page;
        this.emit('change');
        break;
      }
      case 'RECEIVE_OFFERS': {
        this.offers = action.offers;
        this.pages = action.pages;
        this.page = action.page;
        this.emit('change');
        break;
      }
      case 'ADDED_CUSTOMER': {
        this.offer.customer = action.customer;
        this.emit('change');
        break;
      }
      case 'CREATED_CUSTOMER': {
        this.offer.customer = action.createdCustomer;
        this.emit('change');
        break;
      }
      case 'REMOVED_CUSTOMER': {
        this.offer.customer = null;
        this.emit('change');
        break;
      }
      case 'UPDATE_NAME': {
        this.offer.name = action.name;
        this.emit('change');
        break;
      }
      case 'UPDATE_OFFER_DATE': {
        this.offer.offerDate = action.offerDate;
        this.emit('change');
        break;
      }
      case 'ADD_SECTION': {
        if (!this.offer.sections) {
          this.offer.sections = [];
        }

        this.offer.sections.push({});
        this.emit('change');
        break;
      }
      case 'UPDATE_SECTION': {
        this.offer.sections[action.index] = action.section;
        this.emit('change');
        break;
      }
      case 'DELETE_SECTION': {
        this.offer.sections.splice(action.index, 1);
        this.emit('change');
        break;
      }
      case 'ADD_SUB_SECTION': {
        if (!this.offer.sections[action.index].subSections) {
          this.offer.sections[action.index].subSections = [];
        }

        this.offer.sections[action.index].subSections.push({});
        this.emit('change');
        break;
      }
      case 'UPDATE_SUB_SECTION': {
        this.offer.sections[action.sectionIndex].subSections[action.subSectionIndex] = action.subSection;
        this.emit('change');
        break;
      }
      case 'DELETE_SUB_SECTION': {
        this.offer.sections[action.sectionIndex].subSections.splice(action.subSectionIndex, 1);
        this.emit('change');
        break;
      }
      case 'ADD_POSITION': {
        if (!this.offer.sections[action.sectionIndex].subSections[action.subSectionIndex].positions) {
          this.offer.sections[action.sectionIndex].subSections[action.subSectionIndex].positions = [];
        }

        const positions = this.offer.sections[action.sectionIndex].subSections[action.subSectionIndex].positions;
        const positionNumber = positions.length > 0 ? (Math.max.apply(null, positions.map(pos => pos.positionNumber)) + 1) : 1;

        this.offer.sections[action.sectionIndex].subSections[action.subSectionIndex].positions.push({
          positionNumber,
          positionMode: 'normal'
        });
        this.emit('change');
        break;
      }
      case 'UPDATE_POSITION': {
        this.offer.sections[action.sectionIndex]
                  .subSections[action.subSectionIndex]
                  .positions[action.positionIndex] = action.position;
        this.emit('change');
        break;
      }
      case 'DELETE_POSITION': {
        this.offer.sections[action.sectionIndex]
                  .subSections[action.subSectionIndex]
                  .positions
                  .splice(action.positionIndex, 1);
        this.emit('change');
        break;
      }
      case 'UPDATE_MFG': {
        this.offer.showMfg = action.checked;
        this.emit('change');
        break;
      }
      case 'UPDATE_INTRODUCTION': {
        this.offer.showIntroduction = action.checked;
        this.emit('change');
        break;
      }
      case 'UPDATE_LAW_TEXT': {
        this.offer.showLawText = action.checked;
        this.emit('change');
        break;
      }
      case 'RECEIVED_ALL_UNITS': {
        this.units = action.units;
        this.emit('change');
        break;
      }
      case 'OFFER_UPDATE_WITHOUT_TAX': {
        this.offer.withoutTax = action.checked;
        this.emit('change');
        break;
      }
      case 'OFFER_UPDATE_PARTIAL_PAYMENT': {
        this.offer.partialPayment = action.partialPayment;
        this.emit('change');
        break;
      }
    }
  }
}

const offerStore = new OfferStore;
dispatcher.register(offerStore.handleActions.bind(offerStore));

export default offerStore;