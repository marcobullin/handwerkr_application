import dispatcher from '../../dispatcher';
import xhttp from '../../xhttp';

export function createOffer(data, redirect, params) {
  xhttp('POST', '/offers/create', data).then(createdOffer => {
    let redirectTo = '';

    switch(redirect) {
      case 'offers': {
        redirectTo = '/offers';
        break;
      }
      case 'printPreview': {
        redirectTo = `/offers/printPreview/${createdOffer.offerId}`;
        break;
      }
      case 'createInvoice': {
        redirectTo = `/invoices/draft/${createdOffer._id}`;

        if (params && params.sectionIndex >= 0) {
          redirectTo += `/${params.sectionIndex}`;
        }
        break;
      }
    }

    dispatcher.dispatch({type: 'CREATED_OFFER', redirectTo});
  });
}

export function getUnits() {
  xhttp('GET', '/units').then(data => {
    dispatcher.dispatch({type: 'RECEIVED_ALL_UNITS', units: data});
  });
}

export function updateOffer(offerId, data, redirect, params) {
  xhttp('POST', '/offers/' + offerId, data).then(() => {
    let redirectTo = '';

    switch(redirect) {
      case 'offers': {
        redirectTo = '/offers';
        break;
      }
      case 'printPreview': {
        redirectTo = `/offers/printPreview/${data.offerId}`;
        break;
      }
      case 'createInvoice': {
        if (params && params.partialPayment === true) {
          redirectTo = `/invoices/partialPayment/${offerId}`;
        } else {
          redirectTo = `/invoices/draft/${offerId}`;

          if (params && params.sectionIndex >= 0) {
            redirectTo += `/${params.sectionIndex}`;
          }
        }

        break;
      }
    }

    dispatcher.dispatch({type: 'UPDATED_OFFER', redirectTo});
  });
}

export function searchOffer(value, page) {
  xhttp('GET', '/offers/search/' + encodeURIComponent(value) + '?page=' + (page || 1)).then(data => {
    dispatcher.dispatch({type: 'FOUND_OFFERS', offers: data.offers, pages: data.pages, page: data.page});
  });
}

export function deleteOffer(offerId, offerIndex) {
  xhttp('POST', '/offers/delete/' + offerId).then(() => {
    dispatcher.dispatch({type: 'DELETED_OFFER', offerIndex});
  });
}

export function getOffer(offerId) {
  xhttp('GET', '/offers/' + offerId).then(data => {
    dispatcher.dispatch({type: 'FOUND_OFFER', offer: data.offer, units: data.units});
  });
}

export function getAll() {
  xhttp('GET', '/offers').then(data => {
    dispatcher.dispatch({type: 'RECEIVE_OFFERS', offers: data.offers, pages: data.pages, page: data.page});
  });
}

export function setCustomer(customer) {
  dispatcher.dispatch({type: 'ADDED_CUSTOMER', customer});
}

export function removeCustomer() {
  dispatcher.dispatch({type: 'REMOVED_CUSTOMER'});
}

export function setName(name) {
  dispatcher.dispatch({type: 'UPDATE_NAME', name});
}

export function setOfferDate(offerDate) {
  dispatcher.dispatch({type: 'UPDATE_OFFER_DATE', offerDate});
}

export function addSection() {
  dispatcher.dispatch({type: 'ADD_SECTION'});
}

export function updateSection(index, section) {
  dispatcher.dispatch({type: 'UPDATE_SECTION', index, section});
}

export function deleteSection(index) {
  dispatcher.dispatch({type: 'DELETE_SECTION', index});
}

export function addSubSection(index) {
  dispatcher.dispatch({type: 'ADD_SUB_SECTION', index});
}

export function updateSubSection(sectionIndex, subSectionIndex, subSection) {
  dispatcher.dispatch({type: 'UPDATE_SUB_SECTION', sectionIndex, subSectionIndex, subSection});
}

export function deleteSubSection(sectionIndex, subSectionIndex) {
  dispatcher.dispatch({type: 'DELETE_SUB_SECTION', sectionIndex, subSectionIndex});
}

export function addPosition(sectionIndex, subSectionIndex) {
  dispatcher.dispatch({type: 'ADD_POSITION', sectionIndex, subSectionIndex});
}

export function updatePosition(sectionIndex, subSectionIndex, positionIndex, position) {
  dispatcher.dispatch({type: 'UPDATE_POSITION', sectionIndex, subSectionIndex, positionIndex, position});
}

export function deletePosition(sectionIndex, subSectionIndex, positionIndex) {
  dispatcher.dispatch({type: 'DELETE_POSITION', sectionIndex, subSectionIndex, positionIndex});
}

export function setMfg(checked) {
  dispatcher.dispatch({type: 'UPDATE_MFG', checked});
}

export function setIntroduction(checked) {
  dispatcher.dispatch({type: 'UPDATE_INTRODUCTION', checked});
}

export function setLawText(checked) {
  dispatcher.dispatch({type: 'UPDATE_LAW_TEXT', checked});
}

export function setWithoutTax(checked) {
  dispatcher.dispatch({type: 'OFFER_UPDATE_WITHOUT_TAX', checked});
}

export function setPartialPayment(partialPayment) {
  dispatcher.dispatch({type: 'OFFER_UPDATE_PARTIAL_PAYMENT', partialPayment});
}