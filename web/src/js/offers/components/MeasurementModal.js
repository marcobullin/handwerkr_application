import React from 'react';
import mathjs from 'mathjs';
import * as Helper from '../../helper';

export default class MeasurementModal extends React.Component {
  constructor(props) {
    super(props);

    this.position = props.position;

    this.state = {
      value1: '',
      sum1: '',
      value2: '',
      sum2: '',
      value3: '',
      sum3: '',
      total: ''
    }
  }

  cancel() {
    this.props.cancelMeasurementModal();
  }


  componentDidMount() {
    this.calculculateSummeries();
  }

  onKeyUp(e) {
    if (e.keyCode !== 13) {
      return;
    }

    this.calculculateSummeries();
  }

  onBlur() {
    this.calculculateSummeries();
  }

  calculculateSummeries() {
    const value1 = this.refs.value1.value;
    const value2 = this.refs.value2.value;
    const value3 = this.refs.value3.value;

    const sum1 = mathjs.eval(value1.toString().split(',').join('.') || 0);
    const sum2 = mathjs.eval(value2.split(',').join('.') || 0);
    const sum3 = mathjs.eval(value3.split(',').join('.') || 0);

    this.setState({
      sum1,
      sum2,
      sum3
    });

    this.position.measurements = [{value: value1, sum: sum1},{value: value2, sum: sum2},{value: value3, sum: sum3}];

    this.calculateTotal(sum1, sum2, sum3);
  }

  changeValue() {
    const value1 = this.refs.value1.value;
    const value2 = this.refs.value2.value;
    const value3 = this.refs.value3.value;

    this.setState({
      value1,
      value2,
      value3
    });

    this.position.measurements = [{value: value1, sum: this.state.sum1},{value: value2, sum: this.state.sum2},{value: value3, sum: this.state.sum3}];
  }

  calculateTotal(sum1, sum2, sum3) {
    this.setState({
      total: (parseFloat(sum1) + parseFloat(sum2) + parseFloat(sum3))
    });
  }

  save() {
    const position = Object.assign(this.props.position, {
      measurements: this.position.measurements,
      positionQuantity: this.refs.sumTotal.value || 0
    });

    this.props.updatePosition(position);
  }

  render() {
    const { value1, sum1, value2, sum2, value3, sum3, total } = this.state;
    const entries = this.position.measurements || [{value: '', sum: ''},{value: '', sum: ''},{value: '', sum: ''}];
    const entryComponents = entries.map((entry, i) => {
      return <div class="row" key={i}>
        <div class="col-md-10 form-group">
          <input type="text" ref={ "value" + (i + 1) } class="form-control" placeholder={ (i + 1) + ". Eintrag" } onKeyUp={this.onKeyUp.bind(this) } onBlur={ this.onBlur.bind(this) } onChange={ this.changeValue.bind(this) } value={ entry.value } />
        </div>
        <div class="col-md-2 form-group">
          <input type="text" ref={ "sum" + (i + 1) } class="form-control" placeholder="Ergebnis" value={ Helper.round(entry.sum, 2) } disabled/>
        </div>
      </div>
    });

    return (
      <div>
        <div class="modal fade in" aria-labelledby="myModalLabel" style={{display: 'block', background: 'rgba(0, 0, 0, 0.5)'}}>
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" onClick={this.cancel.bind(this)} data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Aufmaß</h4>
              </div>
              <div class="modal-body">
                
                { entryComponents }
                
                <div class="row">
                  <div class="col-md-10 form-group" style={{ textAlign: 'right' }}>
                    <p>Aufmaßsumme</p>
                  </div>
                  <div class="col-md-2 form-group">
                    <input type="number" ref="sumTotal" class="form-control" placeholder="Gesamtergebnis" value={ Helper.round(total, 2) } disabled/>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" onClick={this.cancel.bind(this)} class="btn btn-unimportant pull-left" data-dismiss="modal">Abbrechen</button>
                <button type="button" onClick={this.save.bind(this)} class="btn btn-action">Speichern</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}