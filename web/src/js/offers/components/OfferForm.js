import React from 'react';
import * as OfferActions from '../actions/OfferActions';
import { Link } from 'react-router-dom';
import OfferStore from '../stores/OfferStore';
import CustomerSearch from '../../customers/components/CustomerSearch';
import Customer from '../../customers/components/CustomerBox';
import CustomerModal from '../../customers/components/CustomerModal';
import OfferSection from './OfferSection';
import moment from 'moment';
import * as Helper from '../../helper';
import * as Computing from '../computing';
import * as TaxActions from '../../settings/tax/actions/TaxActions';
import TaxStore from '../../settings/tax/stores/TaxStore';
import Loading from '../../common/Loading';

export default class OfferForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      offer: OfferStore.offer,
      fetching: true,
      showCustomerDialog: false
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleTaxChange = this.handleTaxChange.bind(this);
    
    TaxActions.getTax();

    moment.locale('de');
  }

  handleChange() {
    if (!this._isMounted) { return; }

    this.setState({
      showCustomerDialog: false,
      offer: OfferStore.offer
    });
  }

  handleTaxChange() {
    if (!this._isMounted) { return; }

    OfferStore.offer.tax = OfferStore.offer.tax ? OfferStore.offer.tax : TaxStore.tax;

    this.setState({
      fetching: false,
      offer: OfferStore.offer
    });
  }

  componentDidMount() {
    this._isMounted = true;
    OfferStore.on('change', this.handleChange);
    TaxStore.on('change', this.handleTaxChange);
  }

  componentWillUnmount() {
    this._isMounted = false;
    OfferStore.removeListener('change', this.handleChange);
    TaxStore.removeListener('change', this.handleTaxChange);
    OfferStore.reset();
  }

  canCreateInvoice(goal, partialPayment) {
    if (goal !== 'createInvoice') {
      return true;
    }

    if (partialPayment || !this.state.offer.partialPayment || this.state.offer.partialInvoice) {
      return true;
    }

    return false;
  }

  canCreatePartialInvoice(partialPayment, sectionIndex, sections) {
    if (!partialPayment || sectionIndex === null || !(sectionIndex >= 0)) {
      return true;
    }

    const sectionPrice = Computing.getSectionPrice(sections[sectionIndex]);
    const sectionsPrice = Computing.getSectionsPrice(sections);
    const partialPaymentAmount = Computing.getPartialPaymentAmount(sectionsPrice, partialPayment);
    const remainingSections = sections.filter(section => !section.invoice && sections[sectionIndex].id !== section.id);
    const remainingSectionsPrice = Computing.getSectionsPrice(remainingSections);

    return partialPaymentAmount <= remainingSectionsPrice;
  }

  isEndInvoice(redirecTo, params) {
    return redirecTo === 'createInvoice' && !params
  }

  handleSubmit(redirecTo, params) {
    if (this.isEndInvoice(redirecTo, params)) {      
      const uncompletedParitalInvoices = this.state.offer.invoices.filter(invoice => !invoice.invoiceId);

      if (uncompletedParitalInvoices.length > 0) {
        alert(`Es existieren noch unvollständige Teilrechnungen!`);
        return;
      }
    }

    if (!this.canCreateInvoice(redirecTo, params && params.partialPayment)) {
      alert(`Das Angebot setzt eine Anzahlung/Vorauszahlung von ${this.state.offer.partialPayment} % voraus. Hierfür wurde noch keine Anzahlungsrechnungen erstellt!`);
      return;
    } 

    if (!this.canCreatePartialInvoice(this.state.offer.partialPayment, params ? params.sectionIndex : null, this.state.offer.sections)) {
      alert(`Keine Teilrechnung mehr möglich, da der Anzahlungsbetrag den Endbetrag übersteigen würde!`);
      return;
    }

    const sectionsPrice = Computing.getSectionsPrice(this.state.offer.sections);
    const offerDate = moment(this.state.offer.offerDate, 'DD.MM.YYYY');
    const offerDateFormated = offerDate.isValid() ? offerDate.format('DD.MM.YYYY') : moment().format('DD.MM.YYYY');

    const data = {
      offerDate: offerDateFormated,
      tax: this.state.offer.tax,
      name: this.state.offer.name,
      editor: this.state.offer.editor,
      customerId: this.state.offer.customer ? this.state.offer.customer.customerId : '',
      sections: JSON.stringify(this.state.offer.sections),
      showIntroduction: this.state.offer.showIntroduction || false,
      showLawText: this.state.offer.showLawText || false,
      showMfg: this.state.offer.showMfg || false,
      offerCount: this.state.offer.offerCount,
      offerYear: this.state.offer.offerYear,
      withoutTax: this.state.offer.withoutTax || false,
      partialPayment: this.state.offer.partialPayment,
      partialPaymentAmount: this.state.offer.partialPayment ? Computing.getPartialPaymentAmount(sectionsPrice, this.state.offer.partialPayment) : 0
    };

    if (this.state.offer._id) {
      data.offerId = this.state.offer.offerId;
      OfferActions.updateOffer(this.state.offer._id, data, redirecTo, params);
    } else {
      OfferActions.createOffer(data, redirecTo, params);
    }
  }

  createCustomer() {
    this.setState({
      showCustomerDialog: true
    });
  }

  setCustomer(customer) {
    OfferActions.setCustomer(customer);
  }

  removeCustomer() {
    OfferActions.removeCustomer();
  }

  cancelCustomerModal() {
    this.setState({
      showCustomerDialog: false
    });
  }

  changeName() {
    OfferActions.setName(this.refs.name.value);
  }

  changeOfferDate() {
    OfferActions.setOfferDate(this.refs.offerDate.value);
  }

  changeLawText(e) {
    OfferActions.setLawText(e.target.checked);
  }

  changeIntroduction(e) {
    OfferActions.setIntroduction(e.target.checked);
  }

  changeMfg(e) {
    OfferActions.setMfg(e.target.checked);
  }

  addSection() {
    OfferActions.addSection();
  }

  updateSection(index, section) {
    OfferActions.updateSection(index, section);
  }

  deleteSection(sectionIndex) {
    OfferActions.deleteSection(sectionIndex);
  }

  createInvoice(sectionIndex) {
    this.handleSubmit('createInvoice', {sectionIndex});
  }

  changeTax(e) {
    OfferActions.setWithoutTax(e.target.checked);
  }

  changePartialPayment() {
    OfferActions.setPartialPayment(this.refs.partialPayment.value);
  }

  renderSectionComponents(sections, offerInvoice) {
    return sections ? sections.map((section, i) => {
      if (!section.id) {
        section.id = 'section_' + new Date().getTime() + (Math.ceil(Math.random() * 999999999));
      }

      return <OfferSection offerId={this.state.offer._id} key={section.id} index={i} section={section} offerInvoice={offerInvoice} disabled={ this.state.offer.partialInvoice ? 'disabled' : '' } deleteSection={this.deleteSection.bind(this)} updateSection={this.updateSection.bind(this)} createInvoice={this.createInvoice.bind(this)} units={OfferStore.units}/>
    }) : '';
  }

  renderPartialPaymentComponents() {
    var components = [];
    for (var i = 0; i <= 100; i++) {
      if (i === 0) {
        components.push(<option key={ i } value="">Anzahlungsrechnung auswählen</option>);
        continue;
      }
      components.push(<option key={ i } value={ i }>{`${i} % Anzahlung`}</option>);
    }

    return components;
  }

  render() {
    const { offer, showCustomerDialog, fetching } = this.state;

    if (fetching) {
      return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px'}}>
          <Loading />
        </div>
      );
    }

    const { 
      showLawText, 
      showMfg, 
      showIntroduction, 
      sections,
      customer, 
      name, 
      editor, 
      offerDate,
      invoice,
      withoutTax,
      tax,
      partialPayment,
      invoices,
      partialInvoice
    } = offer;

    const totalWageMinutes = Computing.getTotalWageMinutesAsText(sections);
    const total = Computing.getSectionsPrice(sections);
    const totalNetAmount = Computing.getSectionsNetAmount(total, withoutTax ? 0 : tax);
    const lossOrProfit = Computing.getLossOrProfitForSections(sections);
    
    let profitComponent = <li class="list-group-item list-group-item-success">
        <span class="badge">{ Helper.round(lossOrProfit, 2) } €</span>
        Gewinn
      </li>

    if (lossOrProfit < 0) {
      profitComponent = <li class="list-group-item list-group-item-danger">
        <span class="badge">{ Helper.round(lossOrProfit, 2) } €</span>
        Verlust
      </li>
    }

    if (lossOrProfit === 0) {
      profitComponent = <li class="list-group-item list-group-item-warning">
        <span class="badge">{ Helper.round(lossOrProfit, 2) } €</span>
        Gewinn/Verlust
      </li>
    }
    
    let CustomerComponent = '';

    if (invoice && customer) {
      CustomerComponent = <div>
        <p>{ customer.name }</p>
        <p>{ customer.street }</p>
        <p>{ customer.zip + ' ' + customer.city}</p>
      </div>
    } else if (showCustomerDialog) {
      CustomerComponent = <div><CustomerSearch selectCustomer={this.setCustomer.bind(this)} createCustomer={this.createCustomer.bind(this)}/><CustomerModal cancelCustomerModal={this.cancelCustomerModal.bind(this)}/></div>;
    } else if (customer) {
      CustomerComponent = <Customer customer={customer} removeCustomer={this.removeCustomer.bind(this)}/>;
    } else {
      CustomerComponent = <CustomerSearch selectCustomer={this.setCustomer.bind(this)} createCustomer={this.createCustomer.bind(this)}/>;
    }

    const canCreateEndInvoice = Computing.canCreateEndInvoice(sections);
    let invoiceButton = canCreateEndInvoice && offer._id ? <button type="button" onClick={this.handleSubmit.bind(this, 'createInvoice', null)} class="btn btn-secondary pull-right">Endrechnung erstellen</button> : '';
    
    if (invoice) {
      invoiceButton = <Link to={ `/invoices/${invoice}` } class="btn btn-action pull-right" style={{zIndex: 2, position: 'relative'}}>Zur Endrechnung</Link>
    }

    return (
      <div>
        <h2>Angebotdetails</h2>
        <div class="form-group">
          <label for="editor">Bearbeiter</label>
          <input type="text" ref="editor" class="form-control" placeholder="Bearbeiter" name="editor" value="Andreas Francke" disabled/>
        </div>
        <div class="form-group">
          <label for="offerDate">Datum</label>
          <input type="text" ref="offerDate" class="form-control" placeholder="Datum" name="offerDate" onChange={this.changeOfferDate.bind(this)} value={ offerDate || moment().format('DD.MM.YYYY') } disabled={invoice ? 'disabled' : ''}/>
        </div>
        <div class="form-group">
          <label for="name">Bauvorhaben</label>
          <input type="text" ref="name" class="form-control" placeholder="Bauvorhaben" name="name" onChange={this.changeName.bind(this)} value={ name || '' } disabled={invoice ? 'disabled' : ''}/>
        </div>

        <div class="separator"/>

        <h2>Kunde</h2>
        { CustomerComponent }
        
        <div class="separator"/>

        <h2>Positionen</h2>
        { this.renderSectionComponents(sections, invoice) }
        { !invoice ? <button disabled={ partialInvoice ? 'disabled' : '' } type="button" class="btn btn-secondary" onClick={this.addSection.bind(this)}>Titel hinzufügen</button> : '' }
        
        <div class="separator"/>
        <h2>Zusammenfassung</h2>
        <ul class="list-group">
          <li class="list-group-item">
            <span class="badge">{ totalWageMinutes }</span>
            Zeit
          </li>
          <li class="list-group-item">
            <span class="badge">{ Helper.round(total, 2) } €</span>
            Gesamtbetrag
          </li>
          <li class="list-group-item">
            <span class="badge">{ Helper.round((total), 2) } €</span>
            Nettosumme
          </li>
          <li class="list-group-item">
            <span class="badge">{ Helper.round(total + totalNetAmount, 2) } €</span>
            +{ withoutTax ? 0: tax }%
          </li>

          { profitComponent }
        </ul>

        <div class="separator"/>
        <h2>Einstellungen</h2>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="" onChange={this.changeIntroduction.bind(this)} checked={showIntroduction ? 'checked' : ''} disabled={invoice ? 'disabled' : ''}/>
            Einleitungstext anzeigen
          </label>
        </div>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="" onChange={this.changeMfg.bind(this)} checked={showMfg ? 'checked' : ''} disabled={invoice ? 'disabled' : ''}/>
            "Mit freundlichen Grüßen" anzeigen
          </label>
        </div>
        <div class="checkbox">
          <label>
            <input type="checkbox" value=""  onChange={this.changeLawText.bind(this)} checked={showLawText ? 'checked' : ''} disabled={invoice ? 'disabled' : ''}/>
            Gesetzestext anzeigen
          </label>
        </div>

        <div class="separator"/>
        <h2>Zahlung</h2>
        <div class="checkbox">
          <label>
            <input type="checkbox" value="" onChange={this.changeTax.bind(this)} checked={withoutTax ? 'checked' : ''} disabled={invoice ? 'disabled' : ''}/>
            ohne Umsatzsteuer
          </label>
        </div>
        <div class="row">
          <div class="form-group col-md-3 col-xs-12">
            <select class="form-control" ref="partialPayment" onChange={this.changePartialPayment.bind(this)} value={ partialPayment || '' } disabled={ (invoice || (invoices && invoices.length > 0)) ? 'disabled' : ''}>
              { this.renderPartialPaymentComponents() }  
            </select>
          </div>
        </div>

        <br/>
        <br/>
        <div class="btn-toolbar">
          <Link to="/offers" class="btn btn-unimportant col-md-1 col-xs-3 pull-left">Abbrechen</Link>
          { !invoice ? <button type="button" onClick={this.handleSubmit.bind(this, 'offers')} class="btn btn-action pull-right">Speichern</button> : ''}
          { !invoice ? <button type="button" onClick={this.handleSubmit.bind(this, 'printPreview')} class="btn btn-secondary pull-right">Zur Druckvorschau</button> : ''}
          { invoiceButton }
          { offer._id && partialPayment && !partialInvoice ? <button type="button" onClick={this.handleSubmit.bind(this, 'createInvoice', {partialPayment: true})} class="btn btn-secondary pull-right">Anzahlungsrechnung erstellen</button> : ''}
          { partialInvoice ? <Link to={ `/invoices/${partialInvoice}` } class="btn btn-secondary pull-right">Zur Anzahlungsrechnung</Link> : ''}
        </div>
        <br/>
        <br/>
      </div>
    );
  }
}