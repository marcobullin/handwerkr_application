
import React from 'react';
import * as OfferActions from '../actions/OfferActions';
import { Link } from 'react-router-dom';
import moment from 'moment';

export default class OfferTableEntry extends React.Component {
  deleteOffer() {
    var answer = confirm('Möchtest du wirklich das Angebot "' + this.props.offer.name + '" löschen?');
    
    if (answer === true) {
      OfferActions.deleteOffer(this.props.offer._id, this.props.index);
    }
  }

  render() {
    const { _id, offerId, name, customer, offerDate, invoices } = this.props.offer;

    return (
      <tr>
        <td>{ offerId }</td>
        <td>{ customer ? customer.name: 'Kunde fehlt!' }</td>
        <td>{ name }</td>
        <td>{ moment(offerDate, 'DD.MM.YYYY').format('DD.MM.YYYY') }</td>
        <td>
          { 
            (invoices && invoices.length > 0) ? invoices.map(invoice => {
              let name = 'Entwurf';
              if (invoice.invoiceId && invoice.type === 'end') {
                name = `${invoice.invoiceId} Endrechnung`;
              }

              if (invoice.invoiceId && invoice.type === 'partial') {
                name = `${invoice.invoiceId} Teilrechnung`;
              }

              if (invoice.invoiceId && invoice.type === 'percentage') {
                name = `${invoice.invoiceId} Anzahlungsrechnung`;
              }

              return <Link key={invoice._id} to={`/invoices/${invoice._id}`} style={{display: 'block'}}>{ name }</Link>
            }) : 'keine Rechnungen vorhanden'
          }
        </td>
        <td>
          <Link to={'/offers/update/' + offerId} class="btn btn-secondary btn-sm"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></Link>&nbsp;
          <Link to={'/offers/printPreview/' + offerId} class="btn btn-secondary btn-sm"><span class="glyphicon glyphicon-print" aria-hidden="true"></span></Link>&nbsp;
          { invoices && invoices.length === 0 ? <button type="button" onClick={this.deleteOffer.bind(this)} class="btn btn-outline-primary btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button> : '' }
        </td>
      </tr>
    );
  }
}