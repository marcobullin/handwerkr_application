import React from 'react';
import OfferStore from '../stores/OfferStore';
import OfferPosition from './OfferPosition';
import * as OfferActions from '../actions/OfferActions';

export default class OfferSubSection extends React.Component {
  constructor(props) {
    super(props);

    const { sectionIndex, subSection } = props;
    
    // section
    this.sectionIndex = sectionIndex;

    this.subSection = subSection;
  }

  changeTitle() {
    this.subSection.title = this.refs.title.value;
    this.props.updateSubSection(this.props.index, this.subSection);
  }

  addPosition() {
    OfferActions.addPosition(this.sectionIndex, this.props.index);
  }

  updatePosition(positionIndex, position) {
    OfferActions.updatePosition(this.sectionIndex, this.props.index, positionIndex, position);
  }

  deletePosition(positionIndex) {
    OfferActions.deletePosition(this.sectionIndex, this.props.index, positionIndex);
  }

  deleteSubSection() {
    var answer = confirm('Möchtest du wirklich den Zwischentitel "' + (this.subSection.title ? this.subSection.title : 'kein Titel vorhanden') + '" und die dazugehörigen ' + (this.subSection.positions ? this.subSection.positions.length : 0) + ' Positionen löschen?');
    
    if (answer === true) {
      this.props.deleteSubSection(this.props.index);
    }
  }

  renderPositionComponents(positions) {
    return positions ? positions.map((position, i) => {
      if (!position.key) {
        position.key = 'position_' + new Date().getTime() + (Math.ceil(Math.random() * 999999999));
      }
      return <OfferPosition disabled={ this.props.disabled } ref={"position_" + i} key={position.key} sectionIndex={this.sectionIndex} subSectionIndex={this.props.index} index={i} position={position} deletePosition={this.deletePosition.bind(this)} updatePosition={this.updatePosition.bind(this)} units={this.props.units}/>
    }) : '';
  }

  render() {
    const { title, positions } = this.subSection;
    
    return (
      <div class="well well-sm" style={{ background: 'rgb(243, 243, 243)', boxShadow: '0px 2px 5px #999' }}>
        <div class="row">
          <div class="col-md-12 col-xs-12 form-group">
            <label>Zwischentitel</label>
            <input disabled={ this.props.disabled } type="text" ref="title" class="form-control" placeholder="Zwischentitel" onChange={this.changeTitle.bind(this)} value={ title || '' }/>
          </div>
        </div>

        { this.renderPositionComponents(positions) }

        <div class="panel">
          <div class="panel-body" style={{ boxShadow: '0px 2px 5px #999' }}>
            <button disabled={ this.props.disabled } type="button" onClick={this.addPosition.bind(this)} class="btn btn-link"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Position hinzufügen</button>
          </div>
        </div>

        <button disabled={ this.props.disabled } type="button" onClick={this.deleteSubSection.bind(this)} class="btn btn-sm btn-outline-primary"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>&nbsp;Zwischentitel löschen</button>
      </div>
    );
  }
}