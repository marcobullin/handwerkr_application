import React from 'react';
import OfferStore from '../stores/OfferStore';
import OfferSubSection from './OfferSubSection';
import * as OfferActions from '../actions/OfferActions';
import * as Helper from '../../helper';
import * as Computing from '../computing';
import { Link } from 'react-router-dom';

export default class OfferSection extends React.Component {
  constructor(props) {
    super(props);

    const { section } = props;

    this.section = section;
  }

  changeDiscount() {
    this.section.discount = this.refs.sectionDiscount.value;
    this.props.updateSection(this.props.index, this.section);
  }

  changeTitle() {
    this.section.title = this.refs.title.value;
    this.props.updateSection(this.props.index, this.section);
  }

  addSubSection() {
    OfferActions.addSubSection(this.props.index);
  }

  updateSubSection(subSectionIndex, subSection) {
    OfferActions.updateSubSection(this.props.index, subSectionIndex, subSection);
  }

  deleteSubSection(subSectionIndex) {
    OfferActions.deleteSubSection(this.props.index, subSectionIndex);
  }

  deleteSection() {
    var answer = confirm('Möchtest du wirklich den Titel ' + (this.section.title ? `"${this.section.title}"` : '"(kein Titel vorhanden)"') + ' und die dazugehörigen Zwischentitel mit allen Positionen löschen?');
    
    if (answer === true) {
      this.props.deleteSection(this.props.index);
    }
  }

  createInvoice() {
    this.props.createInvoice(this.props.index);
  }

  renderSubSectionComponents(subSections) {
    return subSections ? subSections.map((subSection, i) => {
      if (!subSection.id) {
        subSection.id = 'subSection_' + new Date().getTime() + (Math.ceil(Math.random() * 999999999));
      }
      return <OfferSubSection disabled={ this.props.disabled } key={subSection.id} sectionIndex={this.props.index} index={i} subSection={subSection} deleteSubSection={this.deleteSubSection.bind(this)} updateSubSection={this.updateSubSection.bind(this)} units={this.props.units}/>
    }) : '' ;
  }

  render() {
    const { title, subSections, discount, invoice } = this.section;
    const sum = Computing.getSectionPrice(this.section);
    const lossOrProfit = Computing.getLossOrProfitForSection(this.section);
    
    let profitComponent = <li class="list-group-item list-group-item-success">
        <span class="badge">{ Helper.round(lossOrProfit, 2) } €</span>
        Gewinn
      </li>

    if (lossOrProfit < 0) {
      profitComponent = <li class="list-group-item list-group-item-danger">
        <span class="badge">{ Helper.round(lossOrProfit, 2) } €</span>
        Verlust
      </li>
    }

    if (lossOrProfit === 0) {
      profitComponent = <li class="list-group-item list-group-item-warning">
        <span class="badge">{ Helper.round(lossOrProfit, 2) } €</span>
        Gewinn/Verlust
      </li>
    }

    let invoiceButton = '';
    
    if (this.props.offerId) {
      invoiceButton = <button type="button" onClick={this.createInvoice.bind(this)} class="btn btn-secondary pull-right">Teilrechnung erstellen</button>
    }
    
    if (invoice && invoice !== this.props.offerInvoice) {
      invoiceButton = <Link to={ `/invoices/${invoice}` } class="btn btn-secondary pull-right" style={{zIndex: 2, position: 'relative'}}>Zur Teilrechnung</Link>
    }

    return (
      <div>
        <div class="panel panel-default" style={{ background: 'rgb(235, 235, 235)' }}>
          <div class="section">
            <div class={ invoice ? 'section__overlay' : '' }></div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-10 cols-xs-12 form-group">
                  <label>Titel</label>
                  <input disabled={ this.props.disabled } type="text" ref="title" class="form-control" placeholder="Titel" onChange={this.changeTitle.bind(this)} value={ title || '' }/>
                </div>
                <div class="col-md-2 col-xs-12 form-group">
                  <label>Rabatt in %</label>
                  <input disabled={ this.props.disabled } type="number" ref="sectionDiscount" class="form-control" placeholder="Rabatt in %" onChange={this.changeDiscount.bind(this)} value={ discount || '' }/>
                </div>
              </div>
              
              { this.renderSubSectionComponents(subSections) }

              <div class="panel">
                <div class="panel-body" style={{ background: 'rgb(243, 243, 243)', boxShadow: '0px 2px 5px #999' }}>
                  <button disabled={ this.props.disabled } type="button" onClick={this.addSubSection.bind(this)} class="btn btn-link"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Zwischentitel hinzufügen</button>
                </div>
              </div>

              <ul class="list-group">
                <li class="list-group-item list-group-item-success">
                  <span class="badge">{ Helper.round(sum, 2) } €</span>
                  { title ? 'Summe für ' + title : 'Summe' }
                </li>
                
                { profitComponent }
              </ul>

              <button disabled={ this.props.disabled } type="button" onClick={this.deleteSection.bind(this)} class="btn btn-outline-primary"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>&nbsp;Titel löschen</button>
              
              { invoiceButton }
            </div>          
          </div>
        </div>
      </div>
    );
  }
}