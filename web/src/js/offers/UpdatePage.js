import React from 'react';
import OfferForm from './components/OfferForm';
import OfferStore from './stores/OfferStore';
import * as OfferActions from './actions/OfferActions';
import { Redirect } from 'react-router-dom';
import Loading from '../common/Loading';

export default class UpdatePage extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      fetching: true,
      redirectTo: OfferStore.redirectTo,
      stateObj: OfferStore.stateObj
    };

    this.handleOnChange = this.handleOnChange.bind(this);
    OfferActions.getOffer(this.props.match.params.offerId);
  }

  handleOnChange() {
    if (!this._isMounted) { return; }

    this.setState({
      fetching: false,
      redirectTo: OfferStore.redirectTo,
      stateObj: OfferStore.stateObj
    });
  }

  componentDidMount() {
    this._isMounted = true;
    OfferStore.on('change', this.handleOnChange);
    window.scrollTo(0, 0);
  }

  componentWillUnmount() {
    this._isMounted = false;
    OfferStore.removeListener('change', this.handleOnChange);
    OfferStore.reset();
  }

  render() {
    const { fetching, redirectTo, stateObj } = this.state;

    if (fetching) {
      return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px'}}>
          <Loading />
        </div>
      );
    }

    if (redirectTo) {
      return <Redirect to={{pathname: redirectTo, state: stateObj}} />;
    }
    
    return (
      <div>
        <div class="container-fluid">
          <h1>Angebot bearbeiten</h1>
          <OfferForm />
        </div>
      </div>
    );
  }
}