import React from 'react';
import OfferTableEntry from './components/OfferTableEntry';
import OfferStore from './stores/OfferStore';
import * as OfferActions from './actions/OfferActions';
import { Link } from 'react-router-dom';
import Loading from '../common/Loading';

export default class OverviewPage extends React.Component {
  constructor() {
    super();
    this.state = {
      fetching: true,
      offers: OfferStore.offers,
      pages: OfferStore.pages,
      page: OfferStore.page
    };

    this.handleChange = this.handleChange.bind(this);
    
    OfferActions.getAll();
  }

  componentDidMount() {
    this._isMounted = true;
    OfferStore.on('change', this.handleChange);
    window.scrollTo(0, 0);
  }

  componentWillUnmount() {
    this._isMounted = false;
    OfferStore.removeListener('change', this.handleChange);
    OfferStore.reset();
  }

  handleChange() {
    if (!this._isMounted) { return; }

    this.setState({
      fetching: false,
      offers: OfferStore.offers,
      pages: OfferStore.pages,
      page: OfferStore.page
    });
  }

  searchOffer(e) {
    OfferActions.searchOffer(e.target.value, 1);
  }

  changePage(page, e) {
    e.preventDefault();

    OfferActions.searchOffer(this.refs.search.value, page);
  }
  
  render() {
    const { fetching, offers, pages, page } = this.state;
    
    if (fetching) {
      return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px'}}>
          <Loading />
        </div>
      );
    }

    const OfferComponents = offers.map((offer, i) => { 
      return <OfferTableEntry key={offer.offerId} index={i} offer={offer} /> 
    });

    let PaginationComponents = [];
    for (let i = 1; i <= parseInt(pages); i++) {
      PaginationComponents.push(<li key={i} class={ i === parseInt(page) ? 'active' : ''}><a href="#" onClick={this.changePage.bind(this, i)}>{ i }</a></li>);
    }

    return (
      <div>
        <div class="container-fluid">
          <h1>Angebote</h1>
          <div class="panel panel-default">
            <div class="panel-heading">Alle Angebote</div>
            <div class="panel-body">
              <input type="text" ref="search" onChange={this.searchOffer.bind(this)} class="form-control" placeholder="Angebot suchen"/>
              <br/>
              <Link to="/offers/create" class="btn btn-action">Angebot erstellen</Link>
            </div>
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th class="col-md-1">#</th>
                    <th class="col-md-2">Kunde</th>
                    <th class="col-md-3">Bauvorhaben</th>
                    <th class="col-md-2">Datum</th>
                    <th class="col-md-2">Rechnungen</th>
                    <th class="col-md-2">Aktion</th>
                  </tr>
                </thead>
                <tbody>
                  { OfferComponents }
                </tbody>
              </table>
            </div>
          </div>
          <nav aria-label="Page navigation" style={{textAlign: 'center'}}>
            <ul class="pagination">
              <li>
                <a href="#" onClick={this.changePage.bind(this, parseInt(page)-1 || 1 )} aria-label="Previous">
                  <span aria-hidden="true">&laquo;</span>
                </a>
              </li>
              { PaginationComponents }
              <li>
                <a href="#" onClick={this.changePage.bind(this, (parseInt(page) + 1 <= pages) ? (parseInt(page) + 1) : pages )} aria-label="Next">
                  <span aria-hidden="true">&raquo;</span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    );
  }
}