import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route, Switch } from 'react-router-dom'; 

import Layout from './layout/Layout';
import Home from './homepage/Home';

import OffersPage from './offers/OverviewPage';
import CreateOfferPage from './offers/CreatePage';
import UpdateOfferPage from './offers/UpdatePage';
import OfferPrintPreviewPage from './offers/PrintPage';

import CustomersPage from './customers/OverviewPage';
import CreateCustomerPage from './customers/CreatePage';
import UpdateCustomerPage from './customers/UpdatePage';

import MaterialsPage from './materials/OverviewPage';
import CreateMaterialPage from './materials/CreatePage';
import UpdateMaterialPage from './materials/UpdatePage';

import CalculationsPage from './calculations/OverviewPage';
import CreateCalculationPage from './calculations/CreatePage';
import UpdateCalculationPage from './calculations/UpdatePage';

import TaxSettingsOverviewPage from './settings/tax/OverviewPage';
import WageSettingsOverviewPage from './settings/wage/OverviewPage';

import InvoicesOverviewPage from './invoices/OverviewPage';
import InvoiceCreatePage from './invoices/CreatePage';
import InvoiceUpdatePage from './invoices/UpdatePage';
import InvoicePrintPreviewPage from './invoices/PrintPage';
import InvoicePartialPaymentPrintPreviewPage from './invoices/PartialPaymentPrintPage';

const app = document.getElementById('app');

ReactDOM.render(
  <Router>
    <Switch>
      <Layout>
        <Route exact={true} path="/" component={Home} />  
        
        <Route exact path="/materials" component={MaterialsPage}></Route>
        <Route exact path="/materials/create" component={CreateMaterialPage}></Route>
        <Route exact path="/materials/update/:materialId" component={UpdateMaterialPage}></Route>
        
        <Route exact path="/calculations" component={CalculationsPage}></Route>
        <Route exact path="/calculations/create" component={CreateCalculationPage}></Route>
        <Route exact path="/calculations/update/:calculationId" component={UpdateCalculationPage}></Route>
        
        <Route exact path="/customers" component={CustomersPage}></Route>
        <Route exact path="/customers/create" component={CreateCustomerPage}></Route>
        <Route exact path="/customers/update/:customerId" component={UpdateCustomerPage}></Route>

        <Route exact path="/offers" component={OffersPage}></Route>
        <Route exact path="/offers/printPreview/:offerId" component={OfferPrintPreviewPage}></Route>
        <Route exact path="/offers/create" component={CreateOfferPage}></Route>
        <Route exact path="/offers/update/:offerId" component={UpdateOfferPage}></Route>

        <Route exact path="/settings/tax" component={TaxSettingsOverviewPage}></Route>
        <Route exact path="/settings/wage" component={WageSettingsOverviewPage}></Route>

        <Route exact path="/invoices" component={InvoicesOverviewPage}></Route>
        <Route exact path="/invoices/printPreview/partialPayment/:invoiceId" component={InvoicePartialPaymentPrintPreviewPage}></Route>
        <Route exact path="/invoices/printPreview/:invoiceId" component={InvoicePrintPreviewPage}></Route>
        <Route exact path="/invoices/partialPayment/:offerId" component={InvoiceCreatePage}></Route>
        <Route exact path="/invoices/draft/:offerId/:sectionIndex" component={InvoiceCreatePage}></Route>
        <Route exact path="/invoices/draft/:offerId" component={InvoiceCreatePage}></Route>
        <Route exact path="/invoices/:invoiceId" component={InvoiceUpdatePage}></Route>
      </Layout>
    </Switch>
  </Router>, 
  app
);