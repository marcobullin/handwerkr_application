import React from 'react';
import CalculationForm from './components/CalculationForm';
import { Redirect } from 'react-router-dom';
import CalculationStore from './stores/CalculationStore';
import * as CalculationActions from './actions/CalculationActions';
import Loading from '../common/Loading';

export default class CreatePage extends React.Component {
  constructor() {
    super();
    this.state = {
      fetchingUnits: CalculationStore.fetchingUnits,
      fetchingMiddleWage: CalculationStore.fetchingMiddleWage,
      redirect: CalculationStore.redirect,
      units: CalculationStore.units,
      middleWage: CalculationStore.middleWage
    };

    this.handleChange = this.handleChange.bind(this);
    
    CalculationActions.getUnits();
    CalculationActions.getMiddleWage();
  }

  handleChange() {
    this.setState({
      fetchingUnits: CalculationStore.fetchingUnits,
      fetchingMiddleWage: CalculationStore.fetchingMiddleWage,
      redirect: CalculationStore.redirect,
      units: CalculationStore.units,
      middleWage: CalculationStore.middleWage
    });
  }

  componentDidMount() {
    CalculationStore.on('change', this.handleChange);
  }

  componentWillUnmount() {
    CalculationStore.reset();
    CalculationStore.removeListener('change', this.handleChange);
  }

  render() {
    const { fetchingUnits, fetchingMiddleWage, redirect } = this.state;
    
    if (fetchingUnits || fetchingMiddleWage) {
      return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px'}}>
          <Loading />
        </div>
      );
    }

    if (redirect) {
      return(<Redirect to="/calculations" />);
    }

    return (
      <div>
        <div class="container-fluid">
          <h1>Neue Kalkulation anlegen</h1>
          <CalculationForm />
        </div>
      </div>
    );
  }
}