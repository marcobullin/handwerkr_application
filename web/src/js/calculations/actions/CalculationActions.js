import dispatcher from '../../dispatcher';
import xhttp from '../../xhttp';

export function createCalculation(data) {
  xhttp('POST', '/calculations/new', data).then(() => {
    dispatcher.dispatch({type: 'CREATED_CALCULATION', redirectTo: '/calculations'});
  });
}

export function updateCalculation(calculationId, data) {
  xhttp('POST', '/calculations/' + calculationId, data).then(() => {
    dispatcher.dispatch({type: 'UPDATED_CALCULATION', redirectTo: '/calculations'});
  });
}

export function searchCalculation(value, page) {
  xhttp('GET', '/calculations/search/' + encodeURIComponent(value) + '?page=' + (page || 1)).then(data => {
    dispatcher.dispatch({type: 'FOUND_CALCULATIONS', calculations: data.calculations, middleWage: data.middleWage, pages: data.pages, page: data.page});
  });
}

export function deleteCalculation(calculationId, calculationIndex) {
  xhttp('POST', '/calculations/delete/' + calculationId).then(() => {
    dispatcher.dispatch({type: 'DELETED_CALCULATION', calculationIndex});
  });
}

export function getCalculation(calculationId) {
  xhttp('GET', '/calculations/' + calculationId).then(data => {
    dispatcher.dispatch({type: 'FOUND_CALCULATION', calculation: data.calculation, units: data.units, middleWage: data.middleWage});
  });
}

export function getAll() {  
  xhttp('GET', '/calculations').then(data => {
    dispatcher.dispatch({type: 'RECEIVE_CALCULATIONS', calculations: data.calculations, pages: data.pages, page: data.page});
  });
}

export function addMaterialPosition() {
  dispatcher.dispatch({type: 'ADD_MATERIAL'});
}

export function updateMaterialPosition(index, material) {
  dispatcher.dispatch({type: 'UPDATE_MATERIAL', index, material});
}

export function deleteMaterialPosition(index) {
  dispatcher.dispatch({type: 'DELETE_MATERIAL', index});
}

export function getUnits() {
  xhttp('GET', '/units').then(data => {
    dispatcher.dispatch({type: 'RECEIVE_UNITS', units: data});
  });
}

export function getMiddleWage() {
  xhttp('GET', '/middleWage').then(middleWage => {
    dispatcher.dispatch({type: 'RECEIVE_MIDDLEWAGE', middleWage});
  });
}

export function changeUnit(unitId) {
  dispatcher.dispatch({type: 'CHANGE_UNIT', unitId});
}

export function changeTitle(title) {
  dispatcher.dispatch({type: 'CHANGE_TITLE', title});
}

export function changeText(text) {
  dispatcher.dispatch({type: 'CHANGE_TEXT', text});
}

export function changeWageMinutes(wageMinutes) {
  dispatcher.dispatch({type: 'CHANGE_WAGE_MINUTES', wageMinutes});
}