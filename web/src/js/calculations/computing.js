import * as Helper from '../helper';

export function getAverageWagePerMinute(middleWage) {
  return parseFloat(middleWage || 0) / 60;
}

export function getWagePortion(wageMinutes, wagePerMinute) {
  return Helper.round(wageMinutes * wagePerMinute, 2);
}

export function getMaterialPortion(materials) {
  materials = materials || [];

  let materialPortion = 0;

  for (let i = 0; i < materials.length; i++) {
    const sellingPrice = parseFloat(materials[i].sellingPrice) || 0;
    const amount = parseFloat(materials[i].amount) || 0;
    
    materialPortion += amount * sellingPrice;
  }

  return Helper.round(materialPortion, 2);
}

export function getCalculationPriceOfWageAndMaterialPortion(wagePortion, materialPortion) {
  return Helper.round((parseFloat(wagePortion) || 0) + (parseFloat(materialPortion) || 0), 2);
}

export function getCalculationPrice(calculation, middleWage) {
  const averageWagePerMinute = this.getAverageWagePerMinute(middleWage);
  const wagePortion = this.getWagePortion(calculation.wageMinutes, averageWagePerMinute);
  const materialPortion = this.getMaterialPortion(calculation.materials);
  
  return this.getCalculationPriceOfWageAndMaterialPortion(wagePortion, materialPortion);
}