import React from 'react';
import * as Helper from '../../helper';
import * as Computing from '../computing';

export default class CalculationModal extends React.Component {
  constructor(props) {
    super(props);

    this.index = props.index;

    const { 
      positionPrice, 
      positionWageMinutes, 
      positionWagePortion, 
      positionMiddleWage, 
      positionMaterialPortion,
      positionNewPrice, 
      positionNewWageMinutes, 
      positionNewWagePortion, 
      positionNewMiddleWage, 
      positionNewMaterialPortion,
      positionHasChanged
    } = this.props.position;

    this.state = {
      goalPositionPrice: positionHasChanged ? positionNewPrice : positionPrice,
      goalPositionWageMinutes: positionHasChanged ? positionNewWageMinutes : positionWageMinutes,
      goalPositionMiddleWage: positionHasChanged ? positionNewMiddleWage : positionMiddleWage,
      goalPositionWagePortion: positionHasChanged ? positionNewWagePortion : positionWagePortion,
      goalPositionMaterialPortion: positionHasChanged ? positionNewMaterialPortion : positionMaterialPortion
    }
  }

  cancel() {
    this.props.cancelCalculationModal();
  }

  changeCalculation(goalPrice, goalWageMinutes, goalMiddleWage, overwriteGoalPrice) {
    const newWagePortion = Helper.getWagePortionForMiddleWage(goalWageMinutes, goalMiddleWage);
    const newPrice = Computing.getCalculationPriceOfWageAndMaterialPortion(newWagePortion, this.state.goalPositionMaterialPortion);

    this.setState({
      goalPositionWagePortion: newWagePortion,
      goalPositionPrice: overwriteGoalPrice ? goalPrice : newPrice,
      goalPositionWageMinutes: goalWageMinutes,
      goalPositionMiddleWage: goalMiddleWage,
      goalPositionMaterialPortion: this.state.goalPositionMaterialPortion
    });
  }

  changeWagePortion() {
    this.setState({
      goalPositionWagePortion: this.refs.goalPositionWagePortion.value,
      goalPositionPrice: Computing.getCalculationPriceOfWageAndMaterialPortion(this.state.goalPositionWagePortion, this.state.goalPositionMaterialPortion)
    });
  }

  changeMaterialPortion() {
    this.setState({
      goalPositionMaterialPortion: this.refs.goalPositionMaterialPortion.value,
      goalPositionPrice: Computing.getCalculationPriceOfWageAndMaterialPortion(this.state.goalPositionWagePortion, this.refs.goalPositionMaterialPortion.value)
    });
  }

  changePrice() {
    this.changeCalculation(this.refs.goalPositionPrice.value, this.state.goalPositionWageMinutes, this.state.goalPositionMiddleWage, true);
  }

  changeWageMinutes() {
    this.changeCalculation(this.state.goalPositionPrice, this.refs.goalPositionWageMinutes.value, this.state.goalPositionMiddleWage);
  }

  changeMiddleWage() {
    this.changeCalculation(this.state.goalPositionPrice, this.state.goalPositionWageMinutes, this.refs.goalPositionMiddleWage.value);
  }

  componentDidMount() {
    this.changeCalculation(this.state.goalPositionPrice, this.state.goalPositionWageMinutes, this.state.goalPositionMiddleWage, true);
  }

  save() {
    const position = Object.assign(this.props.position, {
      positionNewPrice: this.state.goalPositionPrice, 
      positionNewWageMinutes: this.state.goalPositionWageMinutes,
      positionNewMiddleWage: this.state.goalPositionMiddleWage,
      positionNewWagePortion: this.state.goalPositionWagePortion, 
      positionNewMaterialPortion: this.state.goalPositionMaterialPortion,
      positionHasChanged: 1
    });
    
    this.props.updatePosition(position);
  }

  render() {
    const { positionPrice, positionWageMinutes, positionMiddleWage, positionWagePortion, positionMaterialPortion } = this.props.position;
    const { goalPositionPrice, goalPositionWageMinutes, goalPositionMiddleWage, goalPositionWagePortion, goalPositionMaterialPortion } = this.state;

    return (
      <div>
        <div class="modal fade in" aria-labelledby="myModalLabel" style={{display: 'block', background: 'rgba(0, 0, 0, 0.5)'}}>
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" onClick={this.cancel.bind(this)} data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Kalkulation bearbeiten</h4>
              </div>
              <div class="modal-body">
                <div class="form-group row">
                  <div class="col-lg-3">
                    <label>Gesamtkosten in €</label>
                    <input type="number" ref="positionPrice" class="form-control" placeholder="Gesamtkosten in €" defaultValue={ positionPrice } disabled/>
                  </div>
                  <div class="col-lg-2">
                    <label>Zeit in Minuten</label>
                    <input type="text" ref="positionWageMinutes" class="form-control" placeholder="Zeit in Minuten" defaultValue={ positionWageMinutes } disabled/>
                  </div>
                  <div class="col-lg-2">
                    <label>Lohn/Stunde in €</label>
                    <input type="number" ref="positionMiddleWage" class="form-control" placeholder="Lohn/Stunde in €" defaultValue={ positionMiddleWage } disabled/>
                  </div>
                  <div class="col-lg-2">
                    <label>Lohnkosten in €</label>
                    <input type="number" ref="positionWagePortion" class="form-control" placeholder="Lohnkosten in €" defaultValue={ positionWagePortion } disabled/>
                  </div>
                  <div class="col-lg-3">
                    <label>Materialkosten in €</label>
                    <input type="number" ref="positionMaterialPortion" class="form-control" placeholder="Materialkosten in €" defaultValue={ positionMaterialPortion } disabled/>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-lg-3">
                    <label>Gesamtkosten in €</label>
                    <input type="text" ref="goalPositionPrice" class="form-control" onChange={this.changePrice.bind(this)} placeholder="Gesamtkosten in €" value={ goalPositionPrice }/>
                  </div>
                  <div class="col-lg-2">
                    <label>Zeit in Minuten</label>
                    <input type="number" ref="goalPositionWageMinutes" class="form-control" onChange={this.changeWageMinutes.bind(this)} placeholder="Zeit in Minuten" value={goalPositionWageMinutes}/>
                  </div>
                  <div class="col-lg-2">
                    <label>Lohn/Stunde in €</label>
                    <input type="number" ref="goalPositionMiddleWage" class="form-control" onChange={this.changeMiddleWage.bind(this)} placeholder="Lohn/Stunde in €" value={goalPositionMiddleWage}/>
                  </div>
                  <div class="col-lg-2">
                    <label>Lohnkosten in €</label>
                    <input type="text" ref="goalPositionWagePortion" class="form-control" onChange={this.changeWagePortion.bind(this)} placeholder="Lohnkosten in €" value={Helper.round(goalPositionWagePortion, 2)} disabled/>
                  </div>
                  <div class="col-lg-3">
                    <label>Materialkosten in €</label>
                    <input type="number" ref="goalPositionMaterialPortion" class="form-control" onChange={this.changeMaterialPortion.bind(this)} placeholder="Materialkosten in €" value={goalPositionMaterialPortion}/>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" onClick={this.cancel.bind(this)} class="btn btn-unimportant pull-left" data-dismiss="modal">Abbrechen</button>
                <button type="button" onClick={this.save.bind(this)} class="btn btn-action">Speichern</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}