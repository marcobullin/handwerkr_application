import React from 'react';
import * as CalculationActions from '../actions/CalculationActions';
import { Link } from 'react-router-dom';
import CalculationStore from '../stores/CalculationStore';
import MaterialPosition from '../../materials/components/MaterialPosition';
import moment from 'moment';
import * as Computing from '../computing';
import * as Helper from '../../helper';

export default class CalculationForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      calculation: CalculationStore.calculation,
      units: CalculationStore.units,
      middleWage: CalculationStore.middleWage
    }

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange() {
    if (!this._isMounted) {
      return;
    }
    
    this.setState({
      calculation: CalculationStore.calculation,
      units: CalculationStore.units,
      middleWage: CalculationStore.middleWage
    });
  }

  componentDidMount() {
    this._isMounted = true;
    CalculationStore.on('change', this.handleChange);
  }

  componentWillUnmount() {
    this._isMounted = false;
    CalculationStore.removeListener('change', this.handleChange);
    CalculationStore.reset();
  }

  handleSubmit() {
    const { materials } = this.state.calculation;
    const materialMapping = materials ? materials.filter(material => material._id !== '').map(material => {
      return {
        material: material._id,
        amount: parseFloat(material.amount || 0)
      };
    }): [];

    const data = {
      title: this.refs.title.value,
      text: this.refs.text.value,
      wageMinutes: this.refs.wageMinutes.value,
      unitId: this.refs.unitId.value,
      materialMapping: JSON.stringify(materialMapping)
    };

    if (this.state.calculation._id) {
      CalculationActions.updateCalculation(this.state.calculation._id, data);
    } else {
      CalculationActions.createCalculation(data);
    }
  }

  addMaterialPosition() {
    CalculationActions.addMaterialPosition();
  }

  updateMaterialPosition(index, material) {
    CalculationActions.updateMaterialPosition(index, material);
  }

  deleteMaterialPosition(index) {
    CalculationActions.deleteMaterialPosition(index);
  }

  changeTitle() {
    CalculationActions.changeTitle(this.refs.title.value);
  }

  changeText(e) {
    CalculationActions.changeText(this.refs.text.value);
  }

  changeWageMinutes() {
    CalculationActions.changeWageMinutes(this.refs.wageMinutes.value);
  }

  changeUnit() {
    CalculationActions.changeUnit(this.refs.unitId.value);
  }

  renderMaterialComponents(materials) {
    return materials.map((material, i) => {
      if (!material.key) {
        material.key = material._id || 'material_' + new Date().getTime() + (Math.ceil(Math.random() * 999999999));
      }

      return <MaterialPosition ref={"material_" + i} key={material.key} index={i} material={material} deleteMaterialPosition={this.deleteMaterialPosition.bind(this)} updateMaterialPosition={this.updateMaterialPosition.bind(this)}/>
    });
  }

  renderUnitComponents(units) {
    return units.map(unit => {
      return <option key={unit._id} value={ unit._id }>{ unit.name }</option>
    });
  }

  renderForm(calculation, units) {
    const { title, text, materials, wageMinutes, unit } = calculation;
    const buttonName = calculation._id ? 'Speichern' : 'Erstellen';
    const averageWagePerMinute = Computing.getAverageWagePerMinute(this.state.middleWage.middleWage);
    const wagePortion = Computing.getWagePortion(wageMinutes, averageWagePerMinute);
    const materialPortion = Computing.getMaterialPortion(materials);
    const total = Computing.getCalculationPriceOfWageAndMaterialPortion(wagePortion, materialPortion);

    return (
      <div>
        <h2>Kalkulationsdetails</h2>
        <div class="form-group">
          <label for="title">Titel</label>
          <input type="text" ref="title" class="form-control input-sm" placeholder="Titel" onChange={this.changeTitle.bind(this)} value={ title || '' }/>
        </div>
        <div class="form-group">
          <label for="text">Beschreibungstext</label>
          <textarea ref="text" class="form-control input-sm" rows="3" onChange={this.changeText.bind(this)} placeholder="Beschreibungstext" value={ text || '' }></textarea>
        </div>

        <div class="separator"/>
        <h2>Mittellohn</h2>
        <div class="row">
          <div class="col-md-3 col-xs-12 form-group">
            <label for="wageMinutes">Lohnminuten</label>
            <input type="number" ref="wageMinutes" onChange={this.changeWageMinutes.bind(this)} value={ wageMinutes || '' } class="form-control input-sm" placeholder="Lohnminuten"/>
          </div>
          <div class="col-md-1 col-xs-12 form-group">
            <label for="unitId">Einheit</label>
            <select class="form-control input-sm" ref="unitId" onChange={this.changeUnit.bind(this)} value={ unit ? unit._id : '' }>
              { this.renderUnitComponents(units) }  
            </select>
          </div>
          <div class="col-md-2 col-xs-12 form-group">
            <label for="wageType">Lohntyp</label>
            <input type="text" ref="wageType" class="form-control input-sm" placeholder="Lohntyp" defaultValue="Gesellenlohn" disabled/>
          </div>
          <div class="col-md-3 col-xs-12 form-group">
            <label for="middleWage">Mittellohn/Stunde in €</label>
            <input type="text" ref="middleWage" class="form-control input-sm" placeholder="Mittellohn/Stunde in €" defaultValue={ this.state.middleWage.middleWage } disabled/>
          </div>
          <div class="col-md-3 col-xs-12 form-group">
            <label for="averageWagePerMinute">Mittellohn/Minute in €</label>
            <input type="text" ref="averageWagePerMinute" value={ Helper.round(averageWagePerMinute || 0, 2) } class="form-control input-sm" placeholder="Mittellohn/Minute in €" disabled/>
          </div>
        </div>

        <div class="separator"/>
        <h2>Materialzuordnung</h2>
        { this.renderMaterialComponents(materials || []) }  
        <button type="button" class="btn btn-secondary" onClick={this.addMaterialPosition.bind(this)}>Material hinzufügen</button>
        
        <div class="separator"/>
        <h2>Zusammenfassung</h2>
        <ul class="list-group">
          <li class="list-group-item">
            <span class="badge">{ wagePortion || 0 } €</span>
            Lohnanteil
          </li>
          <li class="list-group-item">
            <span class="badge">{ materialPortion || 0 } €</span>
            Materialanteil
          </li>
          <li class="list-group-item">
            <span class="badge">{ total || 0 } €</span>
            Gesamtbetrag
          </li>
        </ul>

        <br/><br/>
        <div class="btn-toolbar">
          <Link to="/calculations" class="btn btn-unimportant pull-left">Abbrechen</Link>
          <button type="button" onClick={this.handleSubmit.bind(this)} class="btn btn-action pull-right">{ buttonName }</button>
        </div>
        <br/><br/>
      </div>
    );
  }

  render() {
    const { calculation, units } = this.state;

    return this.renderForm(calculation, units);
  }
}