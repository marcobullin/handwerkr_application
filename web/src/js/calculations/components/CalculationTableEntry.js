import React from 'react';
import * as CalculationActions from '../actions/CalculationActions';
import { Link } from 'react-router-dom';
import * as Computing from '../computing';

export default class CalculationTableEntry extends React.Component {
  constructor(props) {
    super(props);
  }

  deleteCalculation(e) {
    var answer = confirm('Möchtest du wirklich die Kalkulation "' + this.props.calculation.title + '" löschen?');
    
    if (answer === true) {
      CalculationActions.deleteCalculation(this.props.calculation._id, this.props.index);
    }
  }

  render() {
    const { calculationId, title, text, unit } = this.props.calculation;
    const total = Computing.getCalculationPrice(this.props.calculation, this.props.middleWage.middleWage);

    return (
      <tr>
        <td>{ title }</td>
        <td>{ text }</td>
        <td>{ total + ' €'}</td>
        <td>1 { unit.name }</td>
        <td>
          <Link to={'/calculations/update/' + calculationId} class="btn btn-secondary btn-sm"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></Link>&nbsp;
          <button type="button" onClick={this.deleteCalculation.bind(this)} data-id={calculationId} class="btn btn-outline-primary btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
        </td>
      </tr>
    );
  }
}