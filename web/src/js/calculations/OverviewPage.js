import React from 'react';
import CalculationTableEntry from './components/CalculationTableEntry';
import CalculationStore from './stores/CalculationStore';
import * as CalculationActions from './actions/CalculationActions';
import CalculationForm from './components/CalculationForm';
import { Link } from 'react-router-dom';
import Loading from '../common/Loading';
import * as WageActions from '../settings/wage/actions/WageActions';
import WageStore from '../settings/wage/stores/WageStore';

export default class OverviewPage extends React.Component {
  constructor() {
    super();
    
    this.state = {
      fetching: true,
      fetchingMiddleWage: true,
      middleWage: WageStore.middleWage,
      calculations: CalculationStore.calculations,
      pages: CalculationStore.pages,
      page: CalculationStore.page
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleMiddleWageChange = this.handleMiddleWageChange.bind(this);
    
    WageActions.getMiddleWage();
    CalculationActions.getAll();
  }

  componentDidMount() {
    this._isMounted = true;

    CalculationStore.on('change', this.handleChange);
    WageStore.on('change', this.handleMiddleWageChange);
  }

  componentWillUnmount() {
    this._isMounted = false;

    CalculationStore.removeListener('change', this.handleChange);
    CalculationStore.reset();

    WageStore.removeListener('change', this.handleMiddleWageChange);
    WageStore.reset();
  }

  handleChange() {
    if (!this._isMounted) { return; }

    this.setState({
      fetching: false,
      calculations: CalculationStore.calculations,
      pages: CalculationStore.pages,
      page: CalculationStore.page
    });
  }

  handleMiddleWageChange() {
    if (!this._isMounted) { return; }

    this.setState({
      fetchingMiddleWage: false,
      middleWage: WageStore.middleWage
    });
  }

  searchCalculation(e) {
    CalculationActions.searchCalculation(e.target.value, 1);
  }

  changePage(page, e) {
    e.preventDefault();

    CalculationActions.searchCalculation(this.refs.search.value, page);
  }

  render() {
    const { fetching, fetchingMiddleWage, calculations, middleWage, pages, page } = this.state;

    if (fetching || fetchingMiddleWage) {
      return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px'}}>
          <Loading />
        </div>
      );
    }

    const CalculationComponents = calculations.map((calculation, i) => { 
      return <CalculationTableEntry key={calculation._id} calculation={calculation} index={i} middleWage={ middleWage } /> 
    });

    let PaginationComponents = [];
    for (let i = 1; i <= parseInt(pages); i++) {
      PaginationComponents.push(<li key={i} class={ i === parseInt(page) ? 'active' : ''}><a href="#" onClick={this.changePage.bind(this, i)}>{ i }</a></li>);
    }

    return (
      <div>
        <div class="container-fluid">
          <h1>Kalkulationen</h1>
          <div class="panel panel-default">
            <div class="panel-heading">Alle Kalkulationen</div>
            <div class="panel-body">
              <input type="text" ref="search" onChange={this.searchCalculation.bind(this)} class="form-control" placeholder="Kalkulation suchen"/>
              <br/>
              <Link to="/calculations/create" class="btn btn-action">Kalkulation anlegen</Link>
            </div>
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th class="col-md-3">Titel</th>
                    <th class="col-md-6">Text</th>
                    <th class="col-md-1">Gesamtpreis</th>
                    <th class="col-md-1">Einheit</th>
                    <th class="col-md-1">Aktion</th>
                  </tr>
                </thead>
                <tbody>
                  { CalculationComponents }
                </tbody>
              </table>
            </div>
          </div>
          <nav aria-label="Page navigation" style={{textAlign: 'center'}}>
            <ul class="pagination">
              <li>
                <a href="#" onClick={this.changePage.bind(this, parseInt(page)-1 || 1 )} aria-label="Previous">
                  <span aria-hidden="true">&laquo;</span>
                </a>
              </li>
              { PaginationComponents }
              <li>
                <a href="#" onClick={this.changePage.bind(this, (parseInt(page) + 1 <= pages) ? (parseInt(page) + 1) : pages )} aria-label="Next">
                  <span aria-hidden="true">&raquo;</span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    );
  }
}