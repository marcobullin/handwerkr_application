import { EventEmitter } from 'events';
import dispatcher from '../../dispatcher';

class CalculationStore extends EventEmitter {
  constructor() {
    super();
    this.reset();
  }

  reset() {
    this.redirect = false;
    this.calculations = [];
    this.calculation = {};
    this.units = [];
    this.middleWage = {};
    this.fetchingMiddleWage = true;
    this.fetchingUnits = true;
    this.pages = 1;
    this.page = 1;
  }

  getAll() {
    return this.calculations;
  }

  handleActions(action) {
    switch(action.type) {
      case 'CREATED_CALCULATION': {
        this.redirect = true;
        setTimeout(() => {this.emit('change');}, 0);
        break;
      }
      case 'UPDATED_CALCULATION': {
        this.redirect = true;
        setTimeout(() => {this.emit('change');}, 0);
        break;
      }
      case 'DELETED_CALCULATION': {
        this.calculations.splice(action.calculationIndex, 1);
        this.emit('change');
        break;
      }
      case 'FOUND_CALCULATION': {
        const { _id, calculationId, title, text, wageMinutes, units, materialMapping } = action.calculation;
        this.calculation = {
          _id,
          calculationId,
          title,
          text,
          wageMinutes,
          materials: materialMapping.map(mapping => {
            mapping.material.amount = mapping.amount;
            return mapping.material;
          })
        };

        this.units = action.units;
        this.middleWage = action.middleWage;

        setTimeout(() => {this.emit('change')});
        break;
      }
      case 'FOUND_CALCULATIONS': {
        this.calculations = action.calculations;
        this.pages = action.pages;
        this.page = action.page;
        
        this.calculations.map(calculation => {
          calculation.middleWage = action.middleWage;
          calculation.materials = calculation.materialMapping.map(mapping => {
            mapping.material.amount = mapping.amount;
            return mapping.material;
          });
        });

        this.emit('change');
        break;
      }
      case 'RECEIVE_CALCULATIONS': {
        this.calculations = action.calculations;
        this.pages = action.pages;
        this.page = action.page;

        this.calculations.map(calculation => {
          calculation.materials = calculation.materialMapping.map(mapping => {
            mapping.material.amount = mapping.amount;
            return mapping.material;
          });
        });
        this.emit('change');
        break;
      }
      case 'ADD_MATERIAL': {
        if (!this.calculation.materials) {
          this.calculation.materials = [];
        }
        this.calculation.materials.push({});
        this.emit('change');
        break;
      }
      case 'UPDATE_MATERIAL': {
        this.calculation.materials[action.index] = Object.assign(this.calculation.materials[action.index], action.material);
        this.emit('change');
        break;
      }
      case 'DELETE_MATERIAL': {
        const { materials } = this.calculation;
        materials.splice(action.index, 1);
        this.calculation.materials = materials;
        this.emit('change');
        break;
      }
      case 'RECEIVE_UNITS': {
        this.fetchingUnits = false;
        this.units = action.units;
        this.emit('change');
        break;
      }
      case 'RECEIVE_MIDDLEWAGE': {
        this.middleWage = action.middleWage;
        this.fetchingMiddleWage = false;
        this.emit('change');
        break;
      }
      case 'CHANGE_UNIT': {
        this.calculation.unit = this.units.filter(unit => unit._id === action.unitId)[0];
        this.emit('change');
        break;
      }
      case 'CHANGE_TITLE': {
        this.calculation.title = action.title;
        this.emit('change');
        break;
      }
      case 'CHANGE_TEXT': {
        this.calculation.text = action.text;
        this.emit('change');
        break;
      }
      case 'CHANGE_WAGE_MINUTES': {
        this.calculation.wageMinutes = action.wageMinutes;
        this.emit('change');
        break;
      }
    }
  }
}

const calculationStore = new CalculationStore;
calculationStore.setMaxListeners(0);
dispatcher.register(calculationStore.handleActions.bind(calculationStore));

export default calculationStore;