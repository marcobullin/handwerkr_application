import React from 'react';
import CalculationForm from './components/CalculationForm';
import CalculationStore from './stores/CalculationStore';
import { Redirect } from 'react-router-dom';
import * as CalculationActions from './actions/CalculationActions';
import Loading from '../common/Loading';

export default class UpdatePage extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      fetching: true,
      redirect: CalculationStore.redirect
    };

    this.handleOnChange = this.handleOnChange.bind(this);
    CalculationActions.getCalculation(this.props.match.params.calculationId);
  }

  handleOnChange() {
    this.setState({
      fetching: false,
      redirect: CalculationStore.redirect
    });
  }

  componentDidMount() {
    CalculationStore.on('change', this.handleOnChange);
  }

  componentWillUnmount() {
    CalculationStore.removeListener('change', this.handleOnChange);
    CalculationStore.reset();
  }

  render() {
    const { fetching, redirect } = this.state;
    
    if (fetching) {
      return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px'}}>
          <Loading />
        </div>
      );
    }

    if (redirect) {
      return(<Redirect to="/calculations" />);
    }
    
    return (
      <div>
        <div class="container-fluid">
          <h1>Kalkulation bearbeiten</h1>
          <CalculationForm />
        </div>
      </div>
    );
  }
}