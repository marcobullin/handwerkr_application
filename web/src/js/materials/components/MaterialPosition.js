import React from 'react';
import SuggestionItem from '../../common/SuggestionItem';
import MaterialStore from '../stores/MaterialStore';
import * as MaterialActions from '../actions/MaterialActions';

export default class MaterialPosition extends React.Component {
  constructor(props) {
    super(props);

    const { index, material } = props;
    this.material = material;

    this.state = {
      showSuggestions: false,
      suggestionIndex: 0,
      suggestions: MaterialStore.materials
    }

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange() {
    this.setState({
      suggestions: MaterialStore.materials
    });
  }

  componentDidMount() {
    this._isMounted = true;
    MaterialStore.on('change', this.handleChange);
  }

  componentWillUnmount() {
    this._isMounted = false;
    MaterialStore.removeListener('change', this.handleChange);
  }

  changeActivSuggestion(e) {
    const length = this.state.suggestions.length;

    if (e.keyCode === 38) {
      var index = this.state.suggestionIndex - 1;
      if (index < 0) {
        index = length - 1;
      }

      this.setState({
        suggestionIndex: index
      });

      return;
    } else if (e.keyCode === 40) {
      var index = this.state.suggestionIndex + 1;
      if (index >= length) {
        index = 0;
      }

      this.setState({
        suggestionIndex: index
      });

      return;
    } else if (e.keyCode === 27) {
      this.setState({
        showSuggestions: false,
        suggestionIndex: 0
      });

      return;
    } else if (e.keyCode === 13) {
      this.selectSuggestion(this.state.suggestionIndex);
      return;
    }
  }

  changeMaterialTitle(e) {
    const title = e.target.value;

    var _id = this.material._id;
    var showSuggestions = false;
    if (title.length > 0) {
      MaterialActions.searchMaterial(title);
      showSuggestions = true;
    } else {
      _id = '';
    }

    this.setState({
      showSuggestions: showSuggestions
    });

    this.props.updateMaterialPosition(this.props.index, {
      _id,
      title
    });
  }

  selectSuggestion(index) {
    this.setState({
      showSuggestions: false,
      suggestionIndex: 0
    });

    const { suggestions } = this.state;
    this.props.updateMaterialPosition(this.props.index, {
      _id: suggestions[index]._id || '',
      title: suggestions[index].title || '',
      sellingPrice: suggestions[index].sellingPrice || '',
      amount: suggestions[index].amount || '',
      unit: suggestions[index].unit || ''
    });
  }

  onBlur() {
    window.setTimeout(() => {
      if (this._isMounted && this.state.showSuggestions) {
        this.setState({
          showSuggestions: false,
          suggestionIndex: 0
        }); 
      }
    }, 500);
  }

  changeAmount(e) {
    this.props.updateMaterialPosition(this.props.index, {
      amount: e.target.value
    });
  }

  deleteMaterialPosition(e) {
    e.preventDefault();
    this.props.deleteMaterialPosition(this.props.index);
  }

  render() {
    const { showSuggestions, suggestions } = this.state;
    const { _id, title, sellingPrice, amount, unit } = this.material;

    var suggestionComponents = '';
    if (showSuggestions) {
      suggestionComponents = suggestions.slice(0, 10).map((material,i) => { 
        return <SuggestionItem key={material._id} selected={i === this.state.suggestionIndex} selectSuggestion={this.selectSuggestion.bind(this)} id={i} title={material.title}/>
      });
    }

    return (
      <div>
        <div class="well well-sm">
          <div class="row">
            <input type="hidden" ref="materialId" value={ _id || '' }/>
            <div class="col-md-3 col-xs-12 form-group" style={{ position: 'relative' }}>
              <label>Titel</label>
              <input type="text" ref="materialTitle" autoComplete="off" onBlur={this.onBlur.bind(this)} onChange={this.changeMaterialTitle.bind(this)} onKeyUp={this.changeActivSuggestion.bind(this)} value={ title || '' } class="form-control input-sm" placeholder="Titel"/>
              <div class="list-group" style={{ position: 'absolute', zIndex: 1 }}>
                { suggestionComponents }
              </div>
            </div>
            <div class="col-md-3 col-xs-12 form-group">
              <label>Verkaufspreis</label>
              <input type="number" ref="materialSellingPrice" class="form-control input-sm" placeholder="Verkaufspreis" value={sellingPrice || ''} disabled/>
            </div>
            <div class="col-md-3 col-xs-12 form-group">
              <label>Menge</label>
              <input type="number" ref="materialAmount" class="form-control input-sm" placeholder="Menge" value={amount || ''} onChange={this.changeAmount.bind(this)}/>
            </div>
            <div class="col-md-2 col-xs-12 form-group">
              <label>Einheit</label>
              <input type="text" ref="materialUnit" class="form-control input-sm" placeholder="Einheit" value={ unit ? unit.name : '' } disabled/>
            </div>
            <div class="col-md-1 col-xs-12 form-group">
              <label>Aktion</label>
              <button type="button" onClick={this.deleteMaterialPosition.bind(this)} class="btn btn-sm btn-outline-primary col-xs-12">Löschen</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}