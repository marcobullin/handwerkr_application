import React from 'react';
import * as MaterialActions from '../actions/MaterialActions';
import { Link } from 'react-router-dom';
import MaterialStore from '../stores/MaterialStore';
import moment from 'moment';

export default class MaterialForm extends React.Component {
  constructor(props) {
    super();

    this.state = {
      material: MaterialStore.material,
      units: MaterialStore.units
    }

    moment.locale("de");
  }

  changePurchasePrice() {
    const addition = parseFloat(this.refs.addition.value) || 0;
    const purchasePrice = parseFloat(this.refs.purchasePrice.value) || 0;
    const sellingPriceValue = purchasePrice * (100 + addition) / 100;
    const sellingPrice = Math.round(sellingPriceValue * 100) / 100

    MaterialActions.changeValues(sellingPrice, this.refs.purchasePrice.value, addition);
  }

  changeAddition() {
    const addition = parseFloat(this.refs.addition.value) || 0;
    const purchasePrice = parseFloat(this.refs.purchasePrice.value) || 0;
    const sellingPriceValue = purchasePrice * (100 + addition) / 100;
    const sellingPrice = Math.round(sellingPriceValue * 100) / 100;
    
    MaterialActions.changeValues(sellingPrice, purchasePrice, this.refs.addition.value);
  }

  changeSellingPrice() {
    const sellingPrice = parseFloat(this.refs.sellingPrice.value) || 0;
    const purchasePrice = parseFloat(this.refs.purchasePrice.value) || 0;
    const additionValue = (100 / purchasePrice) * sellingPrice - 100;
    const addition = Math.round(additionValue * 100) / 100;

    MaterialActions.changeValues(this.refs.sellingPrice.value, purchasePrice, addition);
  }

  handleSubmit() {
    const materialId = this.refs.materialId.value;
    const title = this.refs.title.value;
    const text = this.refs.text.value;
    const date = moment(this.refs.date.value, 'D.M.Y').format("YYYY/MM/DD");
    const purchasePrice = this.refs.purchasePrice.value;
    const addition = this.refs.addition.value;
    const sellingPrice = this.refs.sellingPrice.value;
    const amount = this.refs.amount.value;
    const unitId = this.refs.unitId.value;
   
    if (materialId) {
      return MaterialActions.updateMaterial(materialId, {
        title, text, date, purchasePrice, addition, sellingPrice, amount, unitId
      });
    }

    MaterialActions.createMaterial({
      title, text, date, purchasePrice, addition, sellingPrice, amount, unitId
    });
  }

  changeUnit() {
    MaterialActions.changeUnit(this.refs.unitId.value);
  }

  render() {
    const { material, units } = this.state;
    const { _id, materialId, title, text, date, purchasePrice, addition, sellingPrice, amount, unit } = material;

    const UnitComponents = units.map(unit => {
      return <option key={unit._id} value={ unit._id }>{ unit.name }</option>
    });

    return (
      <div>
        <input type="hidden" ref="materialId" defaultValue={materialId}/>
        <div class="form-group">
          <label for="title">Titel</label>
          <input type="text" ref="title" class="form-control input-sm" placeholder="Titel" defaultValue={title}/>
        </div>
        <div class="form-group">
          <label for="text">Beschreibungstext</label>
          <textarea ref="text" class="form-control input-sm" rows="3" placeholder="Beschreibungstext" defaultValue={text}></textarea>
        </div>
        <div class="form-group">
          <label for="date">Datum</label>
          <div class="input-group date">
              <input type="text" class="form-control input-sm" ref="date" placeholder="Datum" defaultValue={moment(date).format('DD.MM.YYYY')}/>
              <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
              </span>
          </div>
        </div>

        <div class="well well-sm">
          <div class="row">
            <div class="col-md-3 col-xs-12 form-group">
              <label for="purchasePrice">Einkaufspreis</label>
              <input type="number" ref="purchasePrice" class="form-control input-sm" onChange={this.changePurchasePrice.bind(this)} placeholder="Einkaufspreis" value={purchasePrice}/>
            </div>
            <div class="col-md-2 col-xs-12 form-group">
              <label for="addition">Aufschlag in %</label>
              <input type="number" ref="addition" class="form-control input-sm" onChange={this.changeAddition.bind(this)} placeholder="Aufschlag in %" value={addition}/>
            </div>
            <div class="col-md-3 col-xs-12 form-group">
              <label for="sellingPrice">Verkaufspreis</label>
              <input type="number" ref="sellingPrice" class="form-control input-sm" placeholder="Verkaufspreis" value={sellingPrice} onChange={this.changeSellingPrice.bind(this)}/>
            </div>
            <div class="col-md-2 col-xs-12 form-group">
              <label for="amount">Menge</label>
              <input type="number" ref="amount" class="form-control input-sm" placeholder="Menge" defaultValue={amount}/>
            </div>
            <div class="col-md-2 col-xs-12 form-group">
              <label for="unitId">Einheit</label>
              <select class="form-control input-sm" ref="unitId" onChange={this.changeUnit.bind(this)} value={unit._id}>
                { UnitComponents }  
              </select>
            </div>
          </div>
        </div>

        <Link to="/materials" class="btn btn-unimportant pull-left">Abbrechen</Link>
        <button type="button" onClick={this.handleSubmit.bind(this)} class="btn btn-action pull-right">Speichern</button>
      </div>
    );
  }
}