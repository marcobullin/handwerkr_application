import React from 'react';
import * as MaterialActions from '../actions/MaterialActions';
import { Link } from 'react-router-dom';
import moment from 'moment';

export default class MaterialTableEntry extends React.Component {
  deleteMaterial() {
    var answer = confirm('Möchtest du wirklich das Material "' + this.props.material.title + '" löschen? Achtung: Das Material wird auch aus den Kalkulationen entfernt in denen es verwendet wird.');
    
    if (answer === true) {
      MaterialActions.deleteMaterial(this.props.material._id, this.props.index);
    }
  }

  render() {
    const { materialId, title, text, sellingPrice, purchasePrice, addition, amount, date, unit } = this.props.material;
    
    return (
      <tr>
        <td>{ title }</td>
        <td>{ text }</td>
        <td>{ sellingPrice }&nbsp;€</td>
        <td>{ purchasePrice }&nbsp;€&nbsp;({ addition || 0 }&nbsp;%)</td>
        <td>{ amount } { unit.name }</td>
        <td>{ moment(new Date(date)).format('DD.MM.YYYY') }</td>
        <td>
          <Link to={'/materials/update/' + materialId} class="btn btn-secondary btn-sm"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></Link>&nbsp;
          <button type="button" onClick={this.deleteMaterial.bind(this)} data-id={materialId} class="btn btn-outline-primary btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
        </td>
      </tr>
    );
  }
}