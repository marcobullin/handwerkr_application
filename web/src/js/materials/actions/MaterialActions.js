import dispatcher from '../../dispatcher';
import xhttp from '../../xhttp';

export function createMaterial(material) {
  xhttp('POST', '/materials/new', material).then(() => {
    dispatcher.dispatch({type: 'CREATED_MATERIAL'});
  });
}

export function updateMaterial(materialId, material) {
  xhttp('POST', '/materials/' + materialId, material).then(() => {
    dispatcher.dispatch({type: 'UPDATED_MATERIAL'});
  });
}

export function searchMaterial(value, page) {
  xhttp('GET', '/materials/search/' + encodeURIComponent(value) + '?page=' + (page || 1)).then(data => {
    dispatcher.dispatch({type: 'FOUND_MATERIALS', materials: data.materials, pages: data.pages, page: data.page});
  });
}

export function deleteMaterial(materialId, materialIndex) {
  xhttp('POST', '/materials/delete/' + materialId).then(() => {
    dispatcher.dispatch({type: 'DELETED_MATERIAL', materialIndex});
  });
}

export function getMaterial(materialId) {
  xhttp('GET', '/materials/' + materialId).then(data => {
    dispatcher.dispatch({type: 'FOUND_MATERIAL', material: data.material, units: data.units});
  });
}

export function getAll() {
  xhttp('GET', '/materials').then(data => {
    dispatcher.dispatch({type: 'RECEIVE_MATERIALS', materials: data.materials, pages: data.pages, page: data.page});
  });
}

export function getUnits() {
  xhttp('GET', '/units').then(data => {
    dispatcher.dispatch({type: 'RECEIVE_UNITS', units: data});
  });
}

export function changeValues(sellingPrice, purchasePrice, addition) {
  dispatcher.dispatch({type: 'CHANGE_VALUES', sellingPrice, purchasePrice, addition});
}

export function changeUnit(unitId) {
  dispatcher.dispatch({type: 'CHANGE_UNIT', unitId});
}
