import React from 'react';
import MaterialForm from './components/MaterialForm';
import * as MaterialActions from './actions/MaterialActions';
import MaterialStore from './stores/MaterialStore';
import { Redirect } from 'react-router-dom';
import Loading from '../common/Loading';

export default class UpdatePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fetching: true,
      redirect: MaterialStore.redirect,
      material: MaterialStore.material
    };

    this.handleChange = this.handleChange.bind(this);
    MaterialActions.getMaterial(this.props.match.params.materialId);
  }

  handleChange() {
    this.setState({
      fetching: false,
      redirect: MaterialStore.redirect,
      material: MaterialStore.material
    });
  }

  componentDidMount() {
    MaterialStore.on('change', this.handleChange);
  }

  componentWillUnmount() {
    MaterialStore.reset();
    MaterialStore.removeListener('change', this.handleChange);
  }

  render() {
    const { fetching, redirect } = this.state
    
    if (fetching) {
      return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px'}}>
          <Loading />
        </div>
      );
    }

    if (redirect) {
      return <Redirect to="/materials" />;
    }
    
    return (
      <div>
        <div class="container-fluid">
          <h1>Material bearbeiten</h1>
          <MaterialForm />
        </div>
      </div>
    );
  }
}