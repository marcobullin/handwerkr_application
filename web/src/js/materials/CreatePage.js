import React from 'react';
import MaterialForm from './components/MaterialForm';
import { Redirect } from 'react-router-dom';
import Loading from '../common/Loading';
import * as MaterialActions from './actions/MaterialActions';
import MaterialStore from './stores/MaterialStore';

export default class CreatePage extends React.Component {
  constructor() {
    super();
    this.state = {
      fetching: true,
      redirect: MaterialStore.redirect,
      units: MaterialStore.units
    };

    this.handleChange = this.handleChange.bind(this);
    MaterialActions.getUnits();
  }

  handleChange() {
    this.setState({
      fetching: false,
      redirect: MaterialStore.redirect,
      units: MaterialStore.units
    });
  }

  componentDidMount() {
    MaterialStore.on('change', this.handleChange);
  }

  componentWillUnmount() {
    MaterialStore.reset();
    MaterialStore.removeListener('change', this.handleChange);
  }

  render() {
    const { fetching, redirect } = this.state;
    
    if (fetching) {
      return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px'}}>
          <Loading />
        </div>
      );
    }

    if (redirect) {
      return <Redirect to="/materials" />;
    }

    return (
      <div>
        <div class="container-fluid">
          <h1>Neues Material anlegen</h1>
          <MaterialForm />
        </div>
      </div>
    );
  }
}