import React from 'react';
import MaterialTableEntry from './components/MaterialTableEntry';
import MaterialStore from './stores/MaterialStore';
import * as MaterialActions from './actions/MaterialActions';
import MaterialForm from './components/MaterialForm';
import { Link } from 'react-router-dom';
import Loading from '../common/Loading';

export default class OverviewPage extends React.Component {
  constructor() {
    super();
    
    this.state = {
      fetching: true,
      materials: MaterialStore.getAll(),
      pages: MaterialStore.pages,
      page: MaterialStore.page
    };

    this.handleChange = this.handleChange.bind(this);
    MaterialActions.getAll();
  }

  componentDidMount() {
    MaterialStore.on('change', this.handleChange);
  }

  componentWillUnmount() {
    MaterialStore.removeListener('change', this.handleChange);
    MaterialStore.reset();
  }

  handleChange() {
    this.setState({
      fetching: false,
      materials: MaterialStore.getAll(),
      pages: MaterialStore.pages,
      page: MaterialStore.page
    });
  }

  searchMaterial(e) {
    MaterialActions.searchMaterial(e.target.value, 1);
  }

  changePage(page, e) {
    e.preventDefault();

    MaterialActions.searchMaterial(this.refs.search.value, page);
  }

  render() {
    const { fetching, materials, pages, page } = this.state;
    
    if (fetching) {
      return (
        <div style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '400px'}}>
          <Loading />
        </div>
      );
    }

    const MaterialComponents = materials.map((material, i) => { 
      return <MaterialTableEntry key={material._id} index={i} material={material} /> 
    });

    let PaginationComponents = [];
    for (let i = 1; i <= parseInt(pages); i++) {
      PaginationComponents.push(<li key={i} class={ i === parseInt(page) ? 'active' : ''}><a href="#" onClick={this.changePage.bind(this, i)}>{ i }</a></li>);
    }

    return (
      <div>
        <div class="container-fluid">
          <h1>Materialien</h1>
          <div class="panel panel-default">
            <div class="panel-heading">Alle Materialien</div>
            <div class="panel-body">
              <input type="text" ref="search" onChange={this.searchMaterial.bind(this)} class="form-control" placeholder="Material suchen"/>
              <br/>
              <Link to="/materials/create" class="btn btn-action">Material anlegen</Link>
            </div>
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th class="col-md-3">Titel</th>
                    <th class="col-md-4">Text</th>
                    <th class="col-md-1">Verkaufspreis</th>
                    <th class="col-md-1">Einkaufspreis</th>
                    <th class="col-md-1">Menge</th>
                    <th class="col-md-1">Datum</th>
                    <th class="col-md-1">Aktion</th>
                  </tr>
                </thead>
                <tbody>
                  { MaterialComponents }
                </tbody>
              </table>
            </div>
          </div>
          <nav aria-label="Page navigation" style={{textAlign: 'center'}}>
            <ul class="pagination">
              <li>
                <a href="#" onClick={this.changePage.bind(this, parseInt(page)-1 || 1 )} aria-label="Previous">
                  <span aria-hidden="true">&laquo;</span>
                </a>
              </li>
              { PaginationComponents }
              <li>
                <a href="#" onClick={this.changePage.bind(this, (parseInt(page) + 1 <= pages) ? (parseInt(page) + 1) : pages )} aria-label="Next">
                  <span aria-hidden="true">&raquo;</span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    );
  }
}