import { EventEmitter } from 'events';
import dispatcher from '../../dispatcher';

class MaterialStore extends EventEmitter {
  constructor() {
    super();
    this.reset();
  }

  getMaterial(materialId) {
    return this.materials.filter(entry => entry.materialId === materialId)[0];
  }

  getAll() {
    return this.materials;
  }

  reset() {
    this.redirect = false;
    this.materials = [];
    this.pages = 1;
    this.page = 1;
    this.material = {
      _id: '',
      materialId: '',
      text: '',
      date: new Date(),
      sellingPrice: '',
      purchasePrice: '',
      addition: '',
      amount: '',
      unit: {
        _id: '',
        name: '',
        unitId: ''
      }
    };
    this.units = [];
  }

  handleActions(action) {
    switch(action.type) {
      case 'CREATED_MATERIAL': {
        this.redirect = true;
        setTimeout(() => {this.emit('change');}, 0);
        break;
      }
      case 'UPDATED_MATERIAL': {
        this.redirect = true;
        setTimeout(() => {this.emit('change');}, 0);
        break;
      }
      case 'DELETED_MATERIAL': {
        this.materials.splice(action.materialIndex, 1);
        this.emit('change');
        break;
      }
      case 'FOUND_MATERIAL': {
        this.material = action.material;
        this.units = action.units;
        this.emit('change');
        break;
      }
      case 'FOUND_MATERIALS': {
        this.materials = action.materials;
        this.pages = action.pages;
        this.page = action.page;
        this.emit('change');
        break;
      }
      case 'RECEIVE_MATERIALS': {
        this.materials = action.materials;
        this.pages = action.pages;
        this.page = action.page;
        this.emit('change');
        break;
      }
      case 'RECEIVE_UNITS': {
        this.units = action.units;
        this.emit('change');
        break;
      }
      case 'CHANGE_VALUES': {
        this.material.sellingPrice = action.sellingPrice;
        this.material.purchasePrice = action.purchasePrice;
        this.material.addition = action.addition;
        this.emit('change');
        break;
      }
      case 'CHANGE_UNIT': {
        this.material.unit = this.units.filter(unit => unit._id === action.unitId)[0];
        this.emit('change');
        break;
      }
    }
  }
}

const materialStore = new MaterialStore;
materialStore.setMaxListeners(0);
dispatcher.register(materialStore.handleActions.bind(materialStore));

export default materialStore;