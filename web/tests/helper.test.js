import * as Helper from '../src/js/Helper';

test('ust of wage', () => {
  expect(Helper.getUst(100, 19)).toBe(19);
});

test('summary of net wage for calculation positions', () => {
  const sections = [{
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '2'
      }] 
    }]
  }, {
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '2'
      }] 
    }]
  }];

  expect(Helper.getNetWage(sections)).toBe(100); 
});

test('summary of net wage for calculation positions with invalid values', () => {
  const sections = [{
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: 'xx',
        positionWageMinutes: 60,
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '2'
      }] 
    }]
  }, {
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: 'yy'
      },
      {
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '2'
      }] 
    }]
  }];

  expect(Helper.getNetWage(sections)).toBe(80); 
});

test('summary of net wage for calculation with mixed positions', () => {
  const sections = [{
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionType: 'material',
        positionWagePortion: '10',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '2'
      }] 
    }]
  }, {
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionType: 'material',
        positionWagePortion: '10',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '2'
      }] 
    }]
  }];

  expect(Helper.getNetWage(sections)).toBe(80); 
});

test('summary of net wage for calculation with updated values', () => {
  const sections = [{
    subSections: [{
      positions: [
      {
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionNewMiddleWage: '20',
        positionWageMinutes: 60,
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '2'
      }] 
    }]
  }, {
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionNewWageMinutes: 30,
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionType: 'material',
        positionMiddleWage: '10',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionType: 'calculation',
        positionMiddleWage: '10',
        positionWageMinutes: 60,
        positionQuantity: '2'
      }] 
    }]
  }];

  expect(Helper.getNetWage(sections)).toBe(85); 
});

test('calculate discount amount for simple values', () => {
  expect(Helper.getDiscountAmount(20, 10)).toBe("2.00"); 
});

test('calculate discount amount for complex values', () => {
  expect(Helper.getDiscountAmount(613.83, 5)).toBe("30.69"); 
});

test('calculate discount amount for strange values', () => {
  expect(Helper.getDiscountAmount(0, 10)).toBe("0.00"); 
});