import * as Computing from '../src/js/offers/computing';

test('get net amount of sections price', () => {
  expect(Computing.getSectionsNetAmount(100, 19)).toBe(19);
});

test('get allowance amount of offer price', () => {
  expect(Computing.getAllowanceAmount(102.98, 2)).toBe(2.0596);
});

test('get sections price without discount', () => {
  const sections = [{
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionPrice : 11,
        positionQuantity: 1
      }]
    }]
  }];

  expect(Computing.getSectionsPrice(sections)).toBe(11);
});

test('get sections price with 10% discount', () => {
  const sections = [{
    subSections: [{
      positions: [{
        positionMode: '',
        positionPrice : 10,
        positionNewPrice : 11,
        positionQuantity: 1
      }]
    }],
    discount: 10
  }];

  expect(Computing.getSectionsPrice(sections)).toBe(9.9);
});

test('get section price of positions with normal mode and unchanged price', () => {
  const section = {
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionNewPrice: '',
        positionPrice: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewPrice: '',
        positionPrice: '25',
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionNewPrice: '',
        positionPrice: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewPrice: '',
        positionPrice: '25',
        positionQuantity: '1'
      }] 
    }],
    discount: 5
  };

  expect(Computing.getSectionPrice(section)).toBe(95); 
});

test('get section price of positions with normal mode and invalid prices', () => {
  const section = {
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionNewPrice: '',
        positionPrice: 'xx',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewPrice: '',
        positionPrice: '25',
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionNewPrice: '',
        positionPrice: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewPrice: '',
        positionPrice: '',
        positionQuantity: '1'
      }] 
    }],
    discount: 10
  };

  expect(Computing.getSectionPrice(section)).toBe(45); 
});

test('get section price of positions with normal mode and changed prices', () => {
  const section = {
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionNewPrice: '50',
        positionPrice: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewPrice: '',
        positionPrice: '25',
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionNewPrice: '50',
        positionPrice: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewPrice: '',
        positionPrice: '25',
        positionQuantity: '1'
      }] 
    }],
    discount: 10
  };

  expect(Computing.getSectionPrice(section)).toBe(135); 
});

test('get section price of positions with normal mode and invalid changed prices', () => {
  const section = {
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionNewPrice: 'xx',
        positionPrice: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewPrice: '',
        positionPrice: '50',
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionNewPrice: 'yy',
        positionPrice: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewPrice: '',
        positionPrice: '50',
        positionQuantity: '1'
      }] 
    }],
    discount: 10
  };

  expect(Computing.getSectionPrice(section)).toBe(135); 
});

test('get section price of positions with alternative mode', () => {
  const section = {
    subSections: [{
      positions: [{
        positionMode: 'alternative',
        positionNewPrice: '',
        positionPrice: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewPrice: '',
        positionPrice: '50',
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'alternative',
        positionNewPrice: '',
        positionPrice: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewPrice: '',
        positionPrice: '50',
        positionQuantity: '1'
      }] 
    }],
    discount: 2.5
  };

  expect(Computing.getSectionPrice(section)).toBe(97.50); 
});

test('get section price of positions with eventual mode', () => {
  const section = {
    subSections: [{
      positions: [{
        positionMode: 'eventual',
        positionNewPrice: '',
        positionPrice: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewPrice: '',
        positionPrice: '50',
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'eventual',
        positionNewPrice: '',
        positionPrice: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewPrice: '',
        positionPrice: '50',
        positionQuantity: '1'
      }] 
    }],
    discount: 5.5
  };

  expect(Computing.getSectionPrice(section)).toBe(94.50); 
});

test('get total wage minutes for sections', () => {
  const sections = [{
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      }] 
    }]
  }, {
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      }] 
    }]
  }];

  expect(Computing.getTotalWageMinutes(sections)).toBe(200); 
});

test('get total wage minutes for invalid positions in sections', () => {
  const sections = [{
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: 'xx',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      }] 
    }]
  }, {
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: 'xx',
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      }] 
    }]
  }];

  expect(Computing.getTotalWageMinutes(sections)).toBe(150); 
});

test('get total wage minutes for changed positions in sections', () => {
  const sections = [{
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '50',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      }] 
    }]
  }, {
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '50',
        positionWageMinutes: '25',
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      }] 
    }]
  }];

  expect(Computing.getTotalWageMinutes(sections)).toBe(250); 
});

test('get total wage minutes for invalid and changed positions in sections', () => {
  const sections = [{
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: 'xx',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      }] 
    }]
  }, {
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: 'yy',
        positionWageMinutes: '25',
        positionQuantity: '2'
      }] 
    }]
  }];

  expect(Computing.getTotalWageMinutes(sections)).toBe(225); 
});

test('get total wage minutes for non normal mode', () => {
  const sections = [{
    subSections: [{
      positions: [{
        positionMode: 'alternative',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      }] 
    }]
  }, {
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'eventual',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '2'
      }] 
    }]
  }];

  expect(Computing.getTotalWageMinutes(sections)).toBe(150); 
});

test('get total wage minutes as text in hours', () => {
  const sections = [{
    subSections: [{
      positions: [{
        positionMode: 'alternative',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      }] 
    }]
  }, {
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'eventual',
        positionNewWageMinutes: '',
        positionWageMinutes: '25',
        positionQuantity: '2'
      }] 
    }]
  }];

  expect(Computing.getTotalWageMinutesAsText(sections)).toBe('2.50 h'); 
});

test('get total wage minutes as text in minutes', () => {
  const sections = [{
    subSections: [{
      positions: [{
        positionMode: 'alternative',
        positionNewWageMinutes: '',
        positionWageMinutes: '10',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '10',
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '10',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '10',
        positionQuantity: '1'
      }] 
    }]
  }, {
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '10',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '10',
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionNewWageMinutes: '',
        positionWageMinutes: '10',
        positionQuantity: '1'
      },
      {
        positionMode: 'eventual',
        positionNewWageMinutes: '',
        positionWageMinutes: '10',
        positionQuantity: '2'
      }] 
    }]
  }];

  expect(Computing.getTotalWageMinutesAsText(sections)).toBe('60.00 min'); 
});

test('get profit for material position', () => {
  expect(Computing.getLossOrProfitForMaterial(10, 11, 2)).toBe(2); 
});

test('get no loss and no profit for material position', () => {
  expect(Computing.getLossOrProfitForMaterial(10, 10, 2)).toBe(0); 
});

test('get loss for material position', () => {
  expect(Computing.getLossOrProfitForMaterial(10, 9, 2)).toBe(-2); 
});

test('get profit for calculation position', () => {
  expect(Computing.getLossOrProfitForCalculation(
    60, 
    60, 
    49.24, 
    49.24, 
    [], 
    undefined, 
    undefined, 
    1).toFixed(2)).toBe("17.78");
});

test('get profit for calculation position with new wage minutes', () => {
  expect(Computing.getLossOrProfitForCalculation(
    60, 
    120, 
    49.24, 
    49.24, 
    [], 
    undefined, 
    undefined, 
    1).toFixed(2)).toBe("35.56");
});

test('get profit for calculation position with one material', () => {
  expect(Computing.getLossOrProfitForCalculation(
    60, 
    60, 
    49.24, 
    49.24, 
    [{
      purchasePrice: 10,
      amount: 1
    }], 
    11, 
    undefined, 
    1).toFixed(2)).toBe("18.78");
});

test('get profit for calculation position with multiple materials', () => {
  expect(Computing.getLossOrProfitForCalculation(
    60, 
    60, 
    49.24, 
    49.24, 
    [{
      purchasePrice: 10,
      amount: 2
    }, {
      purchasePrice: 10,
      amount: 1
    }], 
    33, 
    undefined, 
    1).toFixed(2)).toBe("20.78");
});

test('get profit for calculation position with updated material portion of 0', () => {
  expect(Computing.getLossOrProfitForCalculation(
    60, 
    60, 
    49.24, 
    49.24, 
    [{
      purchasePrice: 10,
      amount: 1
    }], 
    11, 
    0, 
    1).toFixed(2)).toBe("7.78");
});

test('get profit for calculation position with updated material portion of 0 and updated wage minutes', () => {
  expect(Computing.getLossOrProfitForCalculation(
    60, 
    120, 
    49.24, 
    49.24, 
    [{
      purchasePrice: 10,
      amount: 1
    }], 
    11, 
    0, 
    1).toFixed(2)).toBe("25.56");
});

test('get profit for calculation position with updated material portion of value 34', () => {
  expect(Computing.getLossOrProfitForCalculation(
    60, 
    60, 
    49.24, 
    49.24, 
    [{
      purchasePrice: 10,
      amount: 1
    }, {
      purchasePrice: 10,
      amount: 2
    }], 
    33, 
    34, 
    1).toFixed(2)).toBe("21.78");
});

test('get profit for calculation position with updated quantity of value 2', () => {
  expect(Computing.getLossOrProfitForCalculation(
    60, 
    60, 
    49.24, 
    49.24, 
    [{
      purchasePrice: 10,
      amount: 1
    }, {
      purchasePrice: 10,
      amount: 2
    }], 
    33, 
    undefined, 
    2).toFixed(2)).toBe("41.56");
});

test('get loss for calculation position', () => {
  expect(Computing.getLossOrProfitForCalculation(
    120, 
    60, 
    49.24, 
    0, 
    [{
      purchasePrice: 38,
      amount: 1
    }], 
    undefined, 
    undefined, 
    1)).toBe(-38);
});

test('get profit for section with material positions', () => {
  var section = {
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionType: 'material',
        positionPurchasePrice: '10',
        positionPrice: '11',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionType: 'material',
        positionPurchasePrice: '10',
        positionPrice: '11',
        positionNewPrice: '12',
        positionQuantity: '1'
      }] 
    }],
    discount: 0
  };

  expect(Computing.getLossOrProfitForSection(section)).toBe(3); 
});

test('get no profit or loss for section with material positions', () => {
  var section = {
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionType: 'material',
        positionPurchasePrice: '10',
        positionPrice: '11',
        positionQuantity: '2'
      },
      {
        positionMode: 'normal',
        positionType: 'material',
        positionPurchasePrice: '10',
        positionPrice: '11',
        positionNewPrice: '9',
        positionQuantity: '2'
      }] 
    }],
    discount: 0
  };

  expect(Computing.getLossOrProfitForSection(section)).toBe(0); 
});

test('get loss for section with material positions', () => {
  var section = {
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionType: 'material',
        positionPurchasePrice: '10',
        positionPrice: '11',
        positionQuantity: '2'
      },
      {
        positionMode: 'normal',
        positionType: 'material',
        positionPurchasePrice: '10',
        positionPrice: '11',
        positionNewPrice: '8',
        positionQuantity: '2'
      }] 
    }],
    discount: 0
  };

  expect(Computing.getLossOrProfitForSection(section)).toBe(-2); 
});

test('get profit for section with discount', () => {
  var section = {
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionType: 'material',
        positionPurchasePrice: '10',
        positionPrice: '11',
        positionQuantity: '2'
      },
      {
        positionMode: 'normal',
        positionType: 'material',
        positionPurchasePrice: '10',
        positionPrice: '11',
        positionQuantity: '2'
      }] 
    }],
    discount: 10
  };

  expect(Computing.getLossOrProfitForSection(section)).toBe(3.6); 
});

test('get loss for section with discount', () => {
  var section = {
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionType: 'material',
        positionPurchasePrice: '11',
        positionPrice: '12',
        positionNewPrice: '10',
        positionQuantity: '2'
      }] 
    }],
    discount: 10
  };

  expect(Computing.getLossOrProfitForSection(section)).toBe(-2.2); 
});

test('get position total price', () => {
  expect(Computing.getPositionTotal(100, 2)).toBe(200);
});

test('get position total price with floating numbers', () => {
  expect(Computing.getPositionTotal(75.99, 1)).toBe(75.99);
});

test('get position total price with floating numbers and higher quantity', () => {
  expect(Computing.getPositionTotal(75, 3)).toBe(225);
});

test('get section price of positions ignoring the discount', () => {
  const section = {
    subSections: [{
      positions: [{
        positionMode: 'normal',
        positionNewPrice: '',
        positionPrice: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewPrice: '',
        positionPrice: '25',
        positionQuantity: '1'
      }] 
    }, {
      positions: [{
        positionMode: 'normal',
        positionNewPrice: '',
        positionPrice: '25',
        positionQuantity: '1'
      },
      {
        positionMode: 'normal',
        positionNewPrice: '',
        positionPrice: '25',
        positionQuantity: '1'
      }] 
    }],
    discount: 5
  };

  expect(Computing.getSectionPrice(section)).toBe(95);
  expect(Computing.getSectionPriceWithoutDiscount(section)).toBe(100);
  expect(Computing.getSectionPrice(section)).toBe(95);
});

test('get partial payment amount', () => {
  expect(Computing.getPartialPaymentAmount(100, 25)).toBe(25);
});
