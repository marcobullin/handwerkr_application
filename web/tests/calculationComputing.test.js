import * as Computing from '../src/js/calculations/computing';

test('get average wage per minute', () => {
  expect(Computing.getAverageWagePerMinute(30)).toBe(0.5);
})

test('get wage portion', () => {
  expect(Computing.getWagePortion(55, 0.637)).toBe("35.04");
});

test('summary of material portion', () => {
  const positions = [{
    sellingPrice: '50',
    amount: '2'
  },
  {
    sellingPrice: '25',
    amount: '4'
  }];

  expect(Computing.getMaterialPortion(positions)).toBe("200.00"); 
});

test('summary of material portion with invalid price values', () => {
  const positions = [{
    sellingPrice: 'xx',
    amount: '2'
  },
  {
    sellingPrice: '25',
    amount: '4'
  }];

  expect(Computing.getMaterialPortion(positions)).toBe("100.00"); 
});

test('summary of material portion with invalid amount values', () => {
  const positions = [{
    sellingPrice: '50',
    amount: 'xx'
  },
  {
    sellingPrice: '25',
    amount: '4'
  }];

  expect(Computing.getMaterialPortion(positions)).toBe("100.00"); 
});

test('get total of wage and material portion', () => {
  expect(Computing.getCalculationPriceOfWageAndMaterialPortion(55.22, 0.23)).toBe("55.45");
});

test('get calculation end price', () => {
  expect(Computing.getCalculationPrice({
    wageMinutes: 60,
    materials: [
      {
        sellingPrice: 11,
        amount: 1
      }
    ]
  }, 38)).toBe("49.00");
});
