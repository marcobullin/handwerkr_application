var mongoose = require('mongoose');

var wageSchema = mongoose.Schema({
  middleWage: Number
});

module.exports = mongoose.model('Wage', wageSchema);
