var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var calculationSchema = mongoose.Schema({
  calculationId: Number,
  title: String,
  text: String,
  updated: Date,
  wageMinutes: Number,
  unit: { type: mongoose.Schema.ObjectId, ref: 'Unit' },
  materialMapping: [{ type: mongoose.Schema.ObjectId, ref: 'MaterialMapping' }],
});

calculationSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Calculation', calculationSchema);
