var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var invoiceSchema = mongoose.Schema({
  invoiceDate: String,
  invoiceYear: Number,
  invoiceCount: Number,
  offer: {type: mongoose.Schema.ObjectId, ref: 'Offer'},
  tax: Number,
  name: String,
  editor: String,
  offerDate: String,
  status: String,
  created: Date,
  updated: Date,
  customer: mongoose.Schema.Types.Mixed,
  sections: [mongoose.Schema.Types.Mixed],
  dueDay: Number,
  dueDate: String,
  allowance: Number,
  dueDayAllowance: Number,
  invoiceId: String,
  dueDateAllowance: String,
  withoutTax: Boolean,
  type: String,
  showLawText: Boolean,
  partialPayment: Number,
  partialPaymentAmount: Number
});

invoiceSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Invoice', invoiceSchema);
