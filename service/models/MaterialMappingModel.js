var mongoose = require('mongoose');

var materialMappingSchema = mongoose.Schema({
  //materialMappingId: Number,
  material: { type: mongoose.Schema.ObjectId, ref: 'Material' },
  amount: Number
});

//materialMappingSchema.index({ materialMappingId: 1 }, { unique: true });

module.exports = mongoose.model('MaterialMapping', materialMappingSchema);
