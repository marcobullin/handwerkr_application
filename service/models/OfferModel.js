var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var offerSchema = mongoose.Schema({
  offerId: String,
  tax: Number,
  name: String,
  editor: String,
  offerDate: String,
  created: Date,
  updated: Date,
  customer: mongoose.Schema.Types.Mixed,
  sections: [mongoose.Schema.Types.Mixed],
  showIntroduction: Boolean,
  showLawText: Boolean,
  showMfg: Boolean,
  invoice: {type: mongoose.Schema.ObjectId, ref: 'Invoice'},
  invoices: [{type: mongoose.Schema.ObjectId, ref: 'Invoice'}],
  offerYear: Number,
  offerCount: Number,
  withoutTax: Boolean,
  partialPayment: Number,
  partialInvoice: {type: mongoose.Schema.ObjectId, ref: 'Invoice'},
  partialPaymentAmount: Number
});

offerSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Offer', offerSchema);
