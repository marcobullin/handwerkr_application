var mongoose = require('mongoose');

var taxSchema = mongoose.Schema({
  tax: Number
});

module.exports = mongoose.model('Tax', taxSchema);
