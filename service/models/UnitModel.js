var mongoose = require('mongoose');

var unitSchema = mongoose.Schema({
  unitId: Number,
  name: String
});

unitSchema.index({ unitId: 1 }, { unique: true });

module.exports = mongoose.model('Unit', unitSchema);
