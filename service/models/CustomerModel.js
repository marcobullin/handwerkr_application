var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var customerSchema = mongoose.Schema({
  customerId: Number,
  title: String,
  firstname: String,
  lastname: String,
  name: String,
  street: String,
  city: String,
  zip: String,
  mail: String,
  phone: String
});

customerSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Customer', customerSchema);
