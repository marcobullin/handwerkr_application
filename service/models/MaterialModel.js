var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var materialSchema = mongoose.Schema({
  materialId: Number,
  title: String,
  text: String,
  purchasePrice: Number,
  sellingPrice: Number,
  addition: Number,
  amount: Number,
  unit: { type: mongoose.Schema.ObjectId, ref: 'Unit' },
  date: Date
});

materialSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Material', materialSchema);
