var logger = require('../../lib/logger');
var UnitModel = require('../../models/UnitModel');

/**
 * Returns units
 *
 * @param {Object} req - request object
 * @param {Object} res - response object
 *
 * @returns {void}
 */
function getUnits(req, res) {
  logger.debug('Collecting units.');
  
  UnitModel.find({})
    .then(units => {
      res.send(JSON.stringify(units));
    });
}

module.exports = {
  getUnits: getUnits
}