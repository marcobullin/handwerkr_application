var logger = require('../../lib/logger');
var OfferModel = require('../../models/OfferModel');
var CustomerModel = require('../../models/CustomerModel');
var UnitModel = require('../../models/UnitModel');
var InvoiceModel = require('../../models/InvoiceModel');
var moment = require('moment');

/**
 * Returns all offers
 *
 * @param {Object} req - request object
 * @param {Object} res - response object
 *
 * @returns {void}
 */
function getOffers(req, res) {
  OfferModel.paginate({}, {populate: {path: 'invoices', model: 'Invoice'}, sort: {offerId: -1}, limit: 10})
    .then(result => {
      var data = {
        offers: result.docs,
        page: result.page,
        pages: result.pages
      };
      
      res.send(JSON.stringify(data));
    });
}

/**
 * Returns a specifc offer
 *
 * @param {Object} req - request object
 * @param {Object} res - response object
 *
 * @returns {void}
 */
function getOfferById(req, res) {
  OfferModel.findOne({offerId: req.params.offerId}).populate('invoices')
    .then(offer => {
      if (!offer) {
        return res.send(JSON.stringify({offerId: req.params.offerId}));
      }

      UnitModel.find({}).then(units => {
        res.send(JSON.stringify({offer: offer, units: units}));
      })
    });
}

/**
 * Returns all offers found for search value
 *
 * @param {Object} req - request object
 * @param {Object} res - response object
 *
 * @returns {void}
 */
function getOffersByValue(req, res) {
  var value = new RegExp(req.params.value, 'i');
  var query = { $or: [{name: value}, {editor: value}, {offerId: value}, {'customer.name': value}] };
  var page = req.query.page || 1;

  OfferModel.paginate(query, {populate: {path: 'invoices', model: 'Invoice'}, sort: {offerId: -1}, page: page, limit: 10})
    .then(doc => {
      var data = {
        offers: doc.docs,
        page: doc.page,
        pages: doc.pages
      }
      res.send(JSON.stringify(data));
    });
}

function getCustomer(customerId) {
  if (!customerId) {
    return Promise.resolve(null);
  }

  return CustomerModel.findOne({ customerId: customerId});
}

function updateOffer(req, res) {
  var currentTime = moment().format();
  var incomingOffer = {
    customerId: req.body.customerId ? parseInt(req.body.customerId, 10) : 0,
    tax: req.body.tax,
    name: req.body.name,
    editor: 'Andreas Francke',
    updated: currentTime,
    offerDate: req.body.offerDate,
    sections: req.body.sections ? JSON.parse(req.body.sections) : [],
    showIntroduction: req.body.showIntroduction,
    showLawText: req.body.showLawText,
    showMfg: req.body.showMfg,
    offerCount: req.body.offerCount,
    offerYear: req.body.offerYear,
    withoutTax: req.body.withoutTax,
    partialPayment: req.body.partialPayment,
    partialPaymentAmount: req.body.partialPaymentAmount
  };

  getCustomer(req.body.customerId)
    .then(customer => {
      incomingOffer.customer = customer;

      OfferModel.findOne({_id: req.params.offerId})
        .then(offer => {
          // no offer found -> let's create one
          if (!offer) {
            incomingOffer.created = currentTime;
            return new OfferModel(incomingOffer).save(() => {
              res.send();
            });
          }
    
          // let's update existing offer
          OfferModel.update({offerId: offer.offerId}, incomingOffer)
            .then(() => {
              res.send();
            });
        });
    });
}

function deleteOffer(req, res) {
  logger.debug('Deleting the incoming offer.');

  OfferModel.remove({_id: req.params.offerId})
    .then(() => {
      res.send();
    });
}

function createOffer(req, res) {
  var currentYear = (new Date()).getFullYear();
  var incomingOffer = {
    customerId: req.body.customerId ? req.body.customerId : 0,
    tax: req.body.tax,
    name: req.body.name,
    editor: 'Andreas Francke',
    updated: moment().format(),
    offerDate: req.body.offerDate,
    sections: req.body.sections ? JSON.parse(req.body.sections) : [],
    showIntroduction: req.body.showIntroduction,
    showLawText: req.body.showLawText,
    showMfg: req.body.showMfg,
    withoutTax: req.body.withoutTax,
    partialPayment: req.body.partialPayment,
    partialPaymentAmount: req.body.partialPaymentAmount
  };

  getCustomer(req.body.customerId)
    .then(customer => {
      incomingOffer.customer = customer;
      OfferModel.find({offerYear: currentYear}).sort({offerCount: -1}).limit(1).then(latestOffer => {
        incomingOffer.offerYear = currentYear;

        if (!latestOffer || latestOffer.length === 0) {
          incomingOffer.offerCount = 1;
        } else {
          incomingOffer.offerCount = latestOffer[0].offerCount + 1;
        }

        incomingOffer.offerId = incomingOffer.offerYear + '-' + incomingOffer.offerCount.toString().padStart(4, "0");

        OfferModel.collection.insert(incomingOffer)
          .then(doc => {
            res.send(JSON.stringify(doc.ops[0]));
          });
      });
    });
}

module.exports = {
  getOffers: getOffers,
  getOfferById: getOfferById,
  getOffersByValue: getOffersByValue,
  updateOffer: updateOffer,
  createOffer: createOffer,
  deleteOffer: deleteOffer
};
