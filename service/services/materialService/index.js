var logger = require('../../lib/logger');
var MaterialModel = require('../../models/MaterialModel');
var UnitModel = require('../../models/UnitModel');
var MaterialMappingModel = require('../../models/MaterialMappingModel');
var CalculationModel = require('../../models/CalculationModel');

/**
 * Returns materials
 *
 * @param {Object} req - request object
 * @param {Object} res - response object
 *
 * @returns {void}
 */
function getMaterials(req, res) {
  MaterialModel.paginate({}, {populate: {path: 'unit', model: 'Unit'}, limit: 10})
    .then(result => {
      var data = {
        materials: result.docs,
        pages: result.pages,
        page: result.page
      }
      res.send(JSON.stringify(data));
    });
}

function getMaterialById(req, res) {
  logger.debug(`Collecting the material ${req.params.materialId}.`);
  
  Promise.all([MaterialModel.findOne({materialId: req.params.materialId}).populate('unit'), UnitModel.find({})])
    .then(result => {
      var material = result[0],
        units = result[1];

      if (!material) {
        return res.send(JSON.stringify({materialId: req.params.materialId, units: units}));
      }

      res.send(JSON.stringify({material: material, units: units}));
    });
}

function getMaterialsByValue(req, res) {
  var page = req.query.page || 1;
  var value = new RegExp(req.params.value, 'i');
  var query = { $or: [{title: value}, {text: value}] };

  MaterialModel.paginate(query, {populate: {path: 'unit', model: 'Unit'}, page: page, limit: 10})
    .then(result => {
      var data = {
        materials: result.docs,
        pages: result.pages,
        page: result.page
      }
      res.send(JSON.stringify(data));
    });
}

function createMaterial(req, res) {
  logger.debug('Creating material.');

  Promise.all([MaterialModel.find({}), UnitModel.findOne({_id: req.body.unitId})])
    .then(result => {
      var materials = result[0],
        nextMaterialId = materials.length !== 0 ? Math.max.apply(null, materials.map(material => material.materialId)) + 1 : 1,
        unit = result[1];

      var incomingMaterial = {
        materialId: nextMaterialId,
        title: req.body.title,
        text: req.body.text,
        sellingPrice: req.body.sellingPrice,
        purchasePrice: req.body.purchasePrice,
        addition: req.body.addition,          
        amount: req.body.amount,
        unit: unit,
        date: new Date(req.body.date)
      };

      var promise = MaterialModel.create(incomingMaterial);
      promise.then(function (doc) {
        res.send(JSON.stringify(doc));
      })
      .catch(error => {
        console.log(error);
      });
    });
}

function updateMaterial(req, res) {
  logger.debug('Updating material.');

  var material = {
    title: req.body.title,
    text: req.body.text,
    sellingPrice: req.body.sellingPrice,
    purchasePrice: req.body.purchasePrice,
    addition: req.body.addition,
    amount: req.body.amount,
    date: new Date(req.body.date)
  };

  UnitModel.findOne({_id: req.body.unitId}).then(unit => {
    material.unit = unit;

    MaterialModel.update({materialId: req.params.materialId}, material)
    .then(() => {
      res.send();
    });
  });
}

function deleteMaterial(req, res) {
  logger.debug('Deleting the incoming material.');

  MaterialModel.remove({_id: req.params.materialId})
    .then(() => {
      MaterialMappingModel.find({material: req.params.materialId})
        .then(mappings => {
          if (!mappings || mappings.length === 0) {
            return res.send();      
          }

          var mappingIds = mappings.map(mapping => mapping._id);

          CalculationModel.update({}, {$pull: { materialMapping: { $in: mappingIds }}}, {multi: true})
            .then(() => {
              MaterialMappingModel.remove({material: req.params.materialId}).then(() => {
                res.send();
              });
            });
        });
    });
}

module.exports = {
  getMaterials: getMaterials,
  getMaterialById: getMaterialById,
  getMaterialsByValue: getMaterialsByValue,
  createMaterial: createMaterial,
  updateMaterial: updateMaterial,
  deleteMaterial: deleteMaterial
};
