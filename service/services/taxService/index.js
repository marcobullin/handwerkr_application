var TaxModel = require('../../models/TaxModel');

/**
 * Returns units
 *
 * @param {Object} req - request object
 * @param {Object} res - response object
 *
 * @returns {void}
 */
function getTax(req, res) {
  TaxModel.findOne({})
    .then(tax => {
      if (!tax) {
        return res.send(JSON.stringify(19));  
      }
      
      res.send(JSON.stringify(tax.tax));
    });
}

function updateTax(req, res) {
  TaxModel.update({}, {tax: req.body.tax}, {upsert: true})
    .then(() => {
      res.send(JSON.stringify(req.body.tax));
    });
}

module.exports = {
  getTax: getTax,
  updateTax: updateTax
}