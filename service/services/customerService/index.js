var logger = require('../../lib/logger');
var CustomerModel = require('../../models/CustomerModel');

/**
 * Returns customers
 *
 * @param {Object} req - request object
 * @param {Object} res - response object
 *
 * @returns {void}
 */
function getCustomers(req, res) {
  CustomerModel.paginate({}, { sort: {customerId: -1}, limit: 10 })
    .then(result => {
      var data = {
        customers: result.docs,
        pages: result.pages,
        page: result.page
      };

      res.send(JSON.stringify(data));
    });
}

function getCustomerById(req, res) {
  logger.debug(`Collecting the customer ${req.params.customerId}.`);
  
  CustomerModel.findOne({customerId: req.params.customerId})
    .then(customer => {
      if (!customer) {
        return res.send(JSON.stringify({customerId: req.params.customerId}));
      }
      res.send(JSON.stringify(customer));
    });
}

function getCustomersByValue(req, res) {
  var page = req.query.page || 1;
  var value = new RegExp(req.params.value, 'i');
  var query = { $or: [{firstname: value}, {lastname: value}, {name: value}, {street: value}, {city: value}, {zip: value}] };

  CustomerModel.paginate(query, { sort: {customerId: -1}, page: page, limit: 10})
    .then(result => {
      var data = {
        customers: result.docs,
        pages: result.pages,
        page: result.page
      };

      res.send(JSON.stringify(data));
    });
}

function updateCustomer(req, res) {
  logger.debug('Updating customer.');

  var customer = {
    customerId: req.body.customerId,
    title: req.body.title,
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    name: `${req.body.firstname} ${req.body.lastname}`,
    street: req.body.street,
    zip: req.body.zip,
    city: req.body.city,
    mail: req.body.mail,
    phone: req.body.phone
  };

  CustomerModel.update({_id: req.params.customerId}, customer)
    .then(() => {
      res.send();
    });
}

function createCustomer(req, res) {
  logger.debug('Creating customer.');

  CustomerModel.find({})
    .then(customers => {
      var maxCustomerId = customers.length !== 0 ? Math.max.apply(null, customers.map(customer => customer.customerId)) : 0; 

      var incomingCustomer = {
        customerId:  maxCustomerId + 1,
        title: req.body.title,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        name: `${req.body.firstname} ${req.body.lastname}`,
        street: req.body.street,
        zip: req.body.zip,
        city: req.body.city,
        mail: req.body.mail,
        phone: req.body.phone
      };

      CustomerModel.collection.insert(incomingCustomer)
        .then(doc => {
          res.send(JSON.stringify(doc.ops[0]));
        });
    });
}

function deleteCustomer(req, res) {
  logger.debug('Deleting the incoming customer.');

  CustomerModel.remove({_id: req.params.customerId})
    .then(() => {
      res.send();
    });
}

module.exports = {
  getCustomers,
  getCustomerById,
  getCustomersByValue,
  createCustomer,
  updateCustomer,
  deleteCustomer
};
