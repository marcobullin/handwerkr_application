var logger = require('../../lib/logger');
var InvoiceModel = require('../../models/InvoiceModel');
var OfferModel = require('../../models/OfferModel');
var CustomerModel = require('../../models/CustomerModel');
var UnitModel = require('../../models/UnitModel');
var moment = require('moment');

/**
 * Returns units
 *
 * @param {Object} req - request object
 * @param {Object} res - response object
 *
 * @returns {void}
 */
function getInvoices(req, res) {
  InvoiceModel.paginate({}, {limit: 10, sort: {invoiceYear: -1, invoiceCount: -1}})
    .then(result => {
      var data = {
        invoices: result.docs,
        pages: result.pages,
        page: result.page
      };
      res.send(JSON.stringify(data));
    });
}

function getInvoiceById(req, res) {
  InvoiceModel.findOne({_id: req.params.invoiceId}).populate({ path: 'offer', populate: { path: 'invoices', model: 'Invoice'} })
    .then(invoice => {
      if (!invoice) {
        res.writeHead(404);
        return res.end();
      }

      invoice.offer.invoices = invoice.offer.invoices.filter(invoiceItem => invoiceItem._id.toString() !== invoice._id.toString());
      invoice.offer.invoices.reverse();

      UnitModel.find({}).then(units => {
        if (invoice.customerId) {
          CustomerModel.findOne({customerId: invoice.customerId})
            .then(customer => {
              res.send(JSON.stringify({invoice: invoice, customer: customer, units: units}));
            });
        } else {
          res.send(JSON.stringify({invoice: invoice, units: units}));
        }
      });
    });
}

function getInvoiceDraft(req, res) {
  OfferModel.findOne({_id: req.params.offerId}).populate('invoices')
    .then(offer => {
      if (!offer) {
        res.writeHead(404);
        return res.end();
      }

      var invoice = {
        invoiceDate: moment().format('DD.MM.YYYY'),
        customer: offer.customer,
        tax: offer.tax,
        name: offer.name,
        editor: offer.editor,
        offerDate: offer.offerDate,
        withoutTax: offer.withoutTax,
        partialPayment: offer.partialPayment,
        showLawText: offer.showLawText,
        status: 'draft',
        partialPaymentAmount: offer.partialPaymentAmount,
        offer: offer
      }

      if (req.query.isPartialPayment) {
        invoice.type = 'percentage';
        invoice.sections = offer.sections;
      } else if (req.params.sectionIndex === undefined) {
        invoice.sections = offer.sections.filter(section => !section.invoice);
        if (offer.invoices && offer.invoices.length > 0) {
          offer.invoices.reverse().forEach(invoiceItem => {
            if (!invoiceItem.sections || invoiceItem.type === 'percentage') {
              return;
            }

            invoiceItem.sections.map(section => {
              section.belongToPartialInvoice = true;
            });

            invoice.sections = invoiceItem.sections.concat(invoice.sections);
          });
        }
        invoice.type = 'end';
      } else {
        invoice.sectionIndex = req.params.sectionIndex;
        invoice.sections = [offer.sections[req.params.sectionIndex]];
        invoice.type = 'partial';
      }
      
      UnitModel.find({}).then(units => {
        res.send(JSON.stringify({invoice: invoice, units: units}));
      });
    });
}

function _createInvoice(invoice) {
  return new Promise(function(resolve, reject) {
    InvoiceModel.collection.insert(invoice)
      .then(doc => {
        var createdInvoice = doc.ops[0];
        
        OfferModel.findOne({_id: invoice.offer})
          .then(offer => {
            if (!offer) {
              return reject();
            }

            offer.invoices = offer.invoices ? offer.invoices.push(createdInvoice._id) : [createdInvoice._id];
            
            if (invoice.type === 'percentage') {
              offer.partialInvoice = createdInvoice._id;
            } else if (invoice.type === 'partial' && invoice.sectionIndex) {
              offer.sections[invoice.sectionIndex].invoice = createdInvoice._id;
            } else {
              offer.sections.map(section => {
                if (!section.invoice) {
                  section.invoice = createdInvoice._id;
                }
              });
              offer.invoice = createdInvoice._id;
            }

            createdInvoice.offer = offer;

            OfferModel.update({_id: offer._id}, offer).then(() => {
              return resolve(createdInvoice);
            });
          });
      });
  });
}

function getCustomer(customerId) {
  if (!customerId) {
    return Promise.resolve(null);
  }

  return CustomerModel.findOne({ customerId: customerId});
}

function createInvoice(req, res) {
  var invoice = {
    invoiceDate: req.body.invoiceDate,
    offer: req.body.offer,
    tax: req.body.tax,
    name: req.body.name,
    editor: req.body.editor,
    offerDate: req.body.offerDate,
    created: moment().format(),
    sectionIndex: req.body.sectionIndex,
    sections: JSON.parse(req.body.sections || "[]"),
    status: 'draft',
    dueDay: req.body.dueDay,
    allowance: req.body.allowance,
    dueDayAllowance: req.body.dueDayAllowance,
    withoutTax: req.body.withoutTax,
    type: req.body.type,
    showLawText: req.body.showLawText,
    partialPayment: req.body.partialPayment || null,
    partialPaymentAmount: req.body.partialPaymentAmount || null
  };
  
  if (req.body.createInvoiceNumber) {
    var currentYear = (new Date()).getFullYear();

    InvoiceModel.find({invoiceYear: currentYear}).sort({invoiceCount: -1}).limit(1).then(latestInvoice => {
      invoice.invoiceYear = currentYear;

      if (!latestInvoice || latestInvoice.length === 0) {
        invoice.invoiceCount = 1;
      } else {
        invoice.invoiceCount = latestInvoice[0].invoiceCount + 1;
      }
      invoice.status = 'open';
      invoice.dueDate = moment().add(invoice.dueDay, 'day').format('DD.MM.YYYY');
      invoice.invoiceId = invoice.invoiceYear + '-' + invoice.invoiceCount.toString().padStart(4, "0");
      
      if ((parseInt(invoice.allowance || 0) || 0) && invoice.dueDayAllowance) {
        invoice.dueDateAllowance = moment().add(invoice.dueDayAllowance, 'day').format('DD.MM.YYYY');
      }

      getCustomer(req.body.customerId)
        .then(customer => {
          invoice.customer = customer;

          _createInvoice(invoice)
            .then(createdInvoice => {
              return res.send(JSON.stringify({createdInvoice: createdInvoice}));
            })
            .catch(e => {
              res.writeHead(404);
              res.end();
            });
        });
    });
  } else {
    getCustomer(req.body.customerId)
      .then(customer => {
        invoice.customer = customer;
        _createInvoice(invoice)
          .then(createdInvoice => {
            return res.send(JSON.stringify({createdInvoice: createdInvoice}));
          })
          .catch(e => {
            res.writeHead(404);
            res.end();
          });
      });
  }  
}

function updateInvoice(req, res) {
  var invoice = {
    invoiceDate: req.body.invoiceDate,
    offer: req.body.offer,
    tax: req.body.tax,
    name: req.body.name,
    editor: req.body.editor,
    offerDate: req.body.offerDate,
    updated: moment().format(),
    sectionIndex: req.body.sectionIndex,
    sections: JSON.parse(req.body.sections || []),
    showIntroduction: req.body.showIntroduction,
    showLawText: req.body.showLawText,
    showMfg: req.body.showMfg,
    status: req.body.status || 'draft',
    dueDay: req.body.dueDay,
    allowance: req.body.allowance,
    dueDayAllowance: req.body.dueDayAllowance,
    withoutTax: req.body.withoutTax,
    type: req.body.type,
    showLawText: req.body.showLawText,
    partialPayment: req.body.partialPayment || null,
    partialPaymentAmount: req.body.partialPaymentAmount || null
  };
  

  if (invoice.dueDay) {
    invoice.dueDate = moment().add(invoice.dueDay, 'day').format('DD.MM.YYYY');
  } else {
    invoice.dueDate = null;
  }

  if ((parseInt(invoice.allowance || 0) || 0) && invoice.dueDayAllowance) {
    invoice.dueDateAllowance = moment().add(invoice.dueDayAllowance, 'day').format('DD.MM.YYYY');
  } else {
    invoice.dueDateAllowance = null;
  }

  if (req.body.createInvoiceNumber) {
    var currentYear = (new Date()).getFullYear();

    InvoiceModel.find({invoiceYear: currentYear}).sort({invoiceCount: -1}).limit(1).then(latestInvoice => {
      invoice.invoiceYear = currentYear;

      if (!latestInvoice || latestInvoice.length === 0) {
        invoice.invoiceCount = 1;
      } else {
        invoice.invoiceCount = latestInvoice[0].invoiceCount + 1;
      }
      invoice.status = 'open';
      invoice.invoiceId = invoice.invoiceYear + '-' + invoice.invoiceCount.toString().padStart(4, "0");

      getCustomer(req.body.customerId)
        .then(customer => {
          invoice.customer = customer;

          InvoiceModel.update({_id: req.params.invoiceId}, invoice).then(() => {
            res.writeHead(200);
            return res.end();
          });
        });
    });
  } else {
    getCustomer(req.body.customerId)
      .then(customer => {
        invoice.customer = customer;
        InvoiceModel.update({_id: req.params.invoiceId}, invoice).then(() => {
          res.writeHead(200);
          return res.end();
        });
      });
  }
}

function deleteInvoice(req, res) {
  OfferModel.findOne({ invoices: { $in: [req.params.invoiceId]}}).then(offer => {
    if (!offer) {
      res.writeHead(404);
      return res.end();
    }

    if (offer.invoice && offer.invoice.toString() === req.params.invoiceId) {
      offer.invoice = null;
    }
    
    offer.sections.map(section => {
      if (section.invoice && section.invoice.toString() === req.params.invoiceId) {
        section.invoice = null;
      }
    });

    offer.invoices = offer.invoices.filter(invoice => (invoice && invoice.toString() !== req.params.invoiceId));

    OfferModel.update({_id: offer._id}, offer).then(() => {
      InvoiceModel.remove({_id : req.params.invoiceId}).then(() => {
        res.writeHead(200);
        return res.end();
      });
    });
  });
}

/**
 * Returns all invoices found for search value
 *
 * @param {Object} req - request object
 * @param {Object} res - response object
 *
 * @returns {void}
 */
function getInvoicesByValue(req, res) {
  var page = req.query.page || 1;
  var value = new RegExp(req.params.value, 'i');
  var filter = req.query.filter;
  var type = req.query.type;
  var query = { $or: [{invoiceId: value}, {name: value}, {editor: value}, {'customer.name': value}]};

  if (filter !== 'all') {
    query.status = filter;
  }

  if (type) {
    query.type = type;
  }

  InvoiceModel.paginate(query, {page: page, limit: 10, sort: {invoiceYear: -1, invoiceCount: -1}})
    .then(result => {
      var data = {
        invoices: result.docs,
        pages: result.pages,
        page: result.page
      };
      res.send(JSON.stringify(data));
    });
}

function markAsPayed(req, res) {
  InvoiceModel.update({_id: req.params.invoiceId}, {status: 'payed' })
    .then(() => {
      res.writeHead(200);
      return res.end();
    });
}

module.exports = {
  getInvoices: getInvoices,
  getInvoiceById: getInvoiceById,
  getInvoiceDraft: getInvoiceDraft,
  createInvoice: createInvoice,
  updateInvoice: updateInvoice,
  deleteInvoice: deleteInvoice,
  getInvoicesByValue: getInvoicesByValue,
  markAsPayed: markAsPayed
}