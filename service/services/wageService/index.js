var WageModel = require('../../models/WageModel');

/**
 * Returns units
 *
 * @param {Object} req - request object
 * @param {Object} res - response object
 *
 * @returns {void}
 */
function getMiddleWage(req, res) {
  WageModel.findOne({})
    .then(middleWage => {
      if (!middleWage) {
        WageModel.collection.insert({middleWage: 38})
          .then(middleWage => {
            res.send(JSON.stringify(middleWage.ops[0]));
          });
      } else {
        res.send(JSON.stringify(middleWage));
      }
    });
}

function updateMiddleWage(req, res) {
  WageModel.update({_id: req.params.id}, {middleWage: req.body.middleWage})
    .then(() => {
      res.send();
    });
}

module.exports = {
  getMiddleWage: getMiddleWage,
  updateMiddleWage: updateMiddleWage
}