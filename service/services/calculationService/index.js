var logger = require('../../lib/logger');
var CalculationModel = require('../../models/CalculationModel');
var MaterialModel = require('../../models/MaterialModel');
var MaterialMappingModel = require('../../models/MaterialMappingModel');
var UnitModel = require('../../models/UnitModel');
var WageModel = require('../../models/WageModel');
var moment = require('moment');

/**
 * Returns calculations
 *
 * @param {Object} req - request object
 * @param {Object} res - response object
 *
 * @returns {void}
 */
function getCalculations(req, res) {
  CalculationModel.paginate({}, {limit: 10, populate: [{path: 'unit', model: 'Unit'}, {path: 'materialMapping', model: 'MaterialMapping', populate: { path: 'material', populate: { path: 'unit' }}}]})
    .then(result => {
      var data = {
        calculations: result.docs,
        pages: result.pages,
        page: result.page
      };

      res.send(JSON.stringify(data));
    });
}

function getCalculationById(req, res) {
  Promise.all([CalculationModel.findOne({calculationId: req.params.calculationId}).populate('unit').populate({
    path: 'materialMapping',
    populate: { 
      path: 'material',
      populate: { path: 'unit' }
    }
  }).lean(), UnitModel.find({}), WageModel.findOne({})])
    .then(result => {
      var calculation = result[0],
        units = result[1],
        middleWage = result[2]; 

      if (!calculation) {
        return res.send(JSON.stringify({calculationId: req.params.calculationId, units: units, middleWage:middleWage}));
      }

      res.send(JSON.stringify({calculation: calculation, units: units, middleWage: middleWage}));
    });
}

function getCalculationsByValue(req, res) {
  var page = req.query.page || 1;
  var value = new RegExp(req.params.value, 'i');
  var query = { $or: [{title: value}, {text: value}] };

  CalculationModel.paginate(query,{page: page, limit: 10, populate: [{path: 'unit', model: 'Unit'}, {path: 'materialMapping', model: 'MaterialMapping', populate: { path: 'material', populate: { path: 'unit' }}}]})
    .then(result => {
      var calculations = result.docs;

      WageModel.findOne({})
        .then(middleWage => {
          calculations.map(calculation => {
            calculation.materials = [];
            calculation.materialMapping.map(mapping => {
              calculation.materials.push({
                purchasePrice: mapping.material.purchasePrice,
                amount: mapping.amount || mapping.material.amount
              });
            });
          });
          
          var response = {
            calculations: calculations,
            middleWage: middleWage.middleWage,
            pages: result.pages,
            page: result.page
          };

          res.send(JSON.stringify(response));
        });
    });
}

function createCalculation(req, res) {
  Promise.all([CalculationModel.find({}), UnitModel.findOne({_id: req.body.unitId})])
    .then(result => {
      var calculations = result[0],
        nextCalculationId = calculations.length !== 0 ? Math.max.apply(null, calculations.map(calculation => calculation.calculationId)) + 1 : 1,
        unit = result[1];

      var incomingMaterialMapping = JSON.parse(req.body.materialMapping);
      var promise = MaterialMappingModel.create(incomingMaterialMapping);
      promise.then(function (materialMapping) {

        var incomingCalculation = {
          calculationId: nextCalculationId,
          title: req.body.title,
          text: req.body.text,
          wageMinutes: req.body.wageMinutes,
          unit: unit,
          materialMapping: materialMapping || [],
          updated: moment().format()
        };

        var promise = CalculationModel.create(incomingCalculation);
        promise.then(function (doc) {
          res.send(JSON.stringify(doc));
        });
      })
      .catch(error => {
        console.log(error);
      });
      
    });
}

function deleteOldMaterialMappings(ids) {
  MaterialMappingModel.remove({ _id: { '$in': ids } }, function(err) {
    if (err) {
      console.log(err);
    }
  });
}

function updateCalculation(req, res) {
  Promise.all([
    UnitModel.findOne({_id: req.body.unitId}),
    MaterialMappingModel.find({}),
    CalculationModel.findOne({_id: req.params.calculationId}).populate('materialMapping')
  ])
    .then(result => {
      var unit = result[0],
        materialMapping = result[1],
        calculation = result[2],
        oldMaterialMappingIds = calculation.materialMapping.map(function(obj) {
          return obj._id;
        });

      // update calculation including freshly created materialMappings
      var incomingMaterialMapping = JSON.parse(req.body.materialMapping);
      var promise = MaterialMappingModel.create(incomingMaterialMapping);
      promise.then(function (materialMapping) {
        var calculation = {
          title: req.body.title,
          text: req.body.text,
          wageMinutes: req.body.wageMinutes,
          unit: unit,
          materialMapping: materialMapping || [],
          updated: moment().format()
        };

        CalculationModel.update({_id: req.params.calculationId}, calculation)
          .then(() => {
            deleteOldMaterialMappings(oldMaterialMappingIds);
            res.send();
          });
        });
    })
    .catch(error => {
      console.log(error);
    });
}

function deleteCalculation(req, res) {
  logger.debug('Deleting the incoming calculation.');

  CalculationModel.remove({_id: req.params.calculationId})
    .then(() => {
      res.send();
    });
}

module.exports = {
  getCalculations,
  getCalculationById,
  getCalculationsByValue,
  createCalculation,
  updateCalculation,
  deleteCalculation
};
