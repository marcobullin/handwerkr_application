#!/bin/sh

docker container rm -f mongodatabase
docker container run -d -p 27017:27017 --network=handwerkr -itd --name=mongodatabase mongo
docker build -t handwerkr_service . -f Dockerfile.dev
docker container rm -f handwerkr_service
docker container run -d -p 3001:3001 --net=handwerkr -v $(pwd):/app --name=handwerkr_service handwerkr_service
