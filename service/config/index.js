var _ = require('lodash');

module.exports = function () {
  var mapping = {
    production: 'prod',
    prod: 'prod',
    ci: 'ci',
    stage: 'stage',
    dev: 'dev',
    local: 'dev',
    test: 'test'
  };

  var environment = process.env.NODE_ENV || 'dev';

  if (!mapping[environment]) {
    throw 'Could not find correct environment. Please set your NODE_ENV to (prod, stage, ci or dev).';
  }

  var baseConfig = require('./base');
  var envConfig = require('./' + mapping[environment]);

  return _.merge(baseConfig, envConfig);
}();
