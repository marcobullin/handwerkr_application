module.exports = {
  url: 'http://localhost:3000',
  mongodb: 'mongodb://localhost/handwerkr_service_test',
  basicAuth: {
    enabled: false
  },
  logLevel: 'debug'
};
