var customerService = require('./services/customerService');
var calculationService = require('./services/calculationService');
var materialService = require('./services/materialService');
var offerService = require('./services/offerService');
var unitService = require('./services/unitService');
var taxService = require('./services/taxService');
var wageService = require('./services/wageService');
var invoiceService = require('./services/invoiceService');

module.exports = function (app) {
  app.get('/customers', customerService.getCustomers);
  app.get('/customers/search/:value?', customerService.getCustomersByValue);
  app.get('/customers/:customerId', customerService.getCustomerById);
  app.post('/customers/create', customerService.createCustomer);
  app.post('/customers/delete/:customerId', customerService.deleteCustomer);
  app.post('/customers/:customerId', customerService.updateCustomer);
  
  app.get('/units', unitService.getUnits);

  app.get('/tax', taxService.getTax);
  app.post('/tax', taxService.updateTax);

  app.get('/middleWage', wageService.getMiddleWage);
  app.post('/middleWage/:id', wageService.updateMiddleWage);

  app.get('/calculations', calculationService.getCalculations);
  app.get('/calculations/search/:value?', calculationService.getCalculationsByValue);
  app.get('/calculations/:calculationId', calculationService.getCalculationById);
  app.post('/calculations/new', calculationService.createCalculation);
  app.post('/calculations/delete/:calculationId', calculationService.deleteCalculation);
  app.post('/calculations/:calculationId', calculationService.updateCalculation);

  app.get('/materials', materialService.getMaterials);
  app.get('/materials/search/:value?', materialService.getMaterialsByValue);
  app.get('/materials/:materialId', materialService.getMaterialById);
  app.post('/materials/new', materialService.createMaterial);
  app.post('/materials/delete/:materialId', materialService.deleteMaterial);
  app.post('/materials/:materialId', materialService.updateMaterial);

  app.get('/offers', offerService.getOffers);
  app.get('/offers/search/:value?', offerService.getOffersByValue);
  app.get('/offers/:offerId', offerService.getOfferById);
  app.post('/offers/create', offerService.createOffer);
  app.post('/offers/delete/:offerId', offerService.deleteOffer);
  app.post('/offers/:offerId', offerService.updateOffer);

  app.get('/invoices', invoiceService.getInvoices);
  app.get('/invoices/search/:value?', invoiceService.getInvoicesByValue);
  app.post('/invoices/create', invoiceService.createInvoice);
  app.post('/invoices/markAsPayed/:invoiceId', invoiceService.markAsPayed);
  
  app.get('/invoices/:invoiceId', invoiceService.getInvoiceById);
  app.post('/invoices/:invoiceId', invoiceService.updateInvoice);
  app.get('/invoices/draft/:offerId/:sectionIndex?', invoiceService.getInvoiceDraft);
  app.post('/invoices/delete/:invoiceId', invoiceService.deleteInvoice);
};
