var bodyParser = require('body-parser');
var compression = require('compression');
var auth = require('basic-auth');
var mongoose = require('mongoose');
var config = require('./config');
var logger = require('./lib/logger');
var crypto = require('crypto');

module.exports = function (app, express) {
  app.use(compression());

  app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');    
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS');
    res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, X-Signature");
    res.setHeader('X-Frame-Options', 'SAMEORIGIN');
    res.setHeader('X-Content-Type-Options', 'nosniff');
    res.setHeader('X-XSS-Protection', '1; mode=block');

    if (req.method === "OPTIONS") {
      res.writeHead(204);
      res.end();
    } else {
      var sharedSecret = '_my_secret_';
      var retrievedSignature = req.headers["x-signature"];
      var computedSignature = crypto.createHmac("sha256", sharedSecret).update(req.url).digest("hex");
   
      if (computedSignature === retrievedSignature) {
        next();
      } else {
        res.writeHead(403, {
            "Content-Type": "text/plain"
        });
        res.end('Access denied');
      }
    }





    // if (!req.query.k || req.query.k !== '3b8b7c1fe07b6f37cf3534c324264fc2d5132edd1d0adade6659f213c0397c5c') {
    //   res.statusCode = 401;
    //   res.end('Access denied');
    //   return;
    // }

    /*
    res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

    res.setHeader('X-Frame-Options', 'SAMEORIGIN');
    res.setHeader('X-Content-Type-Options', 'nosniff');
    res.setHeader('X-XSS-Protection', '1; mode=block');
    next();
    */
  });

  /*
   * MongoDB
   */
  mongoose.connect(config.mongodb);

  var db = mongoose.connection;

  db.on('error', function (err) {
    logger.error(err);
  });

  db.once('open', function() {
    logger.info('DB connection established.');
  });

  app.use(function (req, res, next) {
    logger.info(`${req.method} ${req.url} ${JSON.stringify(req.params)} ${JSON.stringify(req.body) || ''}`);
    
    if (config.basicAuth.enabled !== true) {
      return next();
    }
    var credentials = auth(req);
    
    if (!credentials || credentials.name !== config.basicAuth.username || credentials.pass !== config.basicAuth.password) {
      res.statusCode = 401;
      res.setHeader('WWW-Authenticate', 'Basic realm="example"');
      res.end('Access denied');
    } else {
      next();
    }
  });

  /*
   * body parsers
   */
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());

  return app;
};
