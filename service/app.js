var express = require('express');
var router = require('./router');
var middleware = require('./middleware');
var app = express();

middleware(app, express);
router(app);

app.listen(process.env.PORT || 3001, function () {});